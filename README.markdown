This file contains a list of instructions that follow the BOILERPLATE.markdown

1) Customizations to Bootstrap V3.0

    Grid System
    @grid-gutter-width: 10px

    Typography
    @font-family-sans-serif: "Helvetica Neue", sans-serif, Arial
    @font-family-serif: "Rokkitt", "Times New Roman", serif

    Scrollbars for IE
    @-ms-viewport{ width: auto !important; } into css

2) Included remove logging https://github.com/ehynds/grunt-remove-logging
    ..no dice but left in the configuration re: http://www.ericfeminella.com/blog/2012/03/24/preprocessing-modules-with-requirejs-optimizer/
3) Added configuration via json model (config.json) http://stackoverflow.com/questions/10914166/creating-backbone-model-using-received-json-data
4) TODO: http://mikemclin.net/configuring-package-json-and-gruntfile-js/
4) Edits required to leaflet.js
    - include images in css\imagea
    - update leaflet image path for debugging and deployment...no script tags in this boilerplate
        L.Icon.Default.imagePath = (function () {

            var url = location.href;  // entire url including querystring - also: window.location.href;
            var baseURL = url.substring(0, url.indexOf('/', 14));

            // Base Url for localhost
            var url = location.href;  // window.location.href;
            var pathname = location.pathname;  // window.location.pathname;
            var index1 = url.indexOf(pathname);
            var index2 = url.indexOf("/", index1 + 1);
            var baseLocalUrl = url.substr(0, index2);

            var path = baseLocalUrl + "/"; // url plus app name
            if (baseURL.indexOf('http://localhost') != -1) {
                // Split the path name and use the first two entries on localhost i.e. app/public
                var pathParts = pathname.split("/");
                path = baseLocalUrl + "/" + pathParts[2]
            }

            return path + "/css/images";
        }
5) TODO: customization to kendo odata to include M for known fields
6) Added Google Analytics - updates tracking id needs to be updated in index.html per deployment

