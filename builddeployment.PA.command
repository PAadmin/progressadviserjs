#!/bin/bash
ROOTDIR="$(dirname $0)"
pushd $ROOTDIR

# get a unique version number for this build
VERSIONNUM="v$(date "+%y%j%H%M")"

# navigate to source build the release files
cd ../progressAdviserJS
SOURCEDIR=$PWD

grunt

cd ../Deployment
ROOTDIR=$PWD

# remove the current deployment/build
rm -rfv progressAdviserJS && mkdir progressAdviserJS

# drop down into deployment and create the necessary directories
cd progressAdviserJS
mkdir css
cd css
mkdir Default
mkdir images
mkdir jQuery.ui.themes
cd ..
mkdir fonts
mkdir images
mkdir img
mkdir localeStrings
mkdir js
cd js
mkdir app
mkdir libs
cd app
mkdir config
mkdir init
cd ..
cd ..

echo “SOURCEDIR:”$SOURCEDIR
echo “ROOTDIR:”$ROOTDIR

# copy the required css files
cp "$SOURCEDIR/public/css/desktop.ie.min.css" "$ROOTDIR/progressAdviserJS/css/desktop.ie.min.css"
cp "$SOURCEDIR/public/css/desktop.min.css" "$ROOTDIR/progressAdviserJS/css/desktop.min.css"
cp "$SOURCEDIR/public/css/desktop.min.prefix.css" "$ROOTDIR/progressAdviserJS/css/desktop.min.prefix.css"
cp "$SOURCEDIR/public/css/tablet.min.css" "$ROOTDIR/progressAdviserJS/css/tablet.min.css"
cp "$SOURCEDIR/public/css/phone.min.css" "$ROOTDIR/progressAdviserJS/css/phone.min.css"
cp "$SOURCEDIR/public/css/bootstrap.min.css" "$ROOTDIR/progressAdviserJS/css/bootstrap.min.css"
cp "$SOURCEDIR/public/css/font-awesome.min.css" "$ROOTDIR/progressAdviserJS/css/font-awesome.min.css"
# cp "$SOURCEDIR/public/css/leaflet.min.css" "$ROOTDIR/progressAdviserJS/css/leaflet.min.css"
cp "$SOURCEDIR/public/css/kendo.common.min.css" "$ROOTDIR/progressAdviserJS/css/kendo.common.min.css"
cp "$SOURCEDIR/public/css/kendo.default.min.css" "$ROOTDIR/progressAdviserJS/css/kendo.default.min.css"
cp "$SOURCEDIR/public/css/kendo.dataviz.min.css" "$ROOTDIR/progressAdviserJS/css/kendo.dataviz.min.css"
cp "$SOURCEDIR/public/css/kendo.dataviz.default.min.css" "$ROOTDIR/progressAdviserJS/css/kendo.dataviz.default.min.css"

# merge the css files into a single desktop.min.css for modern browsers and
# a hybrid with imports for IE
cd "css"
cat desktop.min.prefix.css desktop.ie.min.css > desktop.ie.min.prod.css
rm desktop.min.prefix.css
rm desktop.ie.min.css
mv desktop.ie.min.prod.css desktop.ie.min.css

# copy related themes, images, etc
cp -r $SOURCEDIR/public/css/Default/* $ROOTDIR/progressAdviserJS/css/Default
cp -r $SOURCEDIR/public/css/images/* $ROOTDIR/progressAdviserJS/css/images
cp -r $SOURCEDIR/public/css/jQuery.ui.themes/* $ROOTDIR/progressAdviserJS/css/jQuery.ui.themes
cp -r $SOURCEDIR/public/fonts/* $ROOTDIR/progressAdviserJS/fonts
cp -r $SOURCEDIR/public/images/* $ROOTDIR/progressAdviserJS/images
cp -r $SOURCEDIR/public/img/* $ROOTDIR/progressAdviserJS/img
cp -r $SOURCEDIR/public/localeStrings/* $ROOTDIR/progressAdviserJS/localeStrings
cp -r $SOURCEDIR/public/js/app/config/config.js $ROOTDIR/progressAdviserJS/js/app/config/config.js
cp -r $SOURCEDIR/public/js/app/init/DesktopInit.min.js $ROOTDIR/progressAdviserJS/js/app/init/DesktopInit.min.js
cp -r $SOURCEDIR/public/js/app/init/TabletInit.min.js $ROOTDIR/progressAdviserJS/js/app/init/TabletInit.min.js
cp -r $SOURCEDIR/public/js/app/init/PhoneInit.min.js $ROOTDIR/progressAdviserJS/js/app/init/PhoneInit.min.js
cp -r $SOURCEDIR/public/js/libs/require.js $ROOTDIR/progressAdviserJS/js/libs/require.js
cp -r $SOURCEDIR/public/index.html $ROOTDIR/progressAdviserJS/index.html
cp -r $SOURCEDIR/public/config.json $ROOTDIR/progressAdviserJS/config.json

# version the scripts and css for updates
mv "desktop.ie.min.css" "desktop.ie.min_$VERSIONNUM.css"
mv "desktop.min.css" "desktop.min_$VERSIONNUM.css"
mv "tablet.min.css" "tablet.min_$VERSIONNUM.css"
mv "phone.min.css" "phone.min_$VERSIONNUM.css"
cd ..
cd js/app/init
mv "DesktopInit.min.js" "DesktopInit.min_$VERSIONNUM.js"
mv "TabletInit.min.js" "TabletInit.min_$VERSIONNUM.js"
mv "PhoneInit.min.js" "PhoneInit.min_$VERSIONNUM.js"
cd ..
cd ..


# ensure the production switch is set and that we are referencing the latest files
cd ..
sed -i.bak 's/production = false/production = true/g' index.html
rm index.html.bak
sed -i.bak "s/DesktopInit.min.js/DesktopInit.min_$VERSIONNUM.js/g" index.html
rm index.html.bak
sed -i.bak "s/TabletInit.min.js/TabletInit.min_$VERSIONNUM.js/g" index.html
rm index.html.bak
sed -i.bak "s/PhoneInit.min.js/PhoneInit.min_$VERSIONNUM.js/g" index.html
rm index.html.bak
sed -i.bak "s/desktop.ie.min.css/desktop.ie.min_$VERSIONNUM.css/g" index.html
rm index.html.bak
sed -i.bak "s/desktop.min.css/desktop.min_$VERSIONNUM.css/g" index.html
rm index.html.bak
sed -i.bak "s/tablet.min.css/tablet.min_$VERSIONNUM.css/g" index.html
rm index.html.bak
sed -i.bak "s/phone.min.css/phone.min_$VERSIONNUM.css/g" index.html
rm index.html.bak

popd