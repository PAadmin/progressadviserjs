require.config({
    baseUrl:"./js/app",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery":"../libs/jquery-1.9.1.min",


        "jqueryui":"../libs/jqueryui",
        "jquerymobile":"../libs/jquery.mobile-1.4.4.min",
        "underscore":"../libs/lodash",
        "backbone":"../libs/backbone-min",
        "marionette":"../libs/backbone.marionette",
        "handlebars":"../libs/handlebars",
        "hbs":"../libs/hbs",
        "i18nprecompile":"../libs/i18nprecompile",
        "json2":"../libs/json2",
        "jasmine": "../libs/jasmine",
        "jasmine-html": "../libs/jasmine-html",
        //"ext": "../libs/ext-all",
        "leaflet": "../libs/leaflet-src",
//        "kendo": "../libs/kendo.all.min",

        // Custom Kendo Build (RequireJS AMD)
        "kendo": '../libs/kendo',
        "kendo.dropdowntreeview": "../libs/plugins/kendo.dropdowntreeview",

        // Just Gage
//        'eve' : '../libs/raphael/eve',
//        'raphael-core' : '../libs/raphael/raphael.core',
//        'raphael-svg' : '../libs/raphael/raphael.svg',
//        'raphael-vml' : '../libs/raphael/raphael.vml',
//        'raphael' : '../libs/raphael/raphael.amd',
//        'justguage' :'../libs/justgage.1.0.1.min',

        // Plugins
        "backbone.validateAll":"../libs/plugins/Backbone.validateAll",
        "bootstrap":"../libs/plugins/bootstrap.min",
        "text":"../libs/plugins/text",
        "jasminejquery": "../libs/plugins/jasmine-jquery",
        "jquery.iosslider":"../libs/plugins/jquery.iosslider",
        "jquery.cookie":"../libs/plugins/jquery.cookie",
        "imagesLoaded":  "../libs/plugins/imagesloaded.pkgd.min",
        "jquery.easing": "../libs/plugins/jquery.easing-1.3",
        "jquery.easypiechart": "../libs/plugins/jquery.easypiechart",
        "jquery.circliful": "../libs/plugins/jquery.circliful.min",
        "jquery.scrollTo": "../libs/plugins/jquery.scrollTo",

//        https://github.com/MoonScript/jQuery-ajaxTransport-XDomainRequest
//        "jquery.XDomainRequest": "../libs/plugins/jquery.XDomainRequest",

        // RadiantQ Gantt
        //"RadiantQ":"../libs/RadiantQ-jQuery.Gantt.4.5.8.min",
        "jquery.layout":"../libs/plugins/jquery.layout-latest.min",
        "jquery.tmpl":"../libs/plugins/jquery.tmpl",
        "jquery.ui.grid":"../libs/plugins/jquery.ui.grid",
        "jquery.i18n.properties":"../libs/plugins/jquery.i18n.properties-min-1.0.9",
        "jquery.contextMenu":"../libs/plugins/jquery.contextMenu",
        "date":"../libs/plugins/date", // TODO: Cleanup
        "canvg":"../libs/plugins/canvg",
        "html2canvas": "../libs/html2canvas",
        "jquery.dateFormat": "../libs/plugins/jquery.dateFormat",
        //"RadiantQ.editextension":"../libs/plugins/RadiantQ.editextension",

        // Plugins-leaflet
        //"leaflet.ajax": "../libs/plugins/leaflet.ajax",
        "leaflet.markercluster": "../libs/plugins/leaflet.markercluster-src",
        "leaflet.bouncemarker": "../libs/plugins/leaflet.bouncemarker",
        "leaflet.heat": "../libs/plugins/leaflet.heat",
        "leaflet.esri": "../libs/plugins/esri-leaflet-src",
        "leaflet.esri.heatmap": "../libs/plugins/esri-leaflet-heatmap-feature-layer-src",
        "leaflet.esri.geocoder": "../libs/plugins/esri-leaflet-geocoder-src"

        // Lazy loading example
        //http://www.joezimjs.com/javascript/lazy-loading-javascript-with-requirejs/
        //"lazyLoader": "../libs/lazyLoader"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        // Twitter Bootstrap jQuery plugins
        "bootstrap":["jquery"],


        // jQueryUI
        "jqueryui":["jquery"],

        // jQuery mobile
        "jquerymobile":["jqueryui"],

        // JQuery Plugins
        "jquery.cookie":["jquery"],
        "jquery.iosslider":["jquery"],

        "jquery.easing":["jquery"],
        "jquery.easypiechart":["jquery","jquery.easing"],
        "jquery.circliful": ["jquery"],
        "jquery.scrollTo": ["jquery"],

//        "jquery.XDomainRequest":["jquery"],

        "imagesLoaded":{
            "deps":["jquery"],
            "exports": "imagesLoaded"
        },

//        "raphael":{
//            "exports" : "R"
//        },
//        "justgage": ["R"],

        // RadiantQ Gantt
        "jquery.ui.grid":["jquery", "jqueryui"],
        "jquery.layout":["jquery", "jqueryui"],
        "jquery.tmpl":["jquery"],
        "jquery.i18n.properties":["jquery", "jqueryui"],
        "jquery.contextMenu":["jquery", "jqueryui"],
        "RadiantQ.editextension":["jquery", "jqueryui", "date"],
        "date":["jquery", "jqueryui"],
        "canvg":["jquery"],

        "RadiantQ":{
            "deps":["jquery","jqueryui","jquery.ui.grid","jquery.layout","jquery.tmpl","RadiantQ.editextension","jquery.contextMenu","jquery.i18n.properties","date","canvg"],
            "exports":"RadiantQ"
        },

        // Backbone
        "backbone":{
            // Depends on underscore/lodash and jQuery
            "deps":["underscore", "jquery"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        // Marionette
        "marionette":{
            "deps":["underscore", "backbone", "jquery"],
            "exports":"Marionette"
        },
        // Handlebars
        "handlebars":{
            "exports":"Handlebars"
        },
        // Backbone.validateAll plugin that depends on Backbone
        "backbone.validateAll":["backbone"],


        //ExtJS
        //"ext":{
        //    "exports": "Ext"
        //},

//        // Kendo
//        "kendo":{
//            "exports":"kendo",
//            "deps":["jquery"]
//        },
//        "kendo.dropdowntreeview":{
//            "deps": ["kendo", "jquery"],
//            "exports": "kendo.dropdowntreeview"
//        },
        // Kendo
        "kendo/kendo.core": {
            deps: ["jquery"]
        },
        "kendo.dropdowntreeview":{
            "deps": ["kendo/kendo.treeview", "kendo/kendo.dropdownlist", "jquery"],
            "exports": "kendo.dropdowntreeview"
        },

        // Leaflet
        "leaflet":{
            "exports": "L"
        },
        //"leaflet.ajax":["leaflet"],
        "leaflet.markercluster":["leaflet"],
        "leaflet.bouncemarker":["leaflet"],
        "leaflet.heat":["leaflet"],
        "leaflet.esri":["leaflet"],
        "leaflet.esri.heatmap":["leaflet","leaflet.heat"],
        "leaflet.esri.geocoder":["leaflet","leaflet.esri"],
        
        "jasmine": {
            // Exports the global 'window.jasmine' object
            "exports": "jasmine"
        },

        "jasmine-html": {
            "deps": ["jasmine"],
            "exports": "jasmine"
        }

//        "lazyLoader":{
//            "exports": "lazyLoader"
//        }

    },
    // hbs config - must duplicate in Gruntfile.js Require build
    hbs: {
        templateExtension: "html",
        helperDirectory: "templates/helpers/",
        i18nDirectory: "templates/i18n/",

        compileOptions: {}        // options object which is passed to Handlebars compiler
    }
});