define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(App) {
                _.bindAll(this);

                this.Application = App;

                this.colorDataDate = null;

                this.colorSelected = null;

                // Budget Classification
                this.colorBudgetClass1 = null;
                this.colorBudgetClass2 = null;
                this.colorBudgetClass3 = null;
                this.colorBudgetClass4 = null;
                this.colorBudgetClass5 = null;

                // TODO: Neutral/Default Deconflict
                this.colorDefault = null;
                this.colorDefaultLight = null;
                this.colorDefaultVeryLight = null;
                this.colorDefaultBright = null;
                this.colorNeutral = null;
                this.colorNeutralLight = null;
                this.colorBad = null;
                this.colorBadLight = null;
                this.colorBadVeryLight = null;
                this.colorBadBright = null;
                this.colorGood = null;
                this.colorGoodLight = null;
                this.colorGoodVeryLight = null;
                this.colorGoodBright = null;
                this.colorWarning = null;
                this.colorWarningLight = null;
                this.colorWarningVeryLight = null;
                this.colorWarningBright = null;

                this.colorCPIPeriod = null;
                this.colorCPICummulative = null;
                this.colorSPIPeriod = null;
                this.colorSPICummulative = null;

                this.colorBaselinePeriod = null;
                this.colorBaselineCummulative = null;
                this.colorActualPeriod = null;
                this.colorActualCummulative = null;
                this.colorEarnedPeriod = null;
                this.colorEarnedCummulative = null;
                this.colorForecastPeriod = null;
                this.colorForecastCummulative = null;

            }
            //getDate_Formatted: function(date){
            //
            //    var dateTime = new Date(date);
            //    var month = dateTime.getMonth() + 1; // Month starts at 0 so we add 1 to make it display as current month
            //    var day = dateTime.getDate();
            //    var year = dateTime.getFullYear();
            //
            //    var formatedDate = month + "/" + day + "/" + year;
            //    return formatedDate;
            //},
            //getDate_UTCCorrected: function(date){
            //    var offset = (date.getTimezoneOffset() / 60);
            //    date.setHours(date.getHours()+offset);
            //    return date;
            //},
            //getDate_PeriodEndDateCorrected: function(date){
            //
            //    // Kendo labels default to month or can be explicitly
            //    // set to month - here we correct to show last day of the
            //    // month - or first to correspond with the reporting period
            //    var stupidKendoDate = date;
            //    var month = stupidKendoDate.getMonth();
            //    var year = stupidKendoDate.getYear();
            //    var fixedDayDate = new Date(year, month + 1, 0);
            //    var fixedDay = fixedDayDate.getDate();
            //    var fixedDate = new Date(date.getFullYear(), month, fixedDay);
            //    return fixedDate;
            //},
            //getDate_PeriodStartDateCorrected: function(date){
            //
            //    // Kendo labels default to month or can be explicitly
            //    // set to month - here we correct to show first day of the
            //    // month - or first to correspond with the reporting period
            //    var stupidKendoDate = date;
            //    var month = stupidKendoDate.getMonth();
            //    var year = stupidKendoDate.getYear();
            //    var fixedDayDate =  new Date(year, month, 1);
            //    var fixedDay = fixedDayDate.getDate();
            //    var fixedDate = new Date(date.getFullYear(), month, fixedDay);
            //    return fixedDate;
            //},
            //getDate_MonthsBetweenStartAndEnd: function(d1,d2){
            //    var months;
            //    months = (d2.getFullYear() - d1.getFullYear()) * 12;
            //    months -= d1.getMonth() + 1;
            //    months += d2.getMonth();
            //    return months <= 0 ? 0 : months;
            //},
            //getFont_ChartAxisLabel: function(){
            //    return '11px "Helvetica Neue", sans-serif, Arial';
            //},
            //getFont_ChartLabel: function(){
            //    return 'bold 12px "Helvetica Neue", sans-serif, Arial';
            //},
            //getFont_ChartLegendLabel: function(){
            //    return '12px "Helvetica Neue", sans-serif, Arial';
            //},
            //getColor_DataDate: function(){
            //    if (this.colorSelected === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-data-date").css("color");
            //        tempDiv.remove();
            //        this.colorDataDate = color;
            //    }
            //
            //    return this.colorDataDate;
            //},
            //getColor_selected: function(){
            //    if (this.colorSelected === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-selected").css("color");
            //        tempDiv.remove();
            //        this.colorSelected = color;
            //    }
            //
            //    return this.colorSelected;
            //},
            //getColor_neutral: function(){
            //
            //    if (this.colorNeutral === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-neutral").css("color");
            //        tempDiv.remove();
            //        this.colorNeutral = color;
            //    }
            //
            //    return this.colorNeutral;
            //},
            //getColor_neutral_light: function(){
            //
            //    if (this.colorNeutralLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-neutral-light").css("color");
            //        tempDiv.remove();
            //        this.colorNeutralLight = color;
            //    }
            //
            //    return this.colorNeutralLight;
            //},
            //getColor_default: function(){
            //
            //    if (this.colorDefault === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-default").css("color");
            //        tempDiv.remove();
            //        this.colorDefault = color;
            //    }
            //
            //    return this.colorDefault;
            //},
            //getColor_default_light: function(){
            //
            //    if (this.colorDefaultLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-default-light").css("color");
            //        tempDiv.remove();
            //        this.colorDefaultLight = color;
            //    }
            //
            //    return this.colorDefaultLight;
            //},
            //getColor_default_very_light: function(){
            //
            //    if (this.colorDefaultVeryLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-default-very-light").css("color");
            //        tempDiv.remove();
            //        this.colorDefaultVeryLight = color;
            //    }
            //
            //    return this.colorDefaultVeryLight;
            //},
            //getColor_default_bright: function(){
            //
            //    if (this.colorDefaultBright === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-default-bright").css("color");
            //        tempDiv.remove();
            //        this.colorDefaultBright = color;
            //    }
            //
            //    return this.colorDefaultBright;
            //},
            //getColor_bad: function(){
            //
            //    if (this.colorBad === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-bad").css("color");
            //        tempDiv.remove();
            //        this.colorBad = color;
            //    }
            //
            //    return this.colorBad;
            //},
            //getColor_bad_light: function(){
            //
            //    if (this.colorBadLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-bad-light").css("color");
            //        tempDiv.remove();
            //        this.colorBadLight = color;
            //    }
            //
            //    return this.colorBadLight;
            //},
            //getColor_bad_very_light: function(){
            //
            //    if (this.colorBadVeryLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-bad-very-light").css("color");
            //        tempDiv.remove();
            //        this.colorBadVeryLight = color;
            //    }
            //
            //    return this.colorBadVeryLight;
            //},
            //getColor_bad_bright: function(){
            //
            //    if (this.colorBadBright === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-bad-bright").css("color");
            //        tempDiv.remove();
            //        this.colorBadBright = color;
            //    }
            //
            //    return this.colorBadBright;
            //},
            //getColor_good: function(){
            //
            //    if (this.colorGood === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-good").css("color");
            //        tempDiv.remove();
            //        this.colorGood = color;
            //    }
            //
            //    return this.colorGood;
            //},
            //getColor_good_light: function(){
            //
            //    if (this.colorGoodLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-good-light").css("color");
            //        tempDiv.remove();
            //        this.colorGoodLight = color;
            //    }
            //
            //    return this.colorGoodLight;
            //},
            //getColor_good_very_light: function(){
            //
            //    if (this.colorGoodVeryLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-good-very-light").css("color");
            //        tempDiv.remove();
            //        this.colorGoodVeryLight = color;
            //    }
            //
            //    return this.colorGoodVeryLight;
            //},
            //getColor_good_bright: function(){
            //
            //    if (this.colorGoodBright === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-good-bright").css("color");
            //        tempDiv.remove();
            //        this.colorGoodBright = color;
            //    }
            //
            //    return this.colorGoodBright;
            //},
            //getColor_warning: function(){
            //
            //    if (this.colorWarning === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-warning").css("color");
            //        tempDiv.remove();
            //        this.colorWarning = color;
            //    }
            //
            //    return this.colorWarning;
            //},
            //getColor_warning_light: function(){
            //
            //    if (this.colorWarningLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-warning-light").css("color");
            //        tempDiv.remove();
            //        this.colorWarningLight = color;
            //    }
            //
            //    return this.colorWarningLight;
            //},
            //getColor_warning_very_light: function(){
            //
            //    if (this.colorWarningVeryLight === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-warning-very-light").css("color");
            //        tempDiv.remove();
            //        this.colorWarningVeryLight = color;
            //    }
            //
            //    return this.colorWarningVeryLight;
            //},
            //getColor_warning_bright: function(){
            //
            //    if (this.colorWarningBright === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-warning-bright").css("color");
            //        tempDiv.remove();
            //        this.colorWarningBright = color;
            //    }
            //
            //    return this.colorWarningBright;
            //},
            //getColor_cpi_period: function(){
            //
            //    if (this.colorCPIPeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-cpi-p").css("color");
            //        tempDiv.remove();
            //        this.colorCPIPeriod = color;
            //    }
            //
            //    return this.colorCPIPeriod;
            //},
            //getColor_cpi_cumulative: function(){
            //
            //    if (this.colorCPICummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-cpi-c").css("color");
            //        tempDiv.remove();
            //        this.colorCPICummulative = color;
            //    }
            //
            //    return this.colorCPICummulative;
            //},
            //getColor_spi_period: function(){
            //
            //    if (this.colorSPIPeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-spi-p").css("color");
            //        tempDiv.remove();
            //        this.colorSPIPeriod = color;
            //    }
            //
            //    return this.colorSPIPeriod;
            //},
            //getColor_spi_cumulative: function(){
            //
            //    if (this.colorSPICummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-spi-c").css("color");
            //        tempDiv.remove();
            //        this.colorSPICummulative = color;
            //    }
            //
            //    return this.colorSPICummulative;
            //},
            //getColor_baseline_period: function(){
            //
            //    if (this.colorBaselinePeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-baseline-p").css("color");
            //        tempDiv.remove();
            //        this.colorBaselinePeriod = color;
            //    }
            //
            //    return this.colorBaselinePeriod;
            //},
            //getColor_baseline_cumulative: function(){
            //
            //    if (this.colorBaselineCummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-baseline-c").css("color");
            //        tempDiv.remove();
            //        this.colorBaselineCummulative = color;
            //    }
            //
            //    return this.colorBaselineCummulative;
            //},
            //getColor_earned_period: function(){
            //
            //    if (this.colorEarnedPeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-earned-p").css("color");
            //        tempDiv.remove();
            //        this.colorEarnedPeriod = color;
            //    }
            //
            //    return this.colorEarnedPeriod;
            //},
            //getColor_earned_cumulative: function(){
            //
            //    if (this.colorEarnedCummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-earned-c").css("color");
            //        tempDiv.remove();
            //        this.colorEarnedCummulative = color;
            //    }
            //
            //    return this.colorEarnedCummulative;
            //},
            //getColor_actual_period: function(){
            //
            //    if (this.colorActualPeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-actual-p").css("color");
            //        tempDiv.remove();
            //        this.colorActualPeriod = color;
            //    }
            //
            //    return this.colorActualPeriod;
            //},
            //getColor_actual_cumulative: function(){
            //
            //    if (this.colorActualCummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-actual-c").css("color");
            //        tempDiv.remove();
            //        this.colorActualCummulative = color;
            //    }
            //
            //    return this.colorActualCummulative;
            //},
            //getColor_forecast_period: function(){
            //
            //    if (this.colorForecastPeriod === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-forecast-p").css("color");
            //        tempDiv.remove();
            //        this.colorForecastPeriod = color;
            //    }
            //
            //    return this.colorForecastPeriod;
            //},
            //getColor_forecast_cumulative: function(){
            //
            //    if (this.colorForecastCummulative === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-forecast-c").css("color");
            //        tempDiv.remove();
            //        this.colorForecastCummulative = color;
            //    }
            //
            //    return this.colorForecastCummulative;
            //},
            //getColor_budget_class1: function(){
            //    if (this.colorBudgetClass1 === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-budget-cls-1").css("color");
            //        tempDiv.remove();
            //        this.colorBudgetClass1 = color;
            //    }
            //
            //    return this.colorBudgetClass1;
            //},
            //getColor_budget_class2: function(){
            //    if (this.colorBudgetClass2 === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-budget-cls-2").css("color");
            //        tempDiv.remove();
            //        this.colorBudgetClass2 = color;
            //    }
            //
            //    return this.colorBudgetClass2;
            //},
            //getColor_budget_class3: function(){
            //    if (this.colorBudgetClass3 === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-budget-cls-3").css("color");
            //        tempDiv.remove();
            //        this.colorBudgetClass3 = color;
            //    }
            //
            //    return this.colorBudgetClass3;
            //},
            //getColor_budget_class4: function(){
            //    if (this.colorBudgetClass4 === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-budget-cls-4").css("color");
            //        tempDiv.remove();
            //        this.colorBudgetClass4 = color;
            //    }
            //
            //    return this.colorBudgetClass4;
            //},
            //getColor_budget_class5: function(){
            //    if (this.colorBudgetClass5 === null)
            //    {
            //        var tempDiv =  $("<div>");
            //        var color = tempDiv.appendTo("body").addClass("clr-budget-cls-5").css("color");
            //        tempDiv.remove();
            //        this.colorBudgetClass5 = color;
            //    }
            //
            //    return this.colorBudgetClass5;
            //},
            //getCPISPIColor: function(cpi, spi){
            //
            //    if (cpi <= 0 && spi <= 0){
            //        // Project has not started
            //        return this.getColor_default();
            //    }
            //    else if ((cpi < this.Application.config.CPIBadCeiling && cpi > 0) || (spi < this.Application.config.SPIBadCeiling && spi > 0))
            //    {
            //        // Project has started and CPI or SPI is bad
            //        return this.getColor_bad();
            //    }
            //    else if ((cpi >= this.Application.config.CPIBadCeiling && cpi < this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIBadCeiling && spi < this.Application.config.SPIGoodFloor)){
            //        // Project has started CPI or SPI is at a warning level
            //        return this.getColor_warning();
            //    }
            //    else if ((cpi >= this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIGoodFloor))
            //    {
            //        // Both CPI and SPI and Good
            //        return this.getColor_good();
            //    }
            //    else
            //    {
            //        return this.getColor_good(); // Catch all - one of the values is good while the other is at a default i.e. project has not started
            //    }
            //},
            //getCPISPIColor_light: function(cpi, spi){
            //    if (cpi <= 0 && spi <= 0){
            //        // Project has not started
            //        return this.getColor_default_light();
            //    }
            //    else if ((cpi < this.Application.config.CPIBadCeiling && cpi > 0) || (spi < this.Application.config.SPIBadCeiling && spi > 0))
            //    {
            //        // Project has started and CPI or SPI is bad
            //        return this.getColor_bad_light();
            //    }
            //    else if ((cpi >= this.Application.config.CPIBadCeiling && cpi < this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIBadCeiling && spi < this.Application.config.SPIGoodFloor))
            //    {
            //        // Project has started CPI or SPI is at a warning level
            //        return this.getColor_warning_light();
            //    }
            //    else if ((cpi >= this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIGoodFloor))
            //    {
            //        // Both CPI and SPI are Good
            //        return this.getColor_good_light();
            //    }
            //    else
            //    {
            //        return this.getColor_good_light(); // Catch all - one of the values is good while the other is at a default i.e. project has not started
            //    }
            //},
            //getCPISPIMapSymbol: function(cpi, spi){
            //
            //    var fillColor = null;
            //    var outlineColor = null;
            //
            //    if ((cpi < this.Application.config.CPIBadCeiling && cpi > 0) || (spi < this.Application.config.SPIBadCeiling && spi > 0))
            //    {
            //        // Project has started and CPI or SPI is bad
            //        fillColor = this.getColor_bad_bright();
            //        outlineColor = this.getColor_bad();
            //    }
            //    else if ((cpi >= this.Application.config.CPIBadCeiling && cpi < this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIBadCeiling && spi < this.Application.config.SPIGoodFloor))
            //    {
            //        // Project has started and CPI or SPI is at a warning level
            //        fillColor =  this.getColor_warning_bright();
            //        outlineColor = this.getColor_warning();
            //    }
            //    else if ((cpi >= this.Application.config.CPIGoodFloor) || (spi >= this.Application.config.SPIGoodFloor))
            //    {
            //        // Both CPI and SPI are Good
            //        fillColor = this.getColor_good_bright();
            //        outlineColor = this.getColor_good();
            //    }
            //    else
            //    {
            //        // Project has not started
            //        fillColor = this.getColor_default_bright();
            //        outlineColor = this.getColor_default();
            //    }
            //
            //    return {
            //        color: outlineColor,
            //        fillColor: fillColor,
            //        weight: 1,
            //        opacity: 1,
            //        fillOpacity: 0.6
            //    };
            //},
            //getCPIMapSymbol: function(value){
            //
            //    var fillColor = null;
            //    var outlineColor = null;
            //
            //    if (value < this.Application.config.CPIBadCeiling && value > 0)
            //    {
            //        // Project has started and CPI is bad
            //        fillColor = this.getColor_bad_bright();
            //        outlineColor = this.getColor_bad();
            //    }
            //    else if (value >= this.Application.config.CPIBadCeiling && value < this.Application.config.CPIGoodFloor)
            //    {
            //        // Project has started and CPI is at a warning level
            //        fillColor =  this.getColor_warning_bright();
            //        outlineColor = this.getColor_warning();
            //    }
            //    else if (value >= this.Application.config.CPIGoodFloor)
            //    {
            //        // CPI is Good
            //        fillColor = this.getColor_good_bright();
            //        outlineColor = this.getColor_good();
            //    }
            //    else
            //    {
            //        // Project has not started
            //        fillColor = this.getColor_default_bright();
            //        outlineColor = this.getColor_default();
            //    }
            //
            //    return {
            //        color: outlineColor,
            //        fillColor: fillColor,
            //        weight: 1,
            //        opacity: 1,
            //        fillOpacity: 0.6
            //    };
            //},
            //getSPIMapSymbol: function(value){
            //
            //    var fillColor = null;
            //    var outlineColor = null;
            //
            //    if (value < this.Application.config.SPIBadCeiling && value > 0)
            //    {
            //        // Project has started and SPI is bad
            //        fillColor = this.getColor_bad_bright();
            //        outlineColor = this.getColor_bad();
            //    }
            //    else if (value >= this.Application.config.SPIBadCeiling && value < this.Application.config.SPIGoodFloor)
            //    {
            //        // Project has started and SPI is at a warning level
            //        fillColor =  this.getColor_warning_bright();
            //        outlineColor = this.getColor_warning();
            //    }
            //    else if (value >= this.Application.config.SPIGoodFloor)
            //    {
            //        // SPI is Good
            //        fillColor = this.getColor_good_bright();
            //        outlineColor = this.getColor_good();
            //    }
            //    else
            //    {
            //        // Project has not started
            //        fillColor = this.getColor_default_bright();
            //        outlineColor = this.getColor_default();
            //    }
            //
            //    return {
            //        color: outlineColor,
            //        fillColor: fillColor,
            //        weight: 1,
            //        opacity: 1,
            //        fillOpacity: 0.6
            //    };
            //},
            //getCurrentBudgetMapSymbology: function(classBreaks, BAC){
            //
            //    // We go backwards in the class breaks i.e. 4-0 and check if there is a break before
            //    // applying the condition
            //
            //    if (BAC > 7000000)
            //    {
            //        var s = null;
            //    }
            //    if (classBreaks[0].BreakValue > 0 && (BAC >= 0 && BAC <= classBreaks[0].BreakValue)){
            //
            //            return {
            //                color: this.getColor_budget_class5(),
            //                fillColor: this.getColor_budget_class5(),
            //                weight: 1,
            //                opacity: 1,
            //                fillOpacity: 0.6
            //            };
            //
            //    }
            //    else if (classBreaks[1].BreakValue > 0 && (BAC > classBreaks[0].BreakValue && BAC <= classBreaks[1].BreakValue)) {
            //
            //            return {
            //                color: this.getColor_budget_class4(),
            //                fillColor: this.getColor_budget_class4(),
            //                weight: 1,
            //                opacity: 1,
            //                fillOpacity: 0.6
            //            };
            //
            //    }
            //    else if (classBreaks[2].BreakValue > 0 && (BAC > classBreaks[1].BreakValue && BAC <= classBreaks[2].BreakValue)) {
            //
            //            return {
            //                color: this.getColor_budget_class3(),
            //                fillColor: this.getColor_budget_class3(),
            //                weight: 1,
            //                opacity: 1,
            //                fillOpacity: 0.6
            //            };
            //
            //    }
            //    else if (classBreaks[3].BreakValue > 0 && (BAC > classBreaks[2].BreakValue && BAC <= classBreaks[3].BreakValue)) {
            //
            //            return {
            //                color: this.getColor_budget_class2(),
            //                fillColor: this.getColor_budget_class2(),
            //                weight: 1,
            //                opacity: 1,
            //                fillOpacity: 0.6
            //            };
            //
            //    }
            //    else if (classBreaks[4].BreakValue > 0 && (BAC > classBreaks[3].BreakValue && BAC <= classBreaks[4].BreakValue)) {
            //
            //            return {
            //                color: this.getColor_budget_class1(),
            //                fillColor: this.getColor_budget_class1(),
            //                weight: 1,
            //                opacity: 1,
            //                fillOpacity: 0.6
            //            };
            //
            //    }
            //    else{
            //        return {
            //            color: 'red',
            //            fillColor: 'red',
            //            weight: 1,
            //            opacity: 1,
            //            fillOpacity: 0.6
            //        };
            //    }
            //
            //
            //
            //    // need to know number of breaks and the values for each ???
            //    // then construct the conditionals statements
            //    // then check the current budget number against the statements
            //
            //        // if value < budget class 1 --> highest value (darkest color)
            //
            //        // if value >= budget class 1 and value < budget class 2
            //
            //        // if value >= budget class 2 and value < budget class 3
            //
            //        // if value >= budget class 3 and value < budget class 4
            //
            //        // if value >= budget class 4 and value < budget class 5
            //
            //        // else --> lowest value (lightest color)
            //
            //
            //},
            //getSPIBadge: function(value){
            //
            //    if (value < this.Application.config.SPIBadCeiling)
            //    {
            //        return "<i class='fa fa-exclamation-circle k-grid-td-icon clr-bad'></i>";
            //    }
            //    else if (value >= this.Application.config.SPIBadCeiling && value < this.Application.config.SPIGoodFloor){
            //        return "<i class='fa fa-exclamation-circle k-grid-td-icon clr-warning'></i>";
            //    }
            //    else if (value >= this.Application.config.SPIGoodFloor)
            //    {
            //        return "<i class='fa fa-check-circle k-grid-td-icon clr-good'></i>";
            //    }
            //    else
            //    {
            //        return "<i class='fa fa-circle-o k-grid-td-icon'>";
            //    }
            //},
            //getCPIColor: function(value){
            //
            //    if (value <= 0)
            //    {
            //        // Project has not started
            //        return this.getColor_default();
            //    }
            //    else if (value < this.Application.config.CPIBadCeiling && value > 0)
            //    {
            //        // Project has started and CPI is bad
            //        return this.getColor_bad();
            //    }
            //    else if (value >= this.Application.config.CPIBadCeiling && value < this.Application.config.CPIGoodFloor)
            //    {
            //        // Project has started and CPI is at a warning level
            //        return this.getColor_warning();
            //    }
            //    else if (value >= this.Application.config.CPIGoodFloor)
            //    {
            //        // CPI is Good
            //        return this.getColor_good();
            //    }
            //    else
            //    {
            //        return this.getColor_good(); // Catch all i.e. project has not started
            //    }
            //},
            //getCPIBadge: function(value){
            //
            //    if (value < this.Application.config.CPIBadCeiling)
            //    {
            //        return "<i class='fa fa-exclamation-circle k-grid-td-icon clr-bad'></i>";
            //    }
            //    else if (value >= this.Application.config.CPIBadCeiling && value < this.Application.config.CPIGoodFloor){
            //        return "<i class='fa fa-exclamation-circle k-grid-td-icon clr-warning'></i>";
            //    }
            //    else if (value >= this.Application.config.CPIGoodFloor)
            //    {
            //        return "<i class='fa fa-check-circle k-grid-td-icon clr-good'></i>";
            //    }
            //    else
            //    {
            //        return "<i class='fa fa-circle-o k-grid-td-icon'>";
            //    }
            //}
        });
    });