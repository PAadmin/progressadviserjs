//http://mrbool.com/backbone-js-router/28001
define(['backbone', 'marionette'], function(Backbone, Marionette) {
    return Backbone.Marionette.AppRouter.extend({
       //"index" must be a method in AppRouter's controller
       appRoutes: {
           "": "district",
           "district": "district",
           'district?*queryString' : 'district'

       },
       // GA - To track our application routes
       initialize: function() {
           this.bind('route', this._pageView);

           this.route(/^(.*?)\/changelog/, "changelog");
       },
       _pageView: function() {
           var path = Backbone.history.getFragment();
           ga('send', 'pageview', {page: "/" + path});
       }
    });
});