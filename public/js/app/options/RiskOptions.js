define(["jquery", "backbone",  "../custom/Class", 'enums/AppEnums'],
    function($, Backbone, Class, AppEnums) {

        return Class.extend({
            initialize: function() {

            },
            riskFilterType: null // Determines which risk filter to use for the view
        });
    });