define(["jquery", "backbone",  "../custom/Class", 'enums/AppEnums'],
    function($, Backbone, Class, AppEnums) {

        return Class.extend({
            initialize: function() {

            },
            dataText: null,
            dataInfo: null,
            dataPercent: null,
            dataStartdegree: null,
            dataFgcolor: null,
            labelText: null
        });
    });