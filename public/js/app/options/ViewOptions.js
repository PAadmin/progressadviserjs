define(["jquery", "backbone",  "../custom/Class", 'enums/AppEnums'],
    function($, Backbone, Class, AppEnums) {

        return Class.extend({
            initialize: function() {

            },
            viewDisplay: AppEnums.displaySelectedId, // Determines the behavior for the view
            live: true,                                 // Let's the view know if it needs to listen for change events
            viewModel: null,                            // A model for the view i.e. if display static information then it needs to be provided
            link: null                                  // Drill through or hyperlink for the view

        });
    });