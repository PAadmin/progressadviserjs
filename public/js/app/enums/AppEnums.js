define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(options) {

            },
            KendoFilterEnum: {
                returnAllPeriods: "returnAllPeriods",
                returnAllPeriodsWithinDateRange: "returnAllPeriodsWithinDateRange",
                returnAllPeriodsWithinDefaultDateRange: "returnAllPeriodsWithinDefaultDateRange",
                returnCurrentPeriod: "returnCurrentPeriod"
            },
            //KendoViewDisplayEnum: {
            //    displayTopWBSId: "displayTopWBSId",
            //    displaySelectedWBSId: "displaySelectedWBSId",
            //    displaySelectedWBSIdChildren: "displaySelectedWBSIdChildren",
            //    displayActiveFitler: "displayActiveFilter",
            //    displayProjects: "displayProjects",
            //    displayViewModel: "displayViewModel"
            //},
            KendoPresetFilterEnum: {
                notStarted: "Not Started",
                behindSchedule: "Behind Schedule",
                overBudget: "Over Budget",
                atRisk: "At Risk (Warning CPI or SPI)",
                poorSPI: "Poor SPI",
                warningSPI: "Warning SPI",
                goodSPI: "Good SPI",
                poorCPI: "Poor CPI",
                warningCPI: "Warning CPI",
                goodCPI: "Good CPI",
                highRisk: "High Risks (Assigned to Project)",
                mediumRisk: "Medium Risks (Assigned to Project)",
                lowRisk: "Low Risks (Assigned to Project)"
            },
            MapDisplayEnum: {
                displayCPISPI: "displayCPISPI",
                displayCPI: "displayCPI",
                displaySPI: "displaySPI",
                displayCurrentBudget: "displayCurrentBudget",
                displayPlannedValue: "displayPlannedValue",
                displayActualCost: "displayActualCost"
            },
            Basemap: {
                Streets: 'Streets',
                Topographic: 'Topographic',
                NationalGeographic: 'NationalGeographic',
                Oceans: 'Oceans',
                Gray: 'Gray',
                DarkGray: 'DarkGrayImagery',
                Imagery: 'Imagery',
                ShadedRelief: 'ShadedRelief'
            },
            BasemapLabels: {
                OceansLabels: 'OceansLabels',
                GrayLabels: 'GrayLabels',
                DarkGrayLabels: 'DarkGrayLabels',
                ImageryLabels: 'ImageryLabels',
                ImageryTransportation: 'ImageryTransportation',
                ShadedReliefLabels: 'ShadedReliefLabels'
            }
        });
    });