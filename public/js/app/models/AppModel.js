define(["jquery", "backbone", "jquery.cookie"],
    function ($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            Application: null,

            // Default values for all of the Model attributes
            defaults: {
                // Breadcrumbs
                "breadcrumbs": [],

                // App State

                //"id": null,          // Indicates the selected school
                "centroid": null,

                appBannerVisible: true,
                userSetAppBanner: false,
                districtTab: "",
                //fieldTab: "",
                districtNum: null,
                //permittee: null,

                selectedSchoolId: null,
                selectedBuildingId: null,
                selectedRoomId: null,

                basemapLayer: "Imagery"            // Default basemap layer: Gray, Topographic, Imagery


            },
            initialize: function (App) {
                // List of events

                // Breadcrumbs
                //this.bind("change:breadcrumbs", this.onBreadcrumbChanged);

                // App State
                this.bind("change:appBannerVisible", this.onAppBannerVisibleChanged);
                //this.bind("change:userSetAppBanner", this.onUserSetAppBanner);
                this.bind("change:districtTab", this.onDistrictTabChanged);
                this.bind("change:fieldTab", this.onFieldTabChanged);
                this.bind("change:districtNum", this.ondistrictNumChanged);
                this.bind("change:permittee", this.onPermitteeChanged);
                this.bind("change:selectedSchoolId", this.onSelectedSchoolIdChanged);
                this.bind("change:selectedSchoolIds", this.onSelectedSchoolIdsChanged);
                this.bind("change:selectedBuildingId", this.onSelectedBuildingIdChanged);
                this.bind("change:selectedBuildingIds", this.onSelectedBuildingIdsChanged);
                this.bind("change:selectedRoomId", this.onSelectedRoomIdChanged);
                this.bind("change:selectedRoomIds", this.onSelectedRoomIdsChanged);
                this.bind("change:basemapLayer", this.onBasemapLayerChanged);

                this.bind("change:centroid", this.onCentroidChanged);
                //this.bind("change:id", this.onIdChanged);

                // Filters
                //App.model.bind("change:nameFilter", nameFilterChangeHandler);
                this.Application = App;

                // Restore state
                var districtNum = $.cookie('CRC:AppModel:districtNum');
                var permittee = $.cookie('CRC:AppModel:permittee');
                if (districtNum) {
                    if (districtNum === "null") {
                        this.set("districtNum", null);
                        this.set("permittee", null);
                    }
                    else {
                        this.set("districtNum", districtNum);
                        this.set("permittee", permittee);
                    }
                }

                // Restore state
                var centroid = $.cookie('PMP2:AppModel:centroid');
                var id = $.cookie('PMP2:AppModel:selectedSchoolId');
                if (id) {
                    if (id === "null") {
                        this.set("selectedSchoolid", null);
                        this.set("centroid", null);
                    }
                    else
                        this.set("selectedSchoolId", id);
                    if (centroid)
                        this.set("centroid", centroid);
                }

                var basemapLayer = $.cookie("PMP2:AppModel:basemapLayer");
                if (typeof basemapLayer === 'undefined' || basemapLayer === "null")
                    this.set("basemapLayer", "Imagery"); //Topographic, Gray, Streets, Imagery
                else
                    this.set("basemapLayer", basemapLayer);
                console.log('****** AppModel:init:basemapLayer:' + this.get("basemapLayer"));


            },
            //**************************************************************
            //* Model Triggers - events fired when properties on the model
            //                   change...these events generally have
            //                   state associated with them
            //**************************************************************

            //onBreadcrumbChanged: function (e) {
            //    console.log('****** AppModel:onBreadcrumbChanged:' + this.get("breadcrumbs"));
            //
            //    this.Application.vent.trigger(this.Application.Events.ModelEvents.BreadcrumbChanged, this.get('breadcrumbs'));
            //},

            onAppBannerVisibleChanged: function () {

                $.cookie('CRC:AppModel:appBannerVisible', this.get("appBannerVisible"));

                this.Application.vent.trigger(this.Application.Events.ModelEvents.AppBannerVisibilityChanged, this.get('appBannerVisible'));
            },
            onDistrictTabChanged: function () {
                this.Application.vent.trigger(this.Application.Events.ModelEvents.DistrictTabChanged, this.get('districtTab'));
            },
            onFieldTabChanged: function () {
                this.Application.vent.trigger(this.Application.Events.ModelEvents.FieldTabChanged, this.get('fieldTab'));
            },
            onDistrictNumChanged: function () {

                console.log('****** AppModel:onDistrictNumChanged:' + this.get("districtNum"));

                $.cookie('CRC:AppModel:districtNum', this.get("districtNum"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.districtNumChanged, this.get('districtNum'));
            },
            onPermitteeChanged: function () {

                console.log('****** AppModel:onPermitteeChanged:' + this.get("permittee"));

                $.cookie('CRC:AppModel:permittee', this.get("permittee"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.PermitteeChanged, this.get('permittee'));

            },
            onSelectedSchoolIdChanged: function () {

                console.log('****** AppModel:onSelectedSchoolIdChanged:' + this.get("selectedSchoolId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedSchoolIdChanged, this.get('selectedSchoolId'));

            },
            onSelectedBuildingIdChanged: function () {

                console.log('****** AppModel:onSelectedBuildingIdChanged:' + this.get("selectedBuildingId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedBuildingIdChanged, this.get('selectedBuildingId'));

            },
            onSelectedRoomIdChanged: function () {

                console.log('****** AppModel:onSelectedRoomIdChanged:' + this.get("selectedRoomId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedRoomIdChanged, this.get('selectedRoomId'));

            },
            onCentroidChanged: function (event) {

                $.cookie('PMP2:AppModel:centroid', this.get("centroid"));

                this.Application.vent.trigger(this.Application.Events.ModelEvents.CentroidChanged, "centroid");

            },
            onBasemapLayerChanged: function(){

                $.cookie('PMP2:AppModel:basemapLayer', this.get("basemapLayer"));

                this.Application.vent.trigger(this.Application.Events.ModelEvents.BasemapLayerChanged, this.get('basemapLayer'));

            },

            //**************************************************************
            //* Event Triggers - trigger all events through this method...
            //*                  it is refined to events that do not have
            //*                  state enforcing App Singleton and MVC rules
            //**************************************************************
            triggerEvent: function (event, model) {

                // Prevent model changes from firing events using this method...i.e.
                // ensure values are changed on the model prior to firing events
                for (var key in this.Application.Events.ModelEvents) {
                    if (key == event) {
                        throw new UserException("Invalid Event Path!");
                    }
                }

                this.Application.vent.trigger(event, model);
            },
            //**************************************************************
            //* Methods
            //**************************************************************

            startBreadcrumb: function (crumb) {
                //this.set('breadcrumbs', []);
                if (crumb) {
                    this.set('breadcrumbs', [crumb]);
                }
                else {
                    this.set('breadcrumbs', []);
                }
            },
            pushBreadcrumb: function (crumb) {

                var arr = _.clone(this.get('breadcrumbs'));

                if (!_.contains(arr, crumb))
                    arr.push(crumb);
                else {
                    var stepBackArray = [];
                    for (var i = 0; i < arr.length; i++) {
                        stepBackArray.push(arr[i]);

                        if (arr[i] === crumb)
                            break;
                    }
                    arr = stepBackArray;
                }
                this.set('breadcrumbs', arr);
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function (attrs) {
                //console.log("--------------- AppModel:validate ---------------");
            }

        });

        // Returns the Model class
        return Model;
    }
);