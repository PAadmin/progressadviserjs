define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('SchoolModel:initialize');

            },

            // Default values for all of the Model attributes
            defaults: {

                // PUR data
                //site_id: null,
                //pk_site_id: null,
                //permit_num: null,
                //permittee: null,
                //mtrs: null,
                //crop_list: null,
                //site_name:null,
                //calc_Acres: null,
                //isEditable: false,
                //county_num: null,
                //county_name: null

                ID: null,
                Long: null,
                Lat: null,
                CDSCODE: null,
                ZIP: null,
                SHORTNAME: null,
                DISTRICT:null,
                SCHOOL: null,
                STREET: null,
                CITY: null,
                OPENDATE: null,
                CHARTER: null,
                DOCTYPE: null,
                SOCTYPE: null,
                isEditable: false,
                status:null
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);