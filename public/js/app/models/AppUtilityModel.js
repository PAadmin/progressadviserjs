define( ['App', 'backbone', 'marionette', 'jquery'],
    function(App, Backbone, Marionette, $) {
        return Backbone.Model.extend( {


            getBaseUrl: function(){

                var url = location.href;  // entire url including querystring - also: window.location.href;
                var baseURL = url.substring(0, url.indexOf('/', 14));

                // Base Url for localhost
                var pathname = location.pathname;  // window.location.pathname;
                var index1 = url.indexOf(pathname);
                var index2 = url.indexOf("/", index1 + 1);
                var baseLocalUrl = url.substr(0, index2);

                var path = baseLocalUrl + "/"; // url plus app name
                if (baseURL.indexOf('http://localhost') != -1) {
                    // Split the path name and use the first two entries on localhost i.e. app/public
                    var pathParts = pathname.split("/");
                    path = baseLocalUrl + "/" + pathParts[2];
                }

                return path;
            }
        });
    });