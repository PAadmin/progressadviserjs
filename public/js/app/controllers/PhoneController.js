define(['App', 'backbone', 'marionette', 'views/PhoneHeaderView', 'views/PhoneFooterView', 'views/HomeView','views/DistrictTabLayoutView'],
    function (App, Backbone, Marionette, HeaderView, FooterView,HomeView,DistrictTabLayoutView) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {

            $("body").css('overflow', '');
            $("#crc").removeAttr('style');
            document.ontouchmove = function(e){ return true; };

            // Setup the header and footer
            $("#loading-error-region").empty();
            App.headerRegion.show(new HeaderView());

            // Footer view is used to directly update the BreadCrumbView as
            // the event trigger gets severed after a few calls likely caused
            // by elements being added and removed from the DOM
            this.footerView = new FooterView();
            App.footerRegion.show(this.footerView);

        },
        index:function () {
            console.log('PhoneController:index');

            // Start Crumb
            App.model.startBreadcrumb('Home');
            this.footerView.updateBreadcrumb();

            // Reset the layout
            this.removeLayoutClasses();

            // Update the nav bar
            this.updateNavBar(Backbone.history.fragment);

            App.mainRegion.show(new HomeView());

        },
        district: function(){
            console.log('PhoneController:district');

            // Update containers for scrolling and background
            //this.resetMarkupContainers();

            // Start Breadcrumb
            App.model.startBreadcrumb('FIT');
            App.model.pushBreadcrumb('District');
            this.footerView.updateBreadcrumb();

            // Reset the layout
            this.removeLayoutClasses();

            // Update the nav bar
            this.updateNavBar(Backbone.history.fragment);

            // Setup the main container
            //$("#main-region").addClass("container-fluid-tight");

            App.mainRegion.show(new DistrictTabLayoutView());

            // Store this route/previous route for some conditional breadcrumb items
            this.previousRoute = Backbone.history.fragment;
        },
        updateNavBar:function(route){

            // TODO: Determine if there is a better way to do this...
            if (route === '')
                route = 'index';

            $el = $('#nav-' + route);


            // If current route is highlighted, we're done.
            if ($el.hasClass('current_page_item')) {
                return;
            } else {
                // Unhighlight active tab.
                $('#main-menu li.current_page_item').removeClass('current_page_item');
                // Highlight active page tab.
                $el.addClass('current_page_item');
            }

            $('#nav-programmenu').removeClass('current_page_item');

        },
        removeLayoutClasses:function(){
            $("#main-region").removeClass("container-fluid");
            $("#main-region").removeClass("container-full-map");
            $("#main-region").removeClass("container-padded-home");
            $("#main-region").removeClass("container-fluid-tight");
            $("#main-region").removeClass("container-risk-layout");
            $("#main-region").height(0);
        }
    });
});