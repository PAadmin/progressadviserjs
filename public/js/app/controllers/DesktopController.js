define(['App', 'backbone', 'marionette', 'views/HeaderView', 'views/FooterView', 'views/HomeView','views/DistrictTabLayoutView'],
    function (App, Backbone, Marionette, HeaderView, FooterView, HomeView,DistrictTabLayoutView ) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {

            $("body").css('overflow', '');
            $("#crc").removeAttr('style');
            if (App.mobile) {
                $("#loading-error-region").empty();
                document.ontouchmove = function(e){ return true; };
            }

            // Setup the header and footer
            $("#loading-error-region").empty();
            App.headerRegion.show(new HeaderView());

            // Footer view is used to directly update the BreadCrumbView as
            // the event trigger gets severed after a few calls likely caused
            // by elements being added and removed from the DOM
            this.footerView = new FooterView();
            App.footerRegion.show(this.footerView);

        },
        resetMarkupContainers: function(){
            $("body").css('overflow-x', '');
            $("body").css('overflow-y', '');
            $("body").css('overflow', '');
            $("html").removeAttr('style');
            $("#crc").removeAttr('style');
        },
        //gets mapped to in AppRouter's appRoutes
        index:function () {
            console.log('DesktopController:index');

            // Update containers for scrolling and background
            this.resetMarkupContainers();
            $("body").css('overflow-y', 'auto');
            $("body").css('overflow-x', 'hidden');
            $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
            $("#crc").css('background-color', '#FFFFFF');

            // Start Crumb
            App.model.startBreadcrumb('Home');
            this.footerView.updateBreadcrumb();

            // Reset the layout
            this.removeLayoutClasses();

            // Update the nav bar
            this.updateNavBar(Backbone.history.fragment);

            App.mainRegion.show(new HomeView());

            // Store this route/previous route for some conditional breadcrumb items
            this.previousRoute = Backbone.history.fragment;
        },
        district: function(queryString){
            console.log('DesktopController:district');

            var params = this.parseQueryString(queryString);
            this.districtNum = params.districtNum;

            if (this.districtNum) {
                App.model.set('districtNum', this.districtNum);
                this.startSurvey();
            }
            else {

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                App.model.startBreadcrumb('FIT');
                App.model.pushBreadcrumb('District');
                this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("container-fluid-tight");

                App.mainRegion.show(new DistrictTabLayoutView());

                // Store this route/previous route for some conditional breadcrumb items
                this.previousRoute = Backbone.history.fragment;
            }
        },
        updateNavBar:function(route){

            if (route === '')
                route = 'index';

            if (route.indexOf('?') > -1) {
                route = route.slice(0,route.indexOf('?'));
            }
            $el = $('#nav-' + route);

            // If current route is highlighted, we're done.

            if (route != 'district') {
                if ($el.hasClass('current_page_item')) {
                    return;
                } else {
                    // Unhighlight active tab.
                    $('#main-menu li.current_page_item').removeClass('current_page_item');
                    // Highlight active page tab.
                    $el.addClass('current_page_item');
                }
            }
            else{
                if ($el.hasClass('current_sub_page_item')) {
                    return;
                } else {
                    // Unhighlight active tab.
                    $('#main-menu li.current_sub_page_item').removeClass('current_sub_page_item');
                    $('#main-menu li.current_page_item').removeClass('current_page_item');

                    // Highlight active page tab.
                    $el.addClass('current_sub_page_item');
                }
            }

            // Highlight the menu if applicable...
            //console.log(route);
            //route === 'dashboard' || route === 'projectfilters' ||
            if (route === 'district'){
                if ($('#nav-programmenu').hasClass('current_page_item'))
                    return;

                $('#nav-programmenu').addClass('current_page_item');
                $('#nav-programmenu-text').addClass('current_page_item_text');

            }
            else{
                $('#nav-programmenu').removeClass('current_page_item');
            }
        },
        removeLayoutClasses:function(){
            $("#main-region").removeClass("container-fluid");
            $("#main-region").removeClass("container-full-map");
            $("#main-region").removeClass("container-padded-home");
            $("#main-region").removeClass("container-fluid-tight");
            $("#main-region").removeClass("container-risk-layout");
            $("#main-region").height(0);
        },
        parseQueryString: function(queryString){
            var params = {};
            if(queryString){
                _.each(
                    _.map(decodeURI(queryString).split(/&/g),function(el,i){
                        var aux = el.split('='), o = {};
                        if(aux.length >= 1){
                            var val;
                            if(aux.length == 2)
                                val = aux[1];
                            o[aux[0]] = val;
                        }
                        return o;
                    }),
                    function(o){
                        _.extend(params,o);
                    }
                );
            }
            return params;
        },
        startSurvey: function (e) {

            var self = this;

            console.log('DesktopController:startSurvey:districtNum:' + this.districtNum);

            var dataSource = new kendo.data.DataSource({
                autoBind: false,
                type: "odata",
                transport: {
                    read: {
                        url:  App.config.DataServiceURL + "/odata/RiceField",
                        dataType: "json"
                    }
                },
                schema: {
                    data: "value",
                    total: function (data) {
                        return data['odata.count'];
                    },
                    model: {
                        id: "OBJECTID"
                    }
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                requestStart: function () {
                    //kendo.ui.progress($("#projectWBSLoading"), true);
                },
                requestEnd: function () {
                    //kendo.ui.progress($("#projectWBSLoading"), false);
                },
                filter: { field: "permit_num", operator: "eq", value: this.districtNum.toString()}
            });

            var thisView = this;
            dataSource.fetch(function(){
                var data = this.data();
                var count = data.length;

                if (count === 0){
                    console.log('DesktopController:startSurvey:districtNumNotFound');
                    //self.createAutoClosingAlert($("#startError"), "<strong>Failure.</strong> Permit Number not found.  The permit number should be 7 digits long with no dashes <strong>(format: XXXXXXX)</strong>");
                }
                else {
                    console.log('DesktopController:startSurvey:SetdistrictNum:' + self.districtNum);

                    var permittee = null;
                    var i = 0;
                    while (permittee === null || permittee === undefined || permittee === ""){
                        permittee = data[i].permittee;
                        i++;
                    }
                    console.log('HomeView:StartButtonClicked:permittee:' + permittee);

                    App.model.set('districtNum', self.districtNum);
                    App.model.set('permittee', permittee);

                    // Go to beginning of survey
                    window.location.href = "#district";

                }
            });
        }
    });
});