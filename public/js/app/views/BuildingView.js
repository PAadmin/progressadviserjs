define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/building', 'models/BuildingModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #buildingRow': 'rowClicked'
            },
            initialize: function () {
                console.log('BuildingView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- BuildingView:onShow --------');

                this.id = this.model.get('id');
                this.name = this.model.get('name');
                this.status = this.model.get('status');

                this.selectedBuildingId = App.model.get('selectedBuildingId');
                if (this.selectedBuildingId === null || this.selectedBuildingId === undefined) {
                    this.selectedBuildingId  = this.id;
                }

                this.$('#buildingValue').html("<b>Building: </b>" + this.name); //+ "<br>Other: other"
                this.$('#buildingValue').val(this.id);

                if (this.status === "Complete") {
                    this.$('#buildingRow').css("background-color","#c1e2b3");  // #dff0d8 lightest green,  #c1e2b3 light green
                } else if (this.status === "InProgress") {
                    this.$('#buildingRow').css("background-color", "#f7ecb5");  // #fcf8e3 lightest yellow, #fcf8e3 light yellow, light blue #afd9ee
                }


            },
            rowClicked: function (e) {
                console.log('BuildingView:rowClicked');

                // If not crtl or shift pressed, de-select all rows
                var event = e || window.event;
                if (event.ctrlKey || event.shiftKey) {

                }
                else {
                    $('.building-row').removeClass('k-state-selected');
                }

                // Select or deselect this row
                var selected = true;
                if (!this.$('.building-row').hasClass('k-state-selected')) {
                    //this.$('.building-row').removeClass('field-value-red');
                    this.$('.building-row').addClass('k-state-selected');

                    $('#buildingLabel').html("Current Building: " + this.name);
                    $('#roomLabel').html(", Current Room: None");

                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(100% - 46px)');
                    $('#top-right-panel-body').hide();
                    $('#top-right-panel').height('33px');

                }
                else {
                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(50% - 4px)');
                    $('#top-right-panel-body').show();
                    $('#top-right-panel').height('calc(50% - 7px)');

                    selected = false;
                    this.$('.building-row').removeClass('k-state-selected');
                }

                if (selected) {

                    console.log('BuildingView:rowClicked:selectedSchoolId:' + this.id);

                    App.model.set('selectedBuildingId', this.id);
                }
                else {
                    App.model.set('selectedBuildingId', null);

                }

            },
            onResize: function () {
//                console.log('AdminProjectRiskView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                console.log('BuildingView:resize');

            },
            remove: function () {
                console.log('-------- BuildingView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#buildinglRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
