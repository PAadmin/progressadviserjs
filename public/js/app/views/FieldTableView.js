define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/fieldTable', 'views/FieldGridView'],
    function(App, Backbone, Marionette, $, template,  FieldGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            regions: {
                fieldRegion1: "#field-table-region-1"
            },
            events: {
                'click #NextButton': 'nextButtonClicked',
                'click #SaveButton': 'saveButtonClicked'
            },
            initialize: function(){
                _.bindAll(this);
            },
            onRender: function(){
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('fieldTableView:onShow:' + FieldGridView);

                this.fieldRegion1.show(new FieldGridView());
            },

            resize: function(){
                console.log('fieldView:resize');

                if (this.fieldRegion1.currentView)
                    this.fieldRegion1.currentView.resize();

            }
        });
    });