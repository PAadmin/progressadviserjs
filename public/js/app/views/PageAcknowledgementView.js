define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/pageAcknowledgement'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            events: {
                'click #AcknowledgeInstructions': 'onAcknowledgeClicked'
            },
            initialize: function(options){
                console.log('PageAcknowledgementView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                this.districtNum = App.model.get('districtNum');

                this.getData();

            },
            onRender: function(){
//                console.log('PageAcknowledgementView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- PageAcknowledgementView:onShow ----------');

                // If custom text inputted, use that
                if (this.options){
                    this.$('#errorText').html(this.options.instructions);
                }
            },
            getData: function () {

                var self = this;

                if (self.districtNumber !== null && self.districtNumber !== undefined) {
                    self.districtNum = self.districtNum.toString();
                }

                this.dataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/WholeFarmTemp",
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {

                                console.log('PageAcknowledgementView:update: url');
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp" + "(" + data.Id + ")";

                            }
                        },
                        create: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp";
                            },
                            dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp" + "(" + data.Id + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                districtNumber: {type: "string", validation: {required: true}},
                                LandownerAwareWDR: {type: "string", validation: {required: true}},
                                LandownerCopyWDR: {type: "string", validation: {required: true}},
                                LocationsWaterLeave: {type: "string", validation: {required: true}},
                                LocationsWaterLeaveMarked: {type: "string", validation: {required: true}},
                                LocationsWaterDrains: {type: "string", validation: {required: true}},
                                FarmMapFilledOut: {type: "string", validation: {required: true}},
                                LandownerAcknowledgementSeen: {type: "string", validation: {required: true}}
                            }
                        }
                    },
                    requestStart: function () {
                        console.log('PageAcknowledgementView:dataSource:requestStart');

                    },
                    requestEnd: function () {
                        console.log('PageAcknowledgementView:dataSource:requestEnd');

                    },
                    error: function () {
                        console.log('PageAcknowledgementView:dataSource:error');

                    },
                    change: function () {
                        console.log('PageAcknowledgementView:dataSource:change');

                        var data = this.data(); //self.dataSource.data();
                        if (data.length <= 0) {
                            console.log('PageAcknowledgementView:getData:noData');

                        }
                    },
                    batch: false,
                    pageSize: 50,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    filter: {field: "districtNumber", operator: "eq", value: self.districtNum.toString()}
                });
            },
            onAcknowledgeClicked: function(){
//                console.log('PageAcknowledgementView:onAcknowledgeClicked');

                var self = this;

                this.dataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {

                        var farmRecord = self.dataSource.at(0);
                        farmRecord.set(self.options.field, "Yes");

                        console.log('PageAcknowledgementView:' + self.options.field + ':Yes');

                        $.when(self.dataSource.sync()).done(function () {
                            console.log('PageAcknowledgementView:dataSource:sync done');
                        });
                    }

                });

                this.close();
            },
            onResize: function(){
//                console.log('PageAcknowledgementView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log('PageAcknowledgementView:resize');

                // TODO: Add your code

            },
            remove: function(){
                console.log('---------- PageAcknowledgementView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                $('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });