define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/school', 'models/SchoolModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #schoolRow': 'rowClicked'
            },
            initialize: function () {
                console.log('SchoolView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- SchoolView:onShow --------');

                this.id = this.model.get('ID');
                this.name = this.model.get('SHORTNAME');
                this.lat = this.model.get('Lat');
                this.long = this.model.get('Long');
                this.status = this.model.get('status');

                var district = this.model.get('DISTRICT');
                var school = this.model.get('SCHOOL');
                var zip = Math.trunc(this.model.get('ZIP'));
                var cdscode = this.model.get('CDSCODE');
                var city = this.model.get('CITY');
                var street = this.model.get('STREET');

                //$('#fieldsLabel').text("Fields of: " + permittee + " (" + districtNum + ")");

                this.selectedSchoolId = App.model.get('selectedSchoolId');
                if (this.selectedSchoolId === null || this.selectedSchoolId === undefined) {
                    this.selectedSchoolId  = this.id;
                }
                //$('#fieldLabel').html("Current Field ID: " + this.selectedSchoolId);

                if (this.id !== null) {
                    this.$('#schoolValue').html("<b>School: </b>" + this.name + "<br>" + street + ", " + city + ", " + zip); // + "   -   Crop Year: " + cropYear);
                    this.$('#schoolValue').val(this.id);

                    if (this.status === "Complete") {
                        this.$('#schoolRow').css("background-color","#c1e2b3");  // #dff0d8 lightest green,  #c1e2b3 light green
                    } else if (this.status === "InProgress") {
                        this.$('#schoolRow').css("background-color", "#f7ecb5");  // #fcf8e3 lightest yellow, #fcf8e3 light yellow, light blue #afd9ee
                    }
                }

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onSelectedSchoolIdChanged, this);

            },
            rowClicked: function (e) {
                console.log('SchoolView:rowClicked:e' + e);

                // If not crtl or shift pressed, de-select all rows
                var event = e || window.event;
                if (event.ctrlKey || event.shiftKey) {

                }
                else {
                    $('.school-row').removeClass('k-state-selected');
                }

                // Select or deselect this row
                var selected = true;
                if (!this.$('.school-row').hasClass('k-state-selected')) {
                    //this.$('.school-row').removeClass('field-value-red');
                    this.$('.school-row').addClass('k-state-selected');

                    $('#schoolLabel').html("Current School: " + this.name);

                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(50% - 4px)');
                    $('#top-right-panel-body').show();
                    $('#top-right-panel').height('calc(50% - 7px)');

                }
                else {

                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(100% - 46px)');
                    $('#top-right-panel-body').hide();
                    $('#top-right-panel').height('33px');

                    selected = false;
                    this.$('.school-row').removeClass('k-state-selected');
                }

                // Set collection of SiteIds that are currently selected
                var selectedCells = $('.k-state-selected .field-value');

                if (selected) {

                    console.log('SchoolView:rowClicked:selectedSchoolId:' + this.id);

                    // Store the centroid for this SchoolId
                    App.model.set('centroid', this.long + "," + this.lat);

                    App.model.set('selectedSchoolId', this.id);
                    $('#schoolLabel').html("Current School: " + this.name);
                }
                else {
                    App.model.set('selectedSchoolId', null);

                    $('#schoolLabel').html("Current School: None");
                }
            },
            onSelectedSchoolIdChanged: function (args) {
                console.log('SchoolView:onSelectedSchoolIdChanged:' + args);

                var id = parseInt(App.model.get('selectedSchoolId'), 0);

                if (this.id == id) {

                    // Select or deselect this row
                    var selected = true;
                    if (!this.$('.school-row').hasClass('k-state-selected')) {
                        //this.$('.school-row').removeClass('field-value-red');
                        this.$('.school-row').addClass('k-state-selected');

                        $('#schoolLabel').html("Current School: " + this.name);

                        console.log('SchoolView:onSelectedSchoolIdChanged:selectedSchoolId:' + this.id);
                    }
                }
                else {
                    this.$('.school-row').removeClass('k-state-selected');
                }
            },
            onResize: function () {
//                console.log('AdminProjectRiskView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                console.log('SchoolView:resize');

            },
            remove: function () {
                console.log('-------- SchoolView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#schoolRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
