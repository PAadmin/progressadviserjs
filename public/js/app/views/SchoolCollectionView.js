define(['App', 'backbone', 'marionette', 'jquery', 'models/SchoolModel',
        'hbs!templates/schoolCollection', 'views/SchoolView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, SchoolModel, template, SchoolView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: SchoolView,
            events: {
                //'click #selectAllButton': 'selectAllButtonClicked',
                //'click #clearAllButton': 'clearAllButtonClicked'
            },
            initialize: function (options) {
                console.log('SchoolCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();

                var self = this;

                //this.districtNum = App.model.get('districtNum');
                //this.permittee = App.model.get('permittee');

                this.selectedSchoolId = App.model.get('selectedSchoolId');

                console.log('SchoolCollectionView:initialize:selectedSchoolId: ' + this.selectedSchoolId);

                this.getData();

            },
            onShow: function () {
                console.log("-------- SchoolCollectionView:onShow --------");
                //var buttonText = "";

                //if (!App.mobile) {
                //this.$('#qFieldCollection').attr('title', buttonText);
                //this.$('#qFieldCollection').tooltip();
                //}

            },

            getData: function () {


                // source school data
                //{
                //    "Long":-117.090879083392,
                //    "Lat":32.9336285890539,
                //    "CDSCODE":37683386112726.0,
                //    "DISTRICT":"San Diego Unified",
                //    "SCHOOL":"Dingeman Elementary",
                //    "STREET":"11840 Scripps Creek Drive",
                //    "CITY":"San Diego",
                //    "ZIP":92131.0,
                //    "OPENDATE":"1995-09-02T00:00:00",
                //    "CHARTER":"N",
                //    "DOCTYPE":"Unified School District",
                //    "SOCTYPE":"Elementary Schools (Public)",
                //    "GSOFFERED":null,
                //    "SHORTNAME":"Dingeman",
                //    "ID":9193,
                //    "PRIV":"N"
                //}

                this.schoolDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: "http://199.115.221.114/services/odata/SanDiegoSchool?$filter=indexof%28DISTRICT,%27San%20Diego%27%29%20gt%20-1",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {type: "number"},
                                Long: {type: "number"},
                                Lat: {type: "number"},
                                CDSCODE: {type: "number"},
                                ZIP: {type: "number"},
                                SHORTNAME: {type: "string"},
                                DISTRICT: {type: "string"},
                                SCHOOL: {type: "string", validation: {required: false}},
                                STREET: {type: "string", validation: {required: false}},
                                CITY: {type: "string", validation: {required: true}},
                                OPENDATE: {type: "string", validation: {required: false}},
                                CHARTER: {type: "string", validation: {required: false}},
                                DOCTYPE: {type: "string", validation: {required: false}},
                                SOCTYPE: {type: "string", validation: {required: false}}
                            }
                        }
                    },
                    batch: true,
                    pageSize: 217,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    sort: {field: "SHORTNAME", dir: "asc"},
                    change: this.onSchoolDataSourceChange,
                    requestStart: function () {
                        console.log('SchoolCollectionView:dataSource:requestStart');
                        kendo.ui.progress($("#schoolCollectionViewLoading"), true);
                        kendo.ui.progress($("#buildingCollectionViewLoading"), true);
                        kendo.ui.progress($("#roomCollectionViewLoading"), true);
                    },
                    requestEnd: function () {
                        console.log('SchoolCollectionView:dataSource:requestEnd');
                        kendo.ui.progress($("#schoolCollectionViewLoading"), false);
                        kendo.ui.progress($("#buildingCollectionViewLoading"), false);
                        kendo.ui.progress($("#roomCollectionViewLoading"), false);
                    },
                    error: function () {
                        console.log('SchoolCollectionView:dataSource:error');
                        kendo.ui.progress($("#schoolCollectionViewLoading"), false);
                        kendo.ui.progress($("#buildingCollectionViewLoading"), false);
                        kendo.ui.progress($("#roomCollectionViewLoading"), false);
                        //self.displayNoDataOverlay();
                    }

                });

                this.schoolDataSource.query();

            },
            onSchoolDataSourceChange: function (e) {
                console.log('SchoolCollectionView:onSchoolDataSourceChange');

                var self = this;

                self.collection.reset();
                self.collection.comparator = function (model) {
                    return model.get("ID");
                };

                var sourceData = this.schoolDataSource.data();
                //sourceData.sort('site_id');

                var fieldCount = sourceData.length;

                console.log('SchoolCollectionView:dataSource:fetch');
                kendo.ui.progress($("#schoolCollectionViewLoading"), true);
                kendo.ui.progress($("#buildingCollectionViewLoading"), true);
                kendo.ui.progress($("#roomCollectionViewLoading"), true);

                $.each(sourceData, function (index, value) {

                    // Need to get the data record and populate it
                    var model = new SchoolModel();

                    model.set("ID", value.ID);
                    model.set("Long", value.Long);
                    model.set("Lat", value.Lat);
                    model.set("CDSCODE", value.CDSCODE);
                    model.set("ZIP", value.ZIP);
                    model.set("SHORTNAME", value.SHORTNAME);
                    model.set("DISTRICT", value.DISTRICT);
                    model.set("SCHOOL", value.SCHOOL);
                    model.set("CITY", value.CITY);
                    model.set("STREET", value.STREET);

                    if (self.options)
                        model.set("isEditable", self.options.isEditable);
                    else
                        model.set("isEditable", false);

                    if((value.ID % 3) === 0)
                        model.set ("status","InProgress");
                    else if((value.ID % 2) === 0)
                        model.set ("status","Complete");

                    self.collection.push(model);

                });

                console.log('SchoolCollectionView:dataSource:fetch done');
                kendo.ui.progress($("#schoolCollectionViewLoading"), false);
                kendo.ui.progress($("#buildingCollectionViewLoading"), false);
                kendo.ui.progress($("#roomCollectionViewLoading"), false);

            },
            onResize: function () {
//                console.log('SchoolCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('SchoolCollectionView:resize');

                //TODO: Add your code
            },
            remove: function () {
                console.log('---------- SchoolCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
