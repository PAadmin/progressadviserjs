define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/phoneHeader'],
    function(App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            events:{
                'click #toggleBannerButton': 'onToggleBannerButtonClicked',
                'click #toggleFullScreenButton': 'onToggleFullScreenButtonClicked'
            },
            template: template,
            initialize: function(options) {
                console.log('HeaderView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                // Check to see if browser supports fullscreen...if not, hide the fullscreen button
                this.hideToggleFullScreenButton = false;
                if (document.fullscreenEnabled ||
                    document.webkitFullscreenEnabled ||
                    document.msFullscreenEnabled ||
                    document.mozFullScreenEnabled ||
                    !App.mobile) {
                    $(document).on("webkitfullscreenchange mozfullscreenchange fullscreenchange", this.onFullScreenChange);

                } else {
                    // Hide fullscreen button
                    this.hideToggleFullScreenButton = true;
                }

                this.listenTo(App.vent, App.Events.ModelEvents.AppBannerVisibilityChanged, this.onAppBannerVisibilityChanged, this);
            },
            onShow: function(){
                console.log('HeaderView:onShow');

                if (!App.mobile)
                    this.$("#toggleBannerButton").tooltip();

                if (this.hideToggleFullScreenButton){
                    this.$("#toggleFullScreenButton").css('display', 'none');
                }
                else{
                    this.$("#toggleFullScreenButton").css('display', 'block');
                    if (!App.mobile)
                        this.$("#toggleFullScreenButton").tooltip();
                }

            },
            onFullScreenChange: function(){
                console.log('HeaderView:onFullScreenChange');

                var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
                var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;

                if (fullscreenElement){
                    this.$("#toggleFullScreenButton").find(".fa").removeClass('fa-expand');
                    this.$("#toggleFullScreenButton").find(".fa").addClass('fa-compress');
                    if (!App.mobile)
                        this.$("#toggleFullScreenButton").tooltip( "option", "content", "Exit Full Screen Mode");
                }
                else{
                    this.$("#toggleFullScreenButton").find(".fa").removeClass('fa-compress');
                    this.$("#toggleFullScreenButton").find(".fa").addClass('fa-expand');
                    if (!App.mobile)
                        this.$("#toggleFullScreenButton").tooltip( "option", "content", "Enter Full Screen Mode");
                }

                if (!App.mobile)
                    this.$("#toggleFullScreenButton").tooltip();

            },
            onResize: function () {
                console.log('HeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('HeaderView:resize');

                var userSpecifiedPreference = true; //App.model.get('userSetAppBanner');

                if (userSpecifiedPreference !== true) {
                    var height = $(window).height();
                    if (height < 1024) {
                        // Hide the banner
                        App.model.set('appBannerVisible', false);
                    }
                    else {
                        // Show the banner
                        App.model.set('appBannerVisible', true);
                    }
                }

                var width = $(window).width();
                if (width < 768){
                    this.$("#toggleBannerButton").removeClass("pull-right");
                    this.$("#toggleBannerButton").addClass("pull-left");
                    this.$("#toggleFullScreenButton").removeClass("pull-right");
                    this.$("#toggleFullScreenButton").addClass("pull-left");
                    this.$("#toggleFullScreenButton").css('margin','-25px 10px 0px 35px');
                }
                else{
                    this.$("#toggleBannerButton").removeClass("pull-left");
                    this.$("#toggleBannerButton").addClass("pull-right");
                    this.$("#toggleFullScreenButton").removeClass("pull-left");
                    this.$("#toggleFullScreenButton").addClass("pull-right");
                    this.$("#toggleFullScreenButton").css('margin','-25px 35px 0px 10px');
                }
            },
            onAppBannerVisibilityChanged: function(){
                console.log('HeaderView:onAppBannerVisibilityChanged');

                if (App.model.get('appBannerVisible')){
                    this.$(".banner").css('display', 'block');
                    $(".page-header").css('height', '70px');
                    $(".page-header").css('margin', '40px 0px 20px');
                    this.$("#toggleBannerButton").find(".fa").removeClass('fa-chevron-circle-down');
                    this.$("#toggleBannerButton").find(".fa").addClass('fa-chevron-circle-up');
                    //if (!App.mobile)
                    //    this.$("#toggleBannerButton").tooltip( "option", "content", "Collapse Header");
                }
                else{
                    this.$(".banner").css('display', 'none');
                    $(".page-header").css('height', '32px');
                    $(".page-header").css('margin', '0px 0px 0px');
                    this.$("#toggleBannerButton").find(".fa").removeClass('fa-chevron-circle-up');
                    this.$("#toggleBannerButton").find(".fa").addClass('fa-chevron-circle-down');
                    //if (!App.mobile)
                    //    this.$("#toggleBannerButton").tooltip( "option", "content", "Expand Header");
                }
            },
            onToggleBannerButtonClicked: function(){
                console.log('HeaderView:onToggleBannerButtonClicked');

                //App.model.set('userSetAppBanner', true);

                if (App.model.get('appBannerVisible')){
                    App.model.set('appBannerVisible', false);
                }else{
                    App.model.set('appBannerVisible', true);
                }
            },
            onToggleFullScreenButtonClicked: function(){
                console.log('HeaderView:onToggleFullScreenButtonClicked');

                var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
                var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
                if (fullscreenElement)
                    this.exitFullScreen();
                else
                    this.launchFullScreen(document.getElementById('crc'));

            },
            launchFullScreen: function(element){
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            },
            exitFullScreen: function(){
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            },
            remove: function () {
                console.log('---------- PhoneHeaderView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);
                $(document).off("webkitfullscreenchange mozfullscreenchange fullscreenchange", this.onFullScreenChange);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                if (this.$('#toggleBannerButton').data('ui-tooltip'))
                    this.$('#toggleBannerButton').tooltip('destroy');

                if (this.$('#toggleFullScreenButton').data('ui-tooltip'))
                    this.$('#toggleFullScreenButton').tooltip('destroy');

                // Remove App event listeners
                // TODO: Add your code, if applicable

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });