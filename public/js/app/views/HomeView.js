define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/home', 'jquery.iosslider', 'jquery.easing', 'kendo/kendo.data'],
    function(App, Backbone, Marionette, $, Model, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            events: {
                'click .slider-image': 'onSliderClick',
                'click #StartButton': 'startButtonClicked'
            },

            initialize: function(options){

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.districtNum = App.model.get('districtNum');
                console.log('HomeView:initialize:districtNum:' + this.districtNum);

                // Subscribe to App events
                this.listenTo(App.vent, App.Events.ModelEvents.AppBannerVisibilityChanged, this.onAppBannerVisibilityChanged, this);
            },
            onRender: function(){
//                console.log('---------- HomeView:onRender ----------');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
//                console.log('---------- HomeView:onShow ----------');

                // Setup the container
                this.resize();

                console.log('HomeView:onShow');

                var self = this;

                setTimeout(function(){
                    self.resize();
                    if (self.districtNum === null){
                        self.createAutoClosingAlert($("#startWarning"), "<strong>Attention.</strong> Enter a permit number to proceed with the evaluation.");
                    }
                }, 0);

                this.$("#districtNumber").kendoMaskedTextBox({
                    mask: "000000A",
                    clearPromptChar: true
                });

            },
            startButtonClicked: function (e) {

                var self = this;

                this.districtNum = this.$('#districtNumber').val();
                console.log('HomeView:StartButtonClicked:districtNum:' + this.districtNum);

                // odata call to see if permit number exists
                var dataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url:  App.config.DataServiceURL + "/odata/RiceField",
                            //$select=OBJECTID,PURpermit_num,PURpermittee
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "OBJECTID"
                        }
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //kendo.ui.progress($("#projectWBSLoading"), true);
                    },
                    requestEnd: function () {
                        //kendo.ui.progress($("#projectWBSLoading"), false);
                    },
                    filter: { field: "permit_num", operator: "eq", value: this.districtNum.toString()}
                });

                var thisView = this;
                dataSource.fetch(function(){
                    var data = this.data();
                    var count = data.length;

                    if (count === 0){
                        console.log('HomeView:StartButtonClicked:districtNumNotFound');
                        self.createAutoClosingAlert($("#startError"), "<strong>Failure.</strong> Permit Number not found.  The permit number should be 7 digits long with no dashes <strong>(format: XXXXXXX)</strong>");
                    }
                    else {
                        console.log('HomeView:StartButtonClicked:SetdistrictNum:' + self.districtNum);

                        var permittee = null;
                        var i = 0;
                        while (permittee === null || permittee === undefined || permittee === ""){
                            permittee = data[i].permittee;
                            i++;
                        }
                        console.log('HomeView:StartButtonClicked:permittee:' + permittee);

                        App.model.set('districtNum', self.districtNum);
                        App.model.set('permittee', permittee);

                         // Go to beginning of survey
                        window.location.href = "#district";

                    }
                });
            },
            createAutoClosingAlert: function(selector, html){
                console.log('SummaryView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },
            onAppBannerVisibilityChanged: function(){
//                console.log('MiamiHomeLayoutView:onAppBannerVisibilityChanged');

                var self = this;
                setTimeout(function(){
                    self.resize();
                },0);
            },
            onResize: function(){
//                console.log(' MiamiHomeLayoutView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth !== winNewWidth || this.winHeight !== winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                // Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log(' MiamiHomeLayoutView:resize');

                var height = $(window).height();
                var width = $(window).width();

                var calculatedHeight = height;
                var minHeight = 600;  // 600 for live site
                var headerHeight = 145;
                if (App.model.get('appBannerVisible') === true){
                    calculatedHeight = height - headerHeight;
                }
                else{
                    headerHeight = 67;
                    calculatedHeight = height - headerHeight;
                }
                if (calculatedHeight < minHeight)
                    calculatedHeight = minHeight;
                this.$(".home-container").height(calculatedHeight);
                
                var scrollHeight = $("#crc")[0].scrollHeight;
                var clientHeight =  $("#crc")[0].clientHeight;

                // If we have a scroll bar then bad the bottom row so it can come into view
                if (scrollHeight > clientHeight){
                    this.$(".bottom-row").css('padding-bottom', '38px');
                }
                else{

                    if ((clientHeight - headerHeight) < minHeight)
                    {
                       var paddingBottom =  minHeight - (clientHeight - headerHeight);
                       this.$(".bottom-row").css('padding-bottom', paddingBottom + 'px');
                    }
                    else{
                       this.$(".bottom-row").css('padding-bottom', '');
                    }
                }

                // If we are stacked then pad the last column
                if (width < 992){
                    this.$(".last-column").css('padding-bottom', '38px');
                }
                else{
                    this.$(".last-column").css('padding-bottom', '2px');
                }
                this.$(".home-container").height(calculatedHeight);
            },
            remove: function(){
                console.log('-------- HomeView:remove --------');

                // Remove window events
                $(window).off('resize', this.onResize);

                // Turn off the slider events
                this.$('.iosSlider').iosSlider('destroy');

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
