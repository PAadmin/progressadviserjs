define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/deficiencyGrid', 'hbs!templates/editDeficiencyPopup',
        'jquery.cookie','kendo/kendo.all.min','kendo/kendo.data', 'jquery.dateFormat'], //'kendo/kendo.grid', 'kendo/kendo.data'
    function (App, $, Backbone, Marionette, template, EditDeficiencyPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('DeficiencyGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                // URL Parameters
                var params = kendo.parseQueryStringParams(window.location.href);

                //this.weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

                this.districtID = parseInt(params.districtID,0);
                this.districtName = params.districtName;
                this.schoolID = parseInt(params.schoolID,0);
                this.schoolName = params.schoolName;
                this.color1 = params.color1;

                $('.panel-heading').attr('style', 'background-color: #' + this.color1 + ' !important');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.filter = {
                    logic: "and",
                    filters: [
                        //{
                        //    logic: "or",
                        //    filters: [
                        //        {field: "section1", operator: "eq", value: 1},
                        //        {field: "section1", operator: "eq", value: 2},
                        //        {field: "section2", operator: "eq", value: 1},
                        //        {field: "section2", operator: "eq", value: 2},
                        //        {field: "section3", operator: "eq", value: 1},
                        //        {field: "section3", operator: "eq", value: 2},
                        //        {field: "section4", operator: "eq", value: 1},
                        //        {field: "section4", operator: "eq", value: 2},
                        //        {field: "section5", operator: "eq", value: 1},
                        //        {field: "section5", operator: "eq", value: 2},
                        //        {field: "section6", operator: "eq", value: 1},
                        //        {field: "section6", operator: "eq", value: 2},
                        //        {field: "section7", operator: "eq", value: 1},
                        //        {field: "section7", operator: "eq", value: 2},
                        //        {field: "section8", operator: "eq", value: 1},
                        //        {field: "section8", operator: "eq", value: 2},
                        //        {field: "section9", operator: "eq", value: 1},
                        //        {field: "section9", operator: "eq", value: 2},
                        //        {field: "section10", operator: "eq", value: 1},
                        //        {field: "section10", operator: "eq", value: 2},
                        //        {field: "section11", operator: "eq", value: 1},
                        //        {field: "section11", operator: "eq", value: 2},
                        //        {field: "section12", operator: "eq", value: 1},
                        //        {field: "section12", operator: "eq", value: 2},
                        //        {field: "section13", operator: "eq", value: 1},
                        //        {field: "section13", operator: "eq", value: 2},
                        //        {field: "section14", operator: "eq", value: 1},
                        //        {field: "section14", operator: "eq", value: 2},
                        //        {field: "section15", operator: "eq", value: 1},
                        //        {field: "section15", operator: "eq", value: 2}
                        //    ]
                        //},
                        {field: "districtid", operator: "eq", value: self.districtID},
                        {field: "reportId", operator: "gt", value: 912},
                        {field: "submitted", operator: "eq", value: true}
                    ]  // 881 for test db, change for production db
                };

                // set date filter
                this.startDate = new Date();

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {date: "30 days"};
                }

                if (this.options.date === "30 days") {
                    this.startDate.setDate(this.startDate.getDate() - 30);
                } else if (this.options.date === "60 days") {
                    this.startDate.setDate(this.startDate.getDate() - 60);

                } else if (this.options.date === "90 days") {
                    this.startDate.setDate(this.startDate.getDate() - 90);

                } else if (this.options.date === "180 days") {
                    this.startDate.setDate(this.startDate.getDate() - 180);

                } else if (this.options.date === "All") {
                    this.startDate.setDate(this.startDate.getDate() - 10000);
                } else if (this.options.date === "All with comments") {
                    this.startDate.setDate(this.startDate.getDate() - 10000);

                    this.filter = {
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {field: "section1_comments", operator: "neq", value: ""},
                                    {field: "section2_comments", operator: "neq", value: ""},
                                    {field: "section3_comments", operator: "neq", value: ""},
                                    {field: "section4_comments", operator: "neq", value: ""},
                                    {field: "section5_comments", operator: "neq", value: ""},
                                    {field: "section6_comments", operator: "neq", value: ""},
                                    {field: "section7_comments", operator: "neq", value: ""},
                                    {field: "section8_comments", operator: "neq", value: ""},
                                    {field: "section9_comments", operator: "neq", value: ""},
                                    {field: "section10_comments", operator: "neq", value: ""},
                                    {field: "section11_comments", operator: "neq", value: ""},
                                    {field: "section12_comments", operator: "neq", value: ""},
                                    {field: "section13_comments", operator: "neq", value: ""},
                                    {field: "section14_comments", operator: "neq", value: ""},
                                    {field: "section15_comments", operator: "neq", value: ""}
                                ]
                            },
                            {field: "districtid", operator: "eq", value: self.districtID},
                            {field: "reportId", operator: "gt", value: 912},
                            {field: "submitted", operator: "eq", value: true}
                        ]
                    };
                }

                // Add school id for user type 2
                if (this.schoolName !== undefined) {
                    this.filter.filters.push({field: "schoolid", operator: "eq", value: self.schoolID});
                }

                console.log('DeficiencyGridView:initialize:filter' + JSON.stringify(this.filter));

            },
            onRender: function () {
                console.log('DeficiencyGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log("-------- DeficiencyGridView:onShow --------");
                var self = this;

                // Panel title
                if (this.schoolName !== undefined) {
                    $("#defTitle").html("District: " + this.districtName + ", School: " + this.schoolName);
                } else {
                    $("#defTitle").html("District: " + this.districtName);
                }

                console.log('DeficiencyGridView:onShow:districtID:schoolID:' + this.districtID + ":" + this.schoolID );
                if (this.districtID === null) {
                    // Go back to home page
                    window.location.href = "";
                }

                this.grid = this.$("#def").kendoGrid({

                    //autobind:false,
                    //dataSource: this.dataSource,
                    //height: "800"
                    toolbar: ["excel"],//,{name:"pdf"}
                    excel: {
                        fileName: "Deficiency View Export.xlsx",
                        allPages: true
                    },
                    //excelExport: function(e) {
                    //    self.excelExport(e);
                    //},
                    pdf: {
                        fileName: "Deficiency View Export.pdf",
                        allPages: true,
                        //avoidLinks: true,
                        //paperSize: "A4",
                        //margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
                        landscape: true,
                        repeatHeaders: true
                        //template: $("#page-template").html(),
                        //scale: 0.8
                    },
                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit Deficiency",
                            //animation: true,
                            open: self.openEditTemplate,
                            close: self.closeEditTemplate
                            //save: self.saveEdits
                        },
                        template: function (e) {
                            var template = EditDeficiencyPopup();
                            return template;
                        }
                    },
                    sortable: true,
                    //extra: false,
                    resizable: true,
                    //reorderable: true,
                    //filterable: true,
                    //scrollable: false,
                    //selectable: "row",
                    //pageable: true,
                    groupable: true,
                    // 1,2,12,4,15,9,8,11,10,7,5,6,13,14,3
                    columns: [
                        {
                            command: [
                                {
                                    name: "edit",
                                    title: "Update"
                                }
                            ],
                            locked: true,
                            title: "&nbsp;",
                            width: "90px"
                        },
                        {
                            field: "submitDate",
                            title: "Submit Date",
                            //width: "auto",
                            width: 100,
                            locked: true,
                            template: function (e) {
                                var date = new Date(e.submitDate);
                                var dayOfMonth = date.getDate();//	Returns the day of the month (from 1-31)
                                var dayOfWeek = date.getDay(); //	Returns the day of the week (from 0-6)
                                var year = date.getFullYear();
                                var month = date.getMonth() + 1;
                                //Friday, January 1, 2016
                                //self.weekdays[dayOfWeek] + ", " +
                                var template = "<div style='text-align: left'>" + month + "/" + dayOfMonth  + "/" + year + "</b></div>";
                                return template;
                            }
                        },
                        {
                            field: "schoolName",
                            title: "School",
                            locked: true,
                            width: 150

                        },
                        {
                            field: "buildingName",
                            title: "Building",
                            locked: true,
                            width: 100
                        },
                        {
                            field: "roomName",
                            title: "Room",
                            locked: true,
                            width: 100

                        },
                        {
                            field: "section1",
                            title: "1. Gas Leaks",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0, tooltip: "Not Applicable"},
                                {text: "D", value: 2, tooltip: "Deficiency"},
                                {text: "X", value: 1, tooltip: "Extreme Deficiency"},
                                {text: "ND", value: 3, tooltip: "No Deficiency"}
                            ]
                        },
                        {
                            field: "section1_comments",
                            title: "1. Comments",
                            //hidden: true,
                            width: 150
                        },
                        {
                            field: "section2",
                            title: "2. Mechanical Systems",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section2_comments",
                            title: "2. Comments",
                            width: 150
                        },
                        {
                            field: "section12",
                            title: "3. Sewer",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section12_comments",
                            title: "3. Comments",
                            width: 150
                        },
                        {
                            field: "section4",
                            title: "4. Interior Surfaces",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section4_comments",
                            title: "4. Comments",
                            width: 150
                        },
                        {
                            field: "section15",
                            title: "5. Overall Cleanliness",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section15_comments",
                            title: "5. Comments",
                            width: 150
                        },
                        {
                            field: "section9",
                            title: "6. Pest/Vermin Infestation",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section9_comments",
                            title: "6. Comments",
                            width: 150
                        },
                        {
                            field: "section8",
                            title: "7. Electrical ",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]

                        },
                        {
                            field: "section8_comments",
                            title: "7. Comments",
                            width: 150
                        },
                        {
                            field: "section11",
                            title: "8. Restrooms",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section11_comments",
                            title: "8. Comments",
                            width: 150
                        },
                        {
                            field: "section10",
                            title: "9. Drinking Fountains",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]

                        },
                        {
                            field: "section10_comments",
                            title: "9. Comments",
                            width: 150
                        },
                        {
                            field: "section7",
                            title: "10. Fire Safety",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section7_comments",
                            title: "10. Comments",
                            width: 150
                        },
                        {
                            field: "section5",
                            title: "11. Hazardous Materials",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]

                        },
                        {
                            field: "section5_comments",
                            title: "11. Comments",
                            width: 150
                        },
                        {
                            field: "section6",
                            title: "12. Structural Damage",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]

                        },
                        {
                            field: "section6_comments",
                            title: "12. Comments",
                            width: 150
                        },
                        {
                            field: "section13",
                            title: "13. Roofs",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section13_comments",
                            title: "13. Comments",
                            width: 150
                        },
                        {
                            field: "section14",
                            title: "14. Playground/School Grounds",
                            width: 75,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section14_comments",
                            title: "14. Comments",
                            width: 150
                        },
                        {
                            field: "section3",
                            title: "15. Windows, Doors, Gates, Fences",
                            width: 75,
                            //min-width: 50,
                            values: [
                                {text: "N/A", value: 0},
                                {text: "D", value: 2},
                                {text: "X", value: 1},
                                {text: "ND", value: 3}
                            ]
                        },
                        {
                            field: "section3_comments",
                            title: "15. Comments",
                            width: 150
                        }
                        //,
                        //{
                        //    field: "section1_comments",
                        //    title: "1. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section2_comments",
                        //    title: "2. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section12_comments",
                        //    title: "3. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section4_comments",
                        //    title: "4. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section15_comments",
                        //    title: "5. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //// 1,2,12,4,15,9,8,11,10,7,5,6,13,14,3
                        //{
                        //    field: "section9_comments",
                        //    title: "6. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section8_comments",
                        //    title: "7. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section11_comments",
                        //    title: "8. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section10_comments",
                        //    title: "9. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section7_comments",
                        //    title: "10. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section5_comments",
                        //    title: "11. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section6_comments",
                        //    title: "12. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section13_comments",
                        //    title: "13. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section14_comments",
                        //    title: "14. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //},
                        //{
                        //    field: "section3_comments",
                        //    title: "15. Comments",
                        //    //hidden: true,
                        //    width: 0
                        //}
                    ],
                    //rowTemplate: function (e) {
                    //    return self.buildRowTemplate(false, e);
                    //},
                    //altRowTemplate: function (e) {
                    //    return self.buildRowTemplate(true, e);
                    //},
                    dataBound: function (e) {
                        console.log("DeficiencyGridView:def:onShow:dataBound");

                        // Resize the grid
                        //setTimeout(self.resize, 0);
                    },
                    cancel: function (e) {
                        console.log('DeficiencyGridView:def:onShow:onCancel');

                        // Resize the grid
                        //setTimeout(self.resize, 0);
                        self.getData();
                    },
                    change: function (e) {
                        console.log('DeficiencyGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        console.log('DeficiencyGridView:def:onShow:onSave');

                        //if editing 1 data record, sync to save the change, then re-get the data
                        if (self.dataSource) {
                            $.when(self.dataSource.sync()).done(function () {
                                self.getData();
                            });
                        }
                    },
                    edit: function (e) {
                        console.log('DeficiencyGridView:def:onShow:onEdit');

                        // Disable the reportId editor
                        //e.container.find("input[name='reportId']").prop("disabled", true);

                    }
                }).data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                //setTimeout(this.resize, 0);

            },
            fitValues: function (value) {
                if (value === 0) {
                    return "N/A";
                } else if (value === 1) {
                    return "D";
                } else if (value === 2) {
                    return "X";
                } else if (value === 3) {
                    return "ND";
                }
            },
            formatDate: function (dateString) {
                var date = new Date(dateString);
                var dayOfMonth = date.getDate();//	Returns the day of the month (from 1-31)
                var dayOfWeek = date.getDay(); //	Returns the day of the week (from 0-6)
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                //Friday, January 1, 2016
                //self.weekdays[dayOfWeek] + ", " +
                var template = "<div style='text-align: left'>" + month + "/" + dayOfMonth  + "/" + year + "</b></div>";
                return template;
            },
            buildRowTemplate: function (alt, e) {
                console.log('DeficiencyGridView:buildRowTemplate:start');

                var template = "<tr data-uid='" + e.uid + "' class='";

                if (alt)
                    template += "k-alt ";
                template += "' data-id='" + e.reportId + "'>";

                //template += "<td class='td-right'>" + e.reportId + "</td>";

                template += "<td class='td-center'><a class='k-button k-button-icontext k-grid-edit' href='#'><span class='k-icon k-edit'></span>Edit</a></td>";

                if (e.submitDate === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.formatDate(e.submitDate) + "</td>";
                }

                if (e.schoolName === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.schoolName + "</td>";
                }

                if (e.buildingName === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.buildingName + "</td>";
                }

                if (e.roomName === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.roomName + "</td>";
                }
                // 1,2,12,4,15,9,8,11,10,7,5,6,13,14,3
                if (e.section1 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section1) + "</td>";
                }

                if (e.section2 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section2) + "</td>";
                }
                if (e.section12 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section12) + "</td>";
                }
                if (e.section4 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section4) + "</td>";
                }
                if (e.section15 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section15) + "</td>";
                }
                if (e.section9 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section9) + "</td>";
                }
                if (e.section8 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section8) + "</td>";
                }
                if (e.section11 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section11) + "</td>";
                }
                if (e.section10 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section10) + "</td>";
                }
                if (e.section7 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section7) + "</td>";
                }
                if (e.section5 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section5) + "</td>";
                }
                if (e.section6 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section6) + "</td>";
                }
                if (e.section13 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section13) + "</td>";
                }
                if (e.section14 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section14) + "</td>";
                }
                if (e.section3 === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + this.fitValues(e.section3) + "</td>";
                }
                template += "</tr>";

                //if (!App.mobile) {
                //this.$('#def').tooltip();
                //}

                console.log('DeficiencyGridView:buildRowTemplate:done');
                return template;

            },
            openEditTemplate: function (value) {

                var num = 0;
                $(value.sender.element).find('.fitSection input').each(function () {
                    //console.log("DeficiencyGridView:openEditTemplate:num:$(this):" + num + ":" + ($(this).html()));

                    var textarea = $(value.sender.element).find('.fitSection textarea')[num];

                    if ((this.value === "0" || this.value === "3") && textarea.value === "") {
                        //console.log("DeficiencyGridView:openEditTemplate:" + (num+1) + ":invisible");
                        $(this).parent().parent().parent().css("display","none");
                    }
                    num++;
                });
            },
            closeEditTemplate: function (e) {
                console.log('DeficiencyGridView:closeEditTemplate');

                // Resize the grid
                setTimeout(this.resize, 0);

            },

            getData: function () {
                console.log('DeficiencyGridView:getData');

                var self = this;
                var app = App;
                this.dataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            //type: "odata",
                            //read: {
                            //    url: App.config.DataServiceURL + "/odata/FitReportData",
                            //    dataType: "json"
                            //},
                            read: function(options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/odata/FitReportData",
                                    dataType: "json",
                                    //data: function (e) {
                                    //    console.log('DeficiencyGridView:read:e:' + JSON.stringify(e));
                                    //},
                                    requestStart: function (e) {
                                        kendo.ui.progress(self.$("#defLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //var data = this.data();
                                        kendo.ui.progress(self.$("#defLoading"), false);
                                    },
                                    success: function(result) {
                                        // notify the data source that the request succeeded
                                        options.success(result);
                                        //console.log('DeficiencyGridView:success:result' + JSON.stringify(result));
                                    },
                                    error: function(result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#defLoading"), false);
                                        options.error(result);
                                    }
                                });
                            },
                            update: function(options) {

                                console.log('DeficiencyGridView:update:options:' + JSON.stringify(options));

                                var data = options.data;

                                var record = {
                                    reportId: data.reportId,
                                    timeStamp:data.timeStamp,
                                    observedBy:data.observedBy,
                                    roomID:data.roomID,
                                    section1: data.section1,
                                    section1_comments: data.section1_comments,
                                    section2: data.section2,
                                    section2_comments: data.section2_comments,
                                    section3: data.section3,
                                    section3_comments: data.section3_comments,
                                    section4: data.section4,
                                    section4_comments: data.section4_comments,
                                    section5: data.section5,
                                    section5_comments: data.section5_comments,
                                    section6: data.section6,
                                    section6_comments: data.section6_comments,
                                    section7: data.section7,
                                    section7_comments: data.section7_comments,
                                    section8: data.section8,
                                    section8_comments: data.section8_comments,
                                    section9: data.section9,
                                    section9_comments: data.section9_comments,
                                    section10: data.section10,
                                    section10_comments: data.section10_comments,
                                    section11: data.section11,
                                    section11_comments: data.section11_comments,
                                    section12: data.section12,
                                    section12_comments: data.section12_comments,
                                    section13: data.section13,
                                    section13_comments: data.section13_comments,
                                    section14: data.section14,
                                    section14_comments: data.section14_comments,
                                    section15: data.section15,
                                    section15_comments: data.section15_comments,
                                    schoolid:data.schoolid,
                                    districtid: data.districtid,
                                    reportTypeId:data.reportTypeId,
                                    lastUpdated:(new Date()),
                                    updatedBy:data.updatedBy,
                                    submitted:data.submitted,
                                    submitDate:data.submitDate,
                                    roomType:data.roomType,
                                    weatherConditions:data.weatherConditions,
                                    appObsID:data.appObsID,
                                    deviceID:data.deviceID,
                                    buildingID:data.buildingID,
                                    facilityID:data.facilityID
                                };

                                var record2 = JSON.stringify(record);  //JSON.parse(

                                $.ajax({
                                    type: "PUT",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: (app.config.DataServiceURL + "/odata/FitReportDataTable/(" + options.data.reportId + ")"),
                                    dataType: "json",
                                    id: "reportId",
                                    data: record2,
                                    requestStart: function (e) {
                                        //console.log('DeficiencyGridView:update:requestStart');
                                        kendo.ui.progress(self.$("#defLoading"), false);
                                        kendo.ui.progress(self.$("#defLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //var data = this.data();
                                        //console.log('DeficiencyGridView:update:requestEnd');
                                        kendo.ui.progress(self.$("#defLoading"), false);
                                    },
                                    success: function(result) {
                                        // notify the data source that the request succeeded
                                        console.log('DeficiencyGridView:update:success:' + JSON.stringify(result));
                                        options.success(result);
                                    },
                                    error: function(result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#defLoading"), false);
                                        console.log('DeficiencyGridView:update:error:' + JSON.stringify(result));
                                        options.error(result);
                                    }
                                });
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "reportId",
                                fields: {
                                    reportId: {editable: false, type: "number"},
                                    timeStamp: {type: "date", validation: {required: false}},
                                    submitDate: {type: "string", validation: {required: false}},
                                    section1: {type: "number"},
                                    section1_comments: {type: "string", validation: {required: false}},
                                    section2: {type: "number"},
                                    section2_comments: {type: "string", validation: {required: false}},
                                    section3: {type: "number"},
                                    section3_comments: {type: "string", validation: {required: false}},
                                    section4: {type: "number"},
                                    section4_comments: {type: "string", validation: {required: false}},
                                    section5: {type: "number"},
                                    section5_comments: {type: "string", validation: {required: false}},
                                    section6: {type: "number"},
                                    section6_comments: {type: "string", validation: {required: false}},
                                    section7: {type: "number"},
                                    section7_comments: {type: "string", validation: {required: false}},
                                    section8: {type: "number"},
                                    section8_comments: {type: "string", validation: {required: false}},
                                    section9: {type: "number"},
                                    section9_comments: {type: "string", validation: {required: false}},
                                    section10: {type: "number"},
                                    section10_comments: {type: "string", validation: {required: false}},
                                    section11: {type: "number"},
                                    section11_comments: {type: "string", validation: {required: false}},
                                    section12: {type: "number"},
                                    section12_comments: {type: "string", validation: {required: false}},
                                    section13: {type: "number"},
                                    section13_comments: {type: "string", validation: {required: false}},
                                    section14: {type: "number"},
                                    section14_comments: {type: "string", validation: {required: false}},
                                    section15: {type: "number"},
                                    section15_comments: {type: "string", validation: {required: false}},
                                    facilityID: {editable: false, type: "number"},
                                    buildingID: {editable: false, type: "number"},
                                    roomName: {editable: false, type: "string", validation: {required: false}},
                                    buildingName: {editable: false, type: "string", validation: {required: false}},
                                    schoolName: {editable: false, type: "string", validation: {required: true}},
                                    districtName: {editable: false, type: "string", validation: {required: false}},
                                    districtid: {editable: false, type: "number"},
                                    submitted: {editable: false, type: "boolean"}
                                }
                            }
                        },
                        //pageSize: 25,
                        //batch: false,
                        //serverPaging: true,
                        //serverSorting: true,
                        //serverFiltering: true,
                        sort: [{field: "submitDate", dir: "desc"}], //{field: "facilityID", dir: "asc"},
                        requestStart: function (e) {
                            //kendo.ui.progress(self.$("#defLoading"), false);
                            kendo.ui.progress(self.$("#defLoading"), true);
                        },
                        requestEnd: function (e) {
                            //var data = this.data();
                            kendo.ui.progress(self.$("#defLoading"), false);
                        },
                        change: function (e) {
                            if (e.items) {
                                var data = e.items;
                                if (data.length > 1) {
                                    console.log('DeficiencyGridView:dataSource:change:'); // + JSON.stringify(e)

                                    //var data = this.data();
                                    var data2 = [];
                                    var facilityID = null;
                                    var facilityIDnew = null;
                                    var date = null;

                                    var hasDeficiency = false;
                                    var def = [1,2];

                                    $.each(data, function (index, value) {
                                        //console.log('DeficiencyGridView:getData:value:' + JSON.stringify(value));
                                        facilityIDnew = value.facilityID;

                                        hasDeficiency = false;
                                        if (def.indexOf(value.section1) >= 0 ||
                                            def.indexOf(value.section2) >= 0 ||
                                            def.indexOf(value.section3) >= 0 ||
                                            def.indexOf(value.section4) >= 0 ||
                                            def.indexOf(value.section5) >= 0 ||
                                            def.indexOf(value.section6) >= 0 ||
                                            def.indexOf(value.section7) >= 0 ||
                                            def.indexOf(value.section8) >= 0 ||
                                            def.indexOf(value.section9) >= 0 ||
                                            def.indexOf(value.section10) >= 0 ||
                                            def.indexOf(value.section11) >= 0 ||
                                            def.indexOf(value.section12) >= 0 ||
                                            def.indexOf(value.section13) >= 0 ||
                                            def.indexOf(value.section14) >= 0 ||
                                            def.indexOf(value.section15) >= 0) {
                                            hasDeficiency = true;
                                        }


                                        date = new Date(value.submitDate);  // value.lastUpdated;  value.timeStamp

                                        if (facilityIDnew !== facilityID && facilityIDnew !== null && facilityIDnew !== undefined &&
                                            date > self.startDate || self.options.date === "All with comments") {
                                            if (hasDeficiency === true) {
                                                data2.push(value);
                                            }
                                            facilityID = value.facilityID;
                                        }

                                    });

                                    self.grid.dataSource.data(data2);
                                    self.grid.dataSource.sort({field: "submitDate", dir: "desc"});
                                    //self.grid.dataSource.pageSize = 25;
                                    //self.grid.dataSource.page = 1;

                                    setTimeout(self.resize, 0);

                                } else if (data.length === 0) {
                                    self.displayNoDataOverlay();
                                } else {

                                    // edit 1 data record

                                }
                            } else {
                                console.log('DeficiencyGridView:dataSource:change:no e.items');
                            }
                        },
                        error: function (e) {
                            console.log('DeficiencyGridView:dataSource:error:' + JSON.stringify(e));
                            kendo.ui.progress(self.$("#defLoading"), false);
                        }
                        //filter:this.filter
                    });

                var state = {};
                state.filter = this.filter;
                state.sort = [{ field: "facilityID", dir: "asc" },{field: "submitDate", dir: "desc"}];
                //state.page = 1;
                //state.pageSize = 25;

                //$.when(self.dataSource.query(state)).done(function () {
                //    setTimeout(self.resize, 0);
                //});

                this.dataSource.query(state);

            },

            formatCells: function(value) {
                console.log('DeficiencyGridView:formatCells');
                $('td').filter(function () {
                    return $(this).text() === "D";
                }).css('background-color', '#f2dede');

                $('td').filter(function () {
                    return $(this).text() === "X";
                }).css('background-color', '#e7c3c3');
            },
            displayNoDataOverlay: function () {
                console.log('DeficiencyGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#def').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                console.log('DeficiencyGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#def').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('DeficiencyGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                this.formatCells();

                var parentHeight = this.$("#def").parent().parent().height();
                this.$("#def").height(parentHeight);

                parentHeight = parentHeight - 75; // -75

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                console.log('DeficiencyGridView:resize:parentHeight:' + parentHeight);
                console.log('DeficiencyGridView:resize:headerHeight:' + headerHeight);
                this.$("#def").find(".k-grid-content").height(parentHeight - headerHeight);


                this.$("#defTable").height(parentHeight +105);
            },
            remove: function () {
                console.log("-------- DeficiencyGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#def").data('ui-tooltip'))
                    this.$("#def").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });