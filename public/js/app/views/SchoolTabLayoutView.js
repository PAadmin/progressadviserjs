define( ['App', 'backbone', 'marionette', 'jquery', 'options/ViewOptions', 'hbs!templates/schoolTabLayout',
         'views/SchoolMapView','views/FunctionalityNotAvailableView'],
    function(App, Backbone, Marionette, $, ViewOptions, template, SchoolMapView, FunctionalityNotAvailableView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            regions: {

                schoolTabContentRegion: '#facilities-tab-content-region'

            },
            initialize: function(options){
                this.tab = null;

                App.model.set('schoolTab', 'Deficiency View');

                console.log('SchoolTabLayoutView:initialize');

                _.bindAll(this);

                this.options = options;
            },
            onRender: function(){
                console.log('SchoolTabLayoutView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- SchoolTabLayoutView:onShow ----------');

                // Display the WBS hint for the user
                var viewOptions = new ViewOptions();
                viewOptions.live = false;

                this.listenTo(App.vent, App.Events.ModelEvents.SchoolTabChanged, this.onSchoolTabChanged);

                var detailTabBar = this.$("#schoolTabBar");
                detailTabBar.bind('click', this.onDetailsTabClicked);

                this.activeRegion = '#facilities-tab-content-region';
                $(this.activeRegion).css('display', 'block');
                this.startView = new SchoolMapView();
                this.schoolTabContentRegion.show(this.startView);

            },

            resetTabs: function(){
                this.schoolTabContentRegion.reset();

            },
            onSchoolTabChanged: function (text) {
                console.log('SchoolTabLayoutView:onSchoolTabChanged:tab:' + text);
                this.tab = App.model.get('schoolTab');

                this.$('#schoolTabBar li').removeClass('active');

                if (this.tab === "Buildings") {
                    this.$('#schoolTab2').addClass('active');
                }

                this.onDetailsTabClicked();

                this.tab = null;

            },
            onDetailsTabClicked: function(e){

                var tab;

                if (this.tab !== null) {

                    console.log('SchoolTabLayoutView:onDetailsTabClicked:fromOnSchoolTabChanged:' + this.tab);

                    tab = this.tab;

                    this.resetTabs();

                    // GA - track the tabs
                    this._pageView(tab);

                    console.log('  School Locations Tab Clicked');

                    if (this.activeRegion != '#facilities-tab-content-region')
                        $(this.activeRegion).css('display', 'none');
                    this.activeRegion = '#facilities-tab-content-region';
                    $(this.activeRegion).css('display', 'block');

                    this.schoolTabContentRegion.show(new SchoolMapView());

                }
                else {

                    console.log('SchoolTabLayoutView:onDetailsTabClicked:fromPhysicalTabClick');

                    var tabText = e.target.innerText;

                    if (tabText === undefined) {
                        tabText = e.target.textContent;
                    }
                    tab = tabText;

                    // Store the selected tab
                    App.model.set('schoolTab', tab);
                }
            },
            // GA - To track our application routes
            _pageView: function(tab) {
                var path = Backbone.history.getFragment();
                var prefix = "";
                if (App.mobile){
                    if (App.phone)
                        prefix = 'phone-';
                    else
                        prefix = 'tablet-';
                }
                ga('send', 'pageview', {page: prefix + "/" + path + "#" + tab});
            },
            resize: function(){
                // Force the child views to resize (as necessary)
                var self = this;

            },
            remove: function(){
                console.log('---------- SchoolTabLayoutView:remove ----------');

                // TODO: Kill referenced views

                // Remove all the event handlers
                this.undelegateEvents();

                // Remove jQuery bindings
                this.$("#schoolTabBar").unbind('click');

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });