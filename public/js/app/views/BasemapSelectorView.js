define(['../App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/basemapSelector'],
    function(App, Backbone, Marionette, $, Model, template) {
        return Backbone.Marionette.ItemView.extend( {
            template: template,
            events: {
                'click #grayBasemapButton':                 'grayBasemapButtonClicked',
                'click #streetsBasemapButton':              'streetsBasemapButtonClicked',
                'click #imageryBasemapButton':              'imageryBasemapButtonClicked',
                'click #topographicBasemapButton':          'topographicBasemapButtonClicked',
                'click #nationalGeographicBasemapButton':   'nationalGeographicBasemapButtonClicked',
                'click #shadedReliefBasemapButton':         'shadedReliefBasemapButtonClicked'
            },
            initialize: function(){
                console.log('BaseMapSelectorView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                // Subscribe to App events
                this.listenTo(App.vent, App.Events.ModelEvents.AppBannerVisibilityChanged, this.onAppBannerVisibilityChanged, this);
            },
            onRender: function(){
//                console.log('BaseMapSelectorView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('-------- BaseMapSelectorView:onShow --------');

                // Setup a click handler to destroy the view
                this.$el.on('click', this.onViewClicked);

                // Setup the container
                this.resize();

                // Sync up state
                this.updateBasemapState();
            },
            onViewClicked: function(){
//                console.log('BaseMapSelectorView:onViewClicked');

                this.close();
            },
            onAppBannerVisibilityChanged: function(){
//                console.log('BaseMapSelectorView:onAppBannerVisibilityChanged');

                this.resize();
            },
            updateBasemapState: function(){
//                console.log('BaseMapSelectorView:updateBasemapState');

                // Gray
                if (App.model.get('basemapLayer') === App.Enums.Basemap.Gray)
                    this.$('#grayBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#grayBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');

                // Streets
                if (App.model.get('basemapLayer') === App.Enums.Basemap.Streets)
                    this.$('#streetsBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#streetsBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');

                // Imagery
                if (App.model.get('basemapLayer') === App.Enums.Basemap.Imagery)
                    this.$('#imageryBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#imageryBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');

                // Topographic
                if (App.model.get('basemapLayer') === App.Enums.Basemap.Topographic)
                    this.$('#topographicBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#topographicBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');

                // National Geographic
                if (App.model.get('basemapLayer') === App.Enums.Basemap.NationalGeographic)
                    this.$('#nationalGeographicBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#nationalGeographicBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');

                // Shaded Relief
                if (App.model.get('basemapLayer') === App.Enums.Basemap.ShadedRelief)
                    this.$('#shadedReliefBasemapThumbnail').removeClass('basemap-image-shadow').addClass('basemap-image-selected-program basemap-image-selected');
                else
                    this.$('#shadedReliefBasemapThumbnail').removeClass('basemap-image-selected-program basemap-image-selected').addClass('basemap-image-shadow');
            },
            grayBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:grayBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.Gray);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            streetsBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:streetsBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.Streets);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            imageryBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:imageryBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.Imagery);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            topographicBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:topographicBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.Topographic);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            nationalGeographicBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:nationalGeographicBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.NationalGeographic);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            shadedReliefBasemapButtonClicked: function(e){
//                console.log('BaseMapSelectorView:shadedReliefBasemapButtonClicked');

                App.model.set('basemapLayer', App.Enums.Basemap.ShadedRelief);
                this.updateBasemapState();

                // Prevent the view from being dismissed
                e.stopPropagation();
                return false;
            },
            onResize: function(){
//                console.log('BaseMapSelectorView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth !== winNewWidth || this.winHeight !== winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log('BaseMapSelectorView:resize');

                var height = $(window).height();
                var calculatedHeight = height;
                if (App.model.get('appBannerVisible') === true){
                    calculatedHeight = height - 150;
                }
                else{
                    calculatedHeight = height - 72;
                }
                if (calculatedHeight < 800)
                    calculatedHeight = 800;

//                this.$('.basemap-layout-container').height(calculatedHeight);
            },
            remove: function(){
                console.log('-------- BaseMapSelectorView:remove --------');

                // Turn off events
                $(window).off('resize', this.onResize);

                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });