define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/schoolLayout', 'views/FacilitiesFilteredLayoutView',
         'views/SchoolTabLayoutView'],
    function(App, Backbone, Marionette, $, Model, template, FacilitiesFilteredLayoutView, SchoolTabLayoutView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            regions: {
                facilitiesFilteredLayoutRegion: '#facilities-filtered-layout',
                facilitiesFilteredListLayoutRegion: "#facilities-filtered-list-layout"
            },
            events: {
                'click #FitButton': 'fitButtonClicked'
            },
            initialize: function() {
                console.log('SchoolLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.listenTo(App.vent, App.Events.ModelEvents.AppBannerVisibilityChanged, this.onAppBannerVisibilityChanged, this);
            },
            fitButtonClicked: function (e) {
                console.log('SchoolLayoutView:fitButtonClicked');

                //Bogus d
                //var url = "http://127.0.0.1/progressAdviser/pa/app/reports/fit/fitFormExt.cfm?obsForm=f&referer=wt&districtID=10006&roomId=25863&roomtype=12&obsSchoolID=20137&obsSchoolName='Morse High School'&buildingName='200 Building'&roomName='Media Room'&reportid=0";

                // San Diego
                var url = "http://127.0.0.1/progressAdviser/pa/app/reports/fit/fitFormExt.cfm?obsForm=f&referer=wt&districtID=10300&roomId=31035&roomtype=11&obsSchoolID=21207&obsSchoolName='Morse High School'&buildingName='200 Building'&roomName='Media Room'&reportid=0";
                window.location.href = url;
                //window.open(url);

            },
            onRender: function (){
                console.log('SchoolLayoutView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('SchoolLayoutView:onShow');

                // Setup the container
                this.resize();

                this.facilitiesFilteredLayoutRegion.show(new FacilitiesFilteredLayoutView());
                this.facilitiesFilteredListLayoutRegion.show(new SchoolTabLayoutView());
            },
            onAppBannerVisibilityChanged: function(){
                console.log('SchoolLayoutView:onAppBannerVisibilityChanged');

                this.resize();
            },
            onResize: function(){
                //console.log('SchoolLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                console.log('SchoolLayoutView:resize');

                var height = $(window).height();
                var calculatedHeight = height;
                if (App.model.get('appBannerVisible') === true){
                    calculatedHeight = height - 80 - 150;
                }
                else{
                    calculatedHeight = height - 152; //72
                }
                if (calculatedHeight < 662)
                    calculatedHeight = 662;
                //this.$(".project-details-layout-container").height(calculatedHeight);

                // Now resize our views as the window size has not changed
                if (this.facilitiesFilteredLayoutRegion.currentView)
                    this.facilitiesFilteredLayoutRegion.currentView.resize();
                if (this.facilitiesFilteredListLayoutRegion.currentView)
                    this.facilitiesFilteredListLayoutRegion.currentView.resize();

            },
            remove: function(){
                console.log('SchoolLayoutView:remove');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // App event listeners are cleaned up by Backbone via listenTo
                // App.vent.off(...

                //call the superclass remove method
                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });