define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/functionalityNotAvailable'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            initialize: function(options){
                console.log('FunctionalityNotAvailableView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

            },
            onRender: function(){
//                console.log('FunctionalityNotAvailableView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- FunctionalityNotAvailableView:onShow ----------');

                // If custom text inputted, use that
                if (this.options){
                    this.$('#errorText').text(this.options.text);
                }
                // Setup a click handler to destroy the view
                this.$el.on('click', this.onFunctionalityNotAvailableModalClicked);
            },
            onFunctionalityNotAvailableModalClicked: function(){
//                console.log('FunctionalityNotAvailableView:onFunctionalityNotAvailableModalClicked');

                this.close();
            },
            onResize: function(){
//                console.log('FunctionalityNotAvailableView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log('FunctionalityNotAvailableView:resize');

                // TODO: Add your code

            },
            remove: function(){
                console.log('---------- FunctionalityNotAvailableView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                $('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });