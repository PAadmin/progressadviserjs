define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/room', 'models/RoomModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #roomRow': 'rowClicked'
            },
            initialize: function () {
                console.log('RoomView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- RoomView:onShow --------');

                this.id = this.model.get('id');
                this.name = this.model.get('name');
                this.status = this.model.get('status');

                this.selectedBuildingId = parseInt(App.model.get('selectedBuildingId'), 0);

                this.selectedRoomId = App.model.get('selectedRoomId');
                if (this.selectedRoomId === null || this.selectedRoomId === undefined) {
                    this.selectedRoomId  = this.id;
                }

                this.$('#roomValue').html("<b>Room: </b>" + this.name ); // + "<br>Other2: other2"
                this.$('#roomValue').val(this.id);

                if (this.status === "Complete") {
                    this.$('#roomRow').css("background-color","#c1e2b3");  // #dff0d8 lightest green,  #c1e2b3 light green
                } else if (this.status === "InProgress") {
                    this.$('#roomRow').css("background-color", "#f7ecb5");  // #fcf8e3 lightest yellow, #fcf8e3 light yellow, light blue #afd9ee
                }


            },
            rowClicked: function (e) {
                console.log('RoomView:rowClicked');

                // If not crtl or shift pressed, de-select all rows
                var event = e || window.event;
                if (event.ctrlKey || event.shiftKey) {

                }
                else {
                    $('.room-row').removeClass('k-state-selected');
                }

                // Select or deselect this row
                var selected = true;
                if (!this.$('.room-row').hasClass('k-state-selected')) {
                    this.$('.room-row').addClass('k-state-selected');

                    $('#roomLabel').html(", Current Room: " + this.name);

                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(100% - 46px)');
                    $('#top-right-panel-body').hide();
                    $('#top-right-panel').height('33px');

                }
                else {

                    $('#bottom-right-panel-body').show();
                    $('#bottom-right-panel').height('calc(50% - 4px)');
                    $('#top-right-panel-body').show();
                    $('#top-right-panel').height('calc(50% - 7px)');

                    selected = false;
                    this.$('.room-row').removeClass('k-state-selected');
                }

            },
            onResize: function () {
//                console.log('AdminProjectRiskView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                console.log('RoomView:resize');

            },
            remove: function () {
                console.log('-------- RoomView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#roomRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
