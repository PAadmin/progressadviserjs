define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/fieldGrid', 'jquery.cookie','kendo/kendo.data'],
        //'kendo/kendo.dataviz','kendo/kendo.tabstrip','kendo/kendo.data','kendo/kendo.grid'], ,
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('FieldGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.districtNum = App.model.get('districtNum');
                this.permittee = App.model.get('permittee');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.fieldTempDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/FieldsTemp",
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {

                                console.log('FieldGridView:update: url');
                                return App.config.DataServiceURL + "/odata/FieldsTemp" + "(" + data.Id + ")";

                            }
                        },
                        create: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/FieldsTemp"; // + "(" + data.Id + ")";
                            }
                        },
                        destroy: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/FieldsTemp" + "(" + data.Id + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                PermitNumber: {type: "string", validation: {required: true}},
                                SiteId: {type: "string", validation: {required: true}},
                                PKSiteId: {type: "string", validation: {required: true}},
                                RiceAcreageGrown: {type: "number", validation: {required: true}},
                                NoRiceGrown: {type: "string", validation: {required: true}},
                                RotateRice: {type: "string", validation: {required: true}},
                                OtherCrops: {type: "string", validation: {required: true}},
                                PercentageOtherCrops: {type: "number", validation: {required: true}},
                                RotationFreq: {type: "string", validation: {required: true}},
                                LandTakenOut: {type: "string", validation: {required: true}},
                                FollowLabelRestrictions: {type: "string", validation: {required: true}},
                                FollowCountyPermitCds: {type: "string", validation: {required: true}},
                                WaterholdingRequirements: {type: "string", validation: {required: true}},
                                MonitorWindConditions: {type: "string", validation: {required: true}},
                                AppropriateBufferZones: {type: "string", validation: {required: true}},
                                AttendTrainings: {type: "string", validation: {required: true}},
                                UseLowDriftNozzles: {type: "string", validation: {required: true}},
                                UsePCARecommendations: {type: "string", validation: {required: true}},
                                EndOfRowShutoff: {type: "string", validation: {required: true}},
                                AvoidSurfaceWater: {type: "string", validation: {required: true}},
                                ComplianceWithPPE: {type: "string", validation: {required: true}},
                                UseDriftControlAgents: {type: "string", validation: {required: true}},
                                ProvideEmployeeSafetyTraining: {type: "string", validation: {required: true}},
                                LandLevPrecTech: {type: "string", validation: {required: true}},
                                UtilizeTailwaterReturn: {type: "string", validation: {required: true}},
                                UtilizePeripheralDrains: {type: "string", validation: {required: true}},
                                MonitorRainForecasts: {type: "string", validation: {required: true}},
                                CropRotationOrg: {type: "string", validation: {required: true}},
                                CoverCroppingOrg: {type: "string", validation: {required: true}},
                                StripCroppingOrg: {type: "string", validation: {required: true}},
                                CompactingLevees: {type: "string", validation: {required: true}},
                                WaterReleasePreFieldPrep: {type: "string", validation: {required: true}},
                                WaterReleasePreHarvest: {type: "string", validation: {required: true}},
                                SlowRelease: {type: "string", validation: {required: true}},
                                CropRotationNonorg: {type: "string", validation: {required: true}},
                                VoluntaryTrainings: {type: "string", validation: {required: true}}
                            }
                        }
                    },
                    requestStart: function () {
                        //console.log('FieldGridView:dataSource:requestStart');
                        kendo.ui.progress($("#fieldLoading"), true);
                    },
                    requestEnd: function () {
                        //console.log('FieldGridView:dataSource:requestEnd');
                        kendo.ui.progress($("#fieldLoading"), false);
                    },
                    error: function () {
                        //console.log('FieldGridView:dataSource:error');
                        kendo.ui.progress($("#fieldLoading"), false);
                        //self.displayNoDataOverlay();
                    },
                    change: function () {
                        console.log('FieldGridView:dataSource:change');

                    },
                    batch: false,
                    pageSize: 50,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    filter: {field: "PermitNumber", operator: "eq", value: this.districtNum.toString()}
                });

            },
            onRender: function () {
                console.log('FieldGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log("-------- FieldGridView:onShow --------");
                var self = this;

                // Panel title
                $("#fieldTitle").html(this.permittee + " (" + this.districtNum + ")");

                console.log('FieldGridView:onShow:districtNum:' + this.districtNum);


                var grid = this.$("#fieldGrid").kendoGrid({

                    dataSource: this.fieldTempDataSource,
                    //pdf: {
                    //    fileName: self.districtNum + " Farm Evaluation.pdf",
                    //    proxyURL: "/proxy"
                    //    //paperSize: "A4"
                    //},
                    columns: [
                        {
                            field: "SiteId",
                            title: "Site Id",
                            width: 80
                        },
                        {
                            field: "NoRiceGrown",
                            title: "No Rice Grown",
                            width: 80
                        },
                        {
                            field: "RotateRice",
                            title: "Rotate Rice",
                            width: 80
                        },
                        {
                            field: "OtherCrops",
                            title: "Other Crops",
                            width: 120
                        },
                        {
                            field: "PercentageOtherCrops",
                            title: "% Other Crops",
                            width: 120
                        },
                        {
                            field: "RotationFreq",
                            title: "Rotation Frequency",
                            width: 120
                        },
                        {
                            field: "LandTakenOut",
                            title: "Land Taken Out",
                            width: 120
                        },
                        {
                            field: "FollowLabelRestrictions",
                            title: "Follow Label Restrictions",
                            width: 120
                        },
                        {
                            field: "FollowCountyPermitCds",
                            title: "Follow County Permit Conditions",
                            width: 120
                        },
                        {
                            field: "WaterholdingRequirements",
                            title: "Waterholding Requirements",
                            width: 120
                        },
                        {
                            field: "MonitorWindConditions",
                            title: "Monitor Wind Conditions",
                            width: 120
                        },
                        {
                            field: "AppropriateBufferZones",
                            title: "Appropriate Buffer Zones",
                            width: 120
                        },
                        {
                            field: "AttendTrainings",
                            title: "Attend Trainings",
                            width: 120
                        },
                        {
                            field: "UseLowDriftNozzles",
                            title: "Use Low Drift Nozzles",
                            width: 120
                        },
                        {
                            field: "UsePCARecommendations",
                            title: "Use PCA Recommendations",
                            width: 120
                        },
                        {
                            field: "EndOfRowShutoff",
                            title: "End Of Row Shutoff",
                            width: 120
                        },
                        {
                            field: "AvoidSurfaceWater",
                            title: "Avoid Surface Water",
                            width: 120
                        },
                        {
                            field: "ComplianceWithPPE",
                            title: "Compliance With PPE",
                            width: 120
                        },
                        {
                            field: "UseDriftControlAgents",
                            title: "Use Drift Control Agents",
                            width: 120
                        },
                        {
                            field: "ProvideEmployeeSafetyTraining",
                            title: "Provide Employee Safety Training",
                            width: 120
                        },
                        {
                            field: "LandLevPrecTech",
                            title: "Land Leveling / Precision Technology",
                            width: 120
                        },
                        {
                            field: "UtilizeTailwaterReturn",
                            title: "Utilize Tailwater Return",
                            width: 120
                        },
                        {
                            field: "UtilizePeripheralDrains",
                            title: "Utilize Peripheral Drains",
                            width: 120
                        },
                        {
                            field: "MonitorRainForecasts",
                            title: "Monitor Rain Forecasts",
                            width: 120
                        },
                        {
                            field: "CropRotationOrg",
                            title: "Crop Rotation Organic",
                            width: 120
                        },
                        {
                            field: "CoverCroppingOrg",
                            title: "Cover Cropping Organic",
                            width: 120
                        },
                        {
                            field: "StripCroppingOrg",
                            title: "Strip Cropping Organic",
                            width: 120
                        },
                        {
                            field: "CompactingLevees",
                            title: "Compacting Levees",
                            width: 120
                        },
                        {
                            field: "WaterReleasePreFieldPrep",
                            title: "Water Release PreField Prep",
                            width: 120
                        },
                        {
                            field: "WaterReleasePreHarvest",
                            title: "Water Release PreHarvest",
                            width: 120
                        },
                        {
                            field: "SlowRelease",
                            title: "Slow Release",
                            width: 120
                        },
                        {
                            field: "CropRotationNonorg",
                            title: "Crop Rotation Nonorganic",
                            width: 120
                        },
                        {
                            field: "VoluntaryTrainings",
                            title: "Voluntary Trainings",
                            width: 120
                        }
                    ],
                    //height: "800",
                    //detailTemplate: function(e){ return self.buildDetailTemplate(e); },
                    //toolbar: ["pdf","excel"],
                    //editable: "popup",
                    sortable: true,
                    //scrollable: false,
                    extra: false,
                    filterable: true,
                    //selectable: "multiple",
                    pageable: true,
                    resizable: true,
                    //detailInit: self.detailInit,
                    //dataBound: function() {
                    //    this.expandRow(this.tbody.find("tr.k-master-row").first());
                    //},
                    columnResize: function (e) {
                        //self.resize();
                        window.setTimeout(self.resize, 10);
                    },
                    //rowTemplate: function (e) {
                    //    return self.buildRowTemplate(false, e);
                    //},
                    //altRowTemplate: function (e) {
                    //    return self.buildRowTemplate(true, e);
                    //},
                    change: function (e) {
                        console.log('FieldGridView:onShow:onChange');

                    }
                }); //.data("kendoGrid");

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            detailInit: function (e) {
                detailRow.find(".tabstrip").kendoTabStrip({
                    animation: {
                        open: { effects: "fadeIn" }
                    }
                });

                detailRow.find(".rice-acreage-rows").kendoGrid({
                    dataSource: {
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/FieldsTemp",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "Id",
                                fields: {
                                    Id: {editable: false, defaultValue: 0, type: "number"},
                                    PermitNumber: {type: "string", validation: {required: true}},
                                    SiteId: {type: "string", validation: {required: true}},
                                    PKSiteId: {type: "string", validation: {required: true}},
                                    RiceAcreageGrown: {type: "number", validation: {required: true}},
                                    NoRiceGrown: {type: "string", validation: {required: true}},
                                    RotateRice: {type: "string", validation: {required: true}},
                                    OtherCrops: {type: "string", validation: {required: true}},
                                    PercentageOtherCrops: {type: "number", validation: {required: true}},
                                    RotationFreq: {type: "string", validation: {required: true}},
                                    LandTakenOut: {type: "string", validation: {required: true}}
                                }
                            }
                        },
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 100,
                        filter: {field: "PermitNumber", operator: "eq", value: e.data.permitNumber.toString()}
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    columns: [

                        {
                            field: "SiteId",
                            title: "SiteId",
                            width: 80
                        },
                        {
                            field: "NoRiceGrown",
                            title: "No Rice Grown",
                            width: 80
                        },
                        {
                            field: "RotateRice",
                            title: "Rotate Rice",
                            width: 80
                        },
                        {
                            field: "OtherCrops",
                            title: "OtherCrops",
                            width: 80
                        },
                        {
                            field: "PercentageOtherCrops",
                            title: "PercentageOtherCrops",
                            width: 80
                        },
                        {
                            field: "RotationFreq",
                            title: "RotationFreq",
                            width: 80
                        },
                        {
                            field: "LandTakenOut",
                            title: "LandTakenOut",
                            width: 80
                        }
                    ]
                });

                detailRow.find(".regulatory-rows").kendoGrid({
                    dataSource: {
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/FieldsTemp",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "Id",
                                fields: {
                                    Id: {editable: false, defaultValue: 0, type: "number"},
                                    WellId: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: true}
                                    },
                                    GroundSlopedAway: {
                                        type: "string",
                                        validation: {required: true}
                                    },
                                    StandingWaterAvoided: {type: "string", validation: {required: true}},
                                    GoodHousekeepingPractices: {type: "string", validation: {required: true}},
                                    AirGap: {type: "string", validation: {required: true}},
                                    BackflowPrevCheckValve: {type: "string", validation: {required: true}},
                                    YearAbandoned: {type: "string", validation: {required: true}},
                                    DestroyedDescription: {type: "string", validation: {required: true}}
                                }
                            }
                        },
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 100,
                        filter: {field: "PermitNumber", operator: "eq", value: e.data.permitNumber.toString()}
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    columns: [

                        {
                            field: "SiteId",
                            title: "SiteId",
                            width: 80
                        },
                        {
                            field: "FollowLabelRestrictions",
                            title: "FollowLabelRestrictions",
                            width: 80
                        },
                        {
                            field: "FollowCountyPermitCds",
                            title: "FollowCountyPermitCds",
                            width: 80
                        },
                        {
                            field: "WaterholdingRequirements",
                            title: "WaterholdingRequirements",
                            width: 80
                        },
                        {
                            field: "MonitorWindConditions",
                            title: "MonitorWindConditions",
                            width: 80
                        },
                        {
                            field: "AppropriateBufferZones",
                            title: "AppropriateBufferZones",
                            width: 80
                        },
                        {
                            field: "AttendTrainings",
                            title: "AttendTrainings",
                            width: 80
                        },
                        {
                            field: "UseLowDriftNozzles",
                            title: "UseLowDriftNozzles",
                            width: 80
                        },
                        {
                            field: "UsePCARecommendations",
                            title: "UsePCARecommendations",
                            width: 80
                        },
                        {
                            field: "EndOfRowShutoff",
                            title: "EndOfRowShutoff",
                            width: 80
                        },
                        {
                            field: "AvoidSurfaceWater",
                            title: "AvoidSurfaceWater",
                            width: 80
                        },
                        {
                            field: "ComplianceWithPPE",
                            title: "ComplianceWithPPE",
                            width: 80
                        },
                        {
                            field: "UseDriftControlAgents",
                            title: "UseDriftControlAgentss",
                            width: 80
                        },
                        {
                            field: "ProvideEmployeeSafetyTraining",
                            title: "ProvideEmployeeSafetyTraining",
                            width: 80
                        }
                    ]
                });

                detailRow.find(".voluntary-rows").kendoGrid({
                    dataSource: {
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/FieldsTemp",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "Id",
                                fields: {
                                    Id: {editable: false, defaultValue: 0, type: "number"},
                                    WellId: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: true}
                                    },
                                    GroundSlopedAway: {
                                        type: "string",
                                        validation: {required: true}
                                    },
                                    StandingWaterAvoided: {type: "string", validation: {required: true}},
                                    GoodHousekeepingPractices: {type: "string", validation: {required: true}},
                                    AirGap: {type: "string", validation: {required: true}},
                                    BackflowPrevCheckValve: {type: "string", validation: {required: true}},
                                    YearAbandoned: {type: "string", validation: {required: true}},
                                    DestroyedDescription: {type: "string", validation: {required: true}}
                                }
                            }
                        },
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 100,
                        filter: {field: "PermitNumber", operator: "eq", value: e.data.permitNumber.toString()}
                    },
                    scrollable: false,
                    sortable: true,
                    pageable: true,
                    columns: [

                        {
                            field: "SiteId",
                            title: "SiteId",
                            width: 80
                        },
                        {
                            field: "LandLevPrecTech",
                            title: "LandLevPrecTech",
                            width: 80
                        },
                        {
                            field: "UtilizeTailwaterReturn",
                            title: "UtilizeTailwaterReturn",
                            width: 80
                        },
                        {
                            field: "UtilizePeripheralDrains",
                            title: "UtilizePeripheralDrains",
                            width: 80
                        },
                        {
                            field: "MonitorRainForecasts",
                            title: "MonitorRainForecasts",
                            width: 80
                        },
                        {
                            field: "CropRotationOrg",
                            title: "CropRotationOrg",
                            width: 80
                        },
                        {
                            field: "CoverCroppingOrg",
                            title: "CoverCroppingOrg",
                            width: 80
                        },
                        {
                            field: "StripCroppingOrg",
                            title: "StripCroppingOrgs",
                            width: 80
                        },
                        {
                            field: "CompactingLevees",
                            title: "CompactingLevees",
                            width: 80
                        },
                        {
                            field: "WaterReleasePreFieldPrep",
                            title: "WaterReleasePreFieldPrep",
                            width: 80
                        },
                        {
                            field: "WaterReleasePreHarvest",
                            title: "WaterReleasePreHarvest",
                            width: 80
                        },
                        {
                            field: "SlowRelease",
                            title: "SlowRelease",
                            width: 80
                        },
                        {
                            field: "CropRotationNonorg",
                            title: "CropRotationNonorg",
                            width: 80
                        },
                        {
                            field: "VoluntaryTrainings",
                            title: "VoluntaryTrainings",
                            width: 80
                        }
                    ]
                });

            },
            buildDetailTemplate: function (e) {
                var template = "<div class='tabstrip'>" +
                "<ul><li class='k-state-active'>Rice Acreage</li>" +
                    "<li >Regulatory Farm Management Practices</li>" +
                    "<li >VoluntaryFarm Management Practices</li></ul>" +
                    "<div>" +
                       "<div class='rice-acreage-rows'></div>" +
                    "</div>" +
                    "<div>" +
                      "<div class='regulatory-rows'></div>" +
                    "</div>" +
                    "<div>" +
                      "<div class='voluntary-rows'></div>" +
                    "</div>" +
                "</div>";

                return template;
            },
            buildRowTemplate: function (alt, e) {
                console.log('FieldGridView:buildRowTemplate:start');

                //e.districtNumber = this.districtNum;

                var template = "<tr data-uid='" + e.uid + "' class='";

                if (alt)
                    template += "k-alt ";
                template += "' data-id='" + e.Id + "'>";

                template += "<td class='td-right'>" + e.Id + "</td>";
                template += "<td class='td-right'>" + e.permitNumber + "</td>";
                if (e.LandownerAwareWDR === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.LandownerAwareWDR + "</td>";
                }

                if (e.LandownerCopyWDR === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.LandownerCopyWDR + "</td>";
                }

                if (e.LocationsWaterLeave === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.LocationsWaterLeave + "</td>";
                }

                if (e.LocationsWaterLeaveMarked === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.LocationsWaterLeaveMarked + "</td>";
                }

                if (e.LocationsWaterDrains === null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.LocationsWaterDrains + "</td>";
                }

                if (e.FarmMapFilledOut=== null) {
                    template += "<td class='td-left'></td>";
                }
                else {
                    template += "<td class='td-left'>" + e.FarmMapFilledOut + "</td>";
                }
                template += "</tr>";

                //if (!App.mobile) {
                    this.$('#fieldGrid').tooltip();
                //}

                console.log('FieldGridView:buildRowTemplate:done');
                return template;

            },
            displayNoDataOverlay: function () {
                console.log('FieldGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#fieldGrid').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                console.log('FieldGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#fieldGrid').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('FieldGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#fieldGrid").parent().parent().height();
                this.$("#fieldGrid").height(parentHeight);

                parentHeight = parentHeight - 79;  //87

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                console.log('FieldGridView:resize:parentHeight:' + parentHeight);
                console.log('FieldGridView:resize:headerHeight:' + headerHeight);
                this.$("#fieldGrid").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                console.log("-------- FieldGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#fieldGrid").data('ui-tooltip'))
                    this.$("#fieldGrid").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });