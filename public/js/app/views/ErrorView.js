define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/error'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            initialize: function(options){
                console.log('ErrorView:initialize:' + JSON.stringify(options));
                this.url = options.link;
                this.options = options;

                _.bindAll(this);

            },
            onRender: function(){

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- ErrorView:onShow ----------');

                // Setup a click handler to destroy the view
                this.$el.on('click', this.onCloseClicked);

                //if (this.options.fullScreen === false){
                //    this.$(".outer-fs").addClass('outer').removeClass("outer-fs");
                //    this.$(".inner-fs").addClass('inner').removeClass("inner-fs");
                //}
            },
            onCloseClicked: function(){
                this.close();
            },
            remove: function(){
                console.log('----------ErrorView:remove ----------');

                // Off all events - otherwise handle them individually
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });