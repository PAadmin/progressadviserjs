define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/schoolMap', 'custom/CustomMarker', 'views/BasemapSelectorView', 'leaflet.bouncemarker', 'leaflet.esri'],
    function (App, $, Backbone, Marionette, template, CustomMarker, BasemapSelectorView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #mapSettings': 'onMapSettingsClicked'
            },
            initialize: function () {
                console.log('SchoolMapView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                // NOTE: App events are subscribed to once the view is rendered - adding listeners here
                // will be unbound when Leafletjs initializes


                // Setup the datasource for the filtered projects
                var self = this;
                var app = App;
                this.dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                //return "http://dekalbconsentdecree.com/services/odata/ProjectsView?$select=OBJECTID,WBSId,CenterX,CenterY,Geometry";
                                return "http://199.115.221.114/services/odata/SanDiegoSchool?$select=ID,Lat,Long,SHORTNAME,CDSCODE,DISTRICT&$filter=indexof%28DISTRICT,%27San%20Diego%27%29%20gt%20-1";

                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {type: "number"},
                                Long: {type: "number"},
                                Lat: {type: "number"},
                                SHORTNAME: {type: "string"},
                                CDSCODE: {type: "string"}
                            }
                        }
                    },
                    serverFiltering: true,
                    requestStart: function () {
                        self.hideNoDataOverlay();

                        // Here we will stop listening to changes and then resubscribe when we have valid date -
                        // this is required as something within leafletjs severs the events when data changes
                        self.stopListening(app.vent, app.Events.ModelEvents.SelectedSchoolIdChanged, self.onSelectedSchoolIdChanged, self);

                        kendo.ui.progress(self.$("#projectMapLoading"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress(self.$("#projectMapLoading"), false);
                    },
                    error: function () {
                        self.displayNoDataOverlay();
                    },
                    change: this.dataSourceChanged
                });

            },
            onRender: function () {
                //console.log('ProjectMapView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- SchoolMapView:onShow ----------');

                // Setup the map and base tile layer
                this.map = L.map('projectMap', {trackResize: false, zoomControl: true});
                this.map.zoomControl = false;

                // Set the defaultBasemap
                this.basemapLayer = L.esri.basemapLayer(App.model.get('basemapLayer'));
                this.map.addLayer(this.basemapLayer);

                // Create a layer or cluster group that will contain all of our projects
                //this.projectMapLayer = L.markerClusterGroup();
                this.projectMapLayer = L.featureGroup();
                this.map.addLayer(this.projectMapLayer);

                // Set a default view extent (SD)

                this.map.setView(new L.LatLng(33.4135420447092, -117.148745990203), 13);

                // Here we use the zoomend event to adjust the radius of circle
                // markers
                var self = this;
                self.restoredState = false;

                this.map.on('moveend', function () {
                    // Draw the selected item - only do this on the first render
                    if (!self.restoredState)
                        self.onSelectedSchoolIdChanged(null);
                    self.restoredState = true;
                });

                this.map.on('zoomend', function (e) {
                    //self.geoJsonLayer.eachLayer(function(layer){
                    //    layer.setRadius(1);
                    //});

                    var zoomLevel = self.map.getZoom();

                    if (typeof self.projectMapLayer !== 'undefined') {
                        self.projectMapLayer.eachLayer(function (layer) {
                            layer.eachLayer(function (flayer) {
                                if (flayer.feature.geometry.type === "Point") { //coordinates.length == 2) {
                                    self.geoJsonSetRadius(flayer);
                                }
                            });
                        });
                    }

                    // Exit if we're not using option 2 - Display feature layers from Esri (point, line, poly)
                    if (typeof basemapPointLayer !== 'undefined') {
                        var pointLayer = basemapPointLayer._layers;
                        for (var prop in pointLayer) {
                            var layer = pointLayer[prop];
                            if (layer) {
                                self.geoJsonSetRadius(layer);
                            }
                        }
                    }

                    // Draw the selected item - only do this on the first render
                    if (!self.restoredState)
                        self.onSelectedSchoolIdChanged(null);
                    self.restoredState = true;

                });

                // Get the currently selected data
                var state = {};
                var filter = []; //App.model.getKendoFilter();
                state.filter = filter;
                this.dataSource.query(state);

                // Resize the display
                setTimeout(this.onResize, 100);

                // Subscribe to App events
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onFilterChanged, this);
                this.listenTo(App.vent, App.Events.ModelEvents.MapDisplaySymbologyChanged, this.onMapDisplaySymbologyChanged, this);
                this.listenTo(App.vent, App.Events.ModelEvents.BasemapLayerChanged, this.onBasemapLayerChanged, this);

                // IdChanged is subscribed to once data loads
                //this.listenTo(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onSelectedSchoolIdChanged, this);

                // Map the selected feature
                // TODO: Determine when the map has finished drawing then fire this off
                //var self = this;
                //setTimeout( function(){ self.onWBSIdChanged(null); }, 1000);

                // Setup tooltips - if we're on the desktop
                if (!App.mobile) {
                    this.$('#mapSettings').tooltip();
                    this.$('#mapSettings').tooltip();
                }


                // Add basemap selector button to Leaflet control
                L.Control.Command = L.Control.extend({
                    options: {
                        position: 'topleft'
                    },

                    onAdd: function (map) {
                        var controlDiv = L.DomUtil.create('div', 'leaflet-control-zoom leaflet-bar leaflet-control');
                        L.DomEvent
                            .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
                            .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
                            .addListener(controlDiv, 'click', function () {
                                self.onMapSettingsClicked();
                            });

                        var controlUI = L.DomUtil.create('a', 'leaflet-control-zoom-out', controlDiv);
                        controlUI.title = 'Select basemap';
                        $(controlUI).html('<i class="fa fa-cogs" style="color: #000; font-size: 14px; padding-left: -2px"></i>');
                        return controlDiv;
                    }
                });

                L.control.command = function (options) {
                    return new L.Control.Command(options);
                };

                var c = new L.control.command().addTo(this.map);

                // San Diego
                var SW = new L.LatLng(32.3, -118.3);
                var NE = new L.LatLng(33.1, -116.1);

                // Setup default extent
                var bounds = L.latLngBounds(SW, NE);
                App.model.set('mapBounds', bounds);

                this.map.fitBounds(bounds);
                this.map.zoomIn(5);

            },
            onResize: function () {
                //console.log('SchoolMapView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('SchoolMapView:resize');

                // When the container is resized the map needs to be resized vertically
                // and rendered
                var parentHeight = this.$el.parent().height();
                //this.$("#projectMap").height(parentHeight - 32);

                // http://stackoverflow.com/questions/10762984/leaflet-map-not-displayed-properly-inside-tabbed-panel
                // L.Util.requestAnimFrame(this.map.invalidateSize,this.map,!1,this.map._container);
                var self = this;
                setTimeout(function () {
                    self.map.invalidateSize(false);
                }, 0);
            },
            onBasemapLayerChanged: function (args) {
                console.log('SchoolMapView:onBasemapChanged:' + args);

                // TODO: Remove existing basemap layer
                $(".leaflet-tile-pane").find("leaflet-layer").removeClass("leaflet-layer");

                // Add layer
                var basemapLayer = L.esri.basemapLayer(args);
                this.map.removeLayer(this.basemapLayer);
                this.basemapLayer = basemapLayer;
                this.map.addLayer(basemapLayer);
            },
            onMapSettingsClicked: function () {
//                console.log('ProjectMapView:onMapSettingsClicked');

                App.modal.show(new BasemapSelectorView());
            },
            onFilterChanged: function (args) {
                console.log('SchoolMapView:onFilterChanged:' + args);

                var self = this;

                var i = 0;
                this.map.eachLayer(function (layer) {
                    if (i > 1) {
                        self.map.removeLayer(layer);
                        console.log('SchoolMapView:clickFeature: REMOVING LAYER');
                    }
                    i++;
                });


                // Remove any existing projects
                if (this.selectedProjectMapLayer) {
                    this.map.removeLayer(this.selectedProjectMapLayer);
                    this.selectedProjectMapLayer = null;
                    console.log('SchoolMapView:onFilterChanged: REMOVING LAYER');
                }

                // Query the relevant data
                var state = {};
                //state.filter = App.model.getKendoFilter();
                this.dataSource.query(state);

            },
            onSelectedSchoolIdChanged: function (args) {
                console.log('SchoolMapView:onSelectedSchoolIdChanged:' + args);

                // Remove any existing projects

                if (this.selectedProjectMapLayer) {
                    this.map.removeLayer(this.selectedProjectMapLayer);
                    //console.log('SchoolMapView:onSelectedSchoolIdChanged: REMOVING LAYER: '+ JSON.stringify(this.selectedProjectMapLayer));
                    this.selectedProjectMapLayer = null;
                }

                // Ensure we have a valid wbsid
                if (!App.model.get('selectedSchoolId')) {
                    console.log('SchoolMapView:onSelectedSchoolIdChanged:No App.model.id');
                    return;
                }

                // Ensure that the wbdId still exists in the filtered data
                // if not then bail...the tree or list will trigger a WBSId Changed
                var id = parseInt(App.model.get('selectedSchoolId'), 0);

                var inDataSourceItem = this.dataSource.get(id);
                if (typeof inDataSourceItem === 'undefined') {
                    console.log('ITEM NOT DEFINED WAITING FOR EVENT CHANGE');
                    return;
                }

                // Ensure we have a centroid we can map
                var centroid = App.model.get('centroid');
                if (!centroid) {
                    console.log('SchoolMapView:onSelectedSchoolIdChanged:No centroid');
                    return;
                }
                if (centroid === "null,null")
                    return;

                var coords = centroid.split(",");

                // Center the map
                // TODO: Use the zoom/pan end to drop the pin
                this.map.panTo(new L.LatLng(coords[1], coords[0]));

                // Slight delay then add the pin
                var self = this;
                setTimeout(function () {
                    self.selectedProjectMapLayer = L.marker([coords[1], coords[0]],
                        {
                            bounceOnAdd: true,
                            bounceOnAddOptions: {duration: 750, height: 400}
                            //bounceOnAddCallback: function() {}
                        });
                    //var link = $('<a href="#projectdetails" class=""></a>').click(function () {
                    //    // Add JS Implementation Here
                    //})[0];
                    //self.selectedProjectMapLayer.bindPopup(link);
                    self.map.addLayer(self.selectedProjectMapLayer);
                    //self.selectedProjectMapLayer.openPopup();
                }, 0);


            },
            onMapDisplaySymbologyChanged: function (args) {
                console.log('SchoolMapView:onMapDisplaySymbologyChanged:' + args);

                // Stop listening to WBSId as we will setup a listener again once data is added to the map - see
                // datasource documentation
                this.stopListening(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onSelectedSchoolIdChanged, this);

                // Call the data source changed event to map out the features
                this.dataSourceChanged();

            },
            mouseOverFeature: function (e) {
                console.log('SchoolMapView: mouseOverFeature');

                var layer = e.target;

                layer.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera) {
                    layer.bringToFront();
                }

                //e.target.openPopup();
            },
            mouseOutFeature: function (e) {
                console.log('SchoolMapView: mouseOutFeature');

                this.geoJsonLayer.resetStyle(e.target);
                if (e.target.feature.geometry.type === "Point") {
                    //if (e.target.feature.geometry.coordinates.length == 2){
                    this.geoJsonSetRadius(e.target);
                }

                //e.target.closePopup();
            },
            clickFeature: function (e) {

                //e.target.openPopup();

                console.log('SchoolMapView: clickFeature');

                // Store the centroid for this SchoolId
                App.model.set('centroid', e.target.feature.properties.CenterX + "," + e.target.feature.properties.CenterY);

                // Set the current schoolid
                App.model.set("selectedSchoolId", e.target.feature.properties.Id);

            },

            geoJsonPointToLayer: function (feature, latlng) {
                var geojsonMarkerOptions = {
                    radius: 4
                };

                return L.circleMarker(latlng, geojsonMarkerOptions);
            },
            geoJsonLayerEachFeature: function (feature, layer) {
                // TODO: Display marker for each feature?? Let the feature provide basemap type support while the marker
                // represents a KPI??

                layer.bindPopup(feature.properties.Name.toString());

                // Project Events
                layer.on("mouseover", this.mouseOverFeature);
                layer.on("mouseout", this.mouseOutFeature);
                layer.on("click", this.clickFeature);
            },
            geoJsonSetRadius: function (feature) {
                var zoomLevel = this.map.getZoom();

                if (zoomLevel < 13)
                    feature.setRadius(3);
                else if (zoomLevel == 13)
                    feature.setRadius(3);
                else if (zoomLevel == 14)
                    feature.setRadius(5);
                else
                    feature.setRadius(zoomLevel / 2);
            },
            dataSourceChanged: function (e) {
                console.log('SchoolMapView:dataSourceChanged');

                // If we're not using the map we can ignore any changes to the datasource
                if (!this.map)
                    return;

                // Clear any existing layers
                this.projectMapLayer.clearLayers();

                var elements = this.dataSource.data();
                console.log("  ---------------------" + elements.length + " features to map");
                for (var i = 0; i < elements.length; i++) {

                    // TODO: Symbolize the features
                    var school = elements[i];

                    // TODO: Symbolize the pin - need to determine the desired logic around this
                    //L.marker([wbs.CenterY, wbs.CenterX]).addTo(this.map);

                    var feature = {
                        "type": "Feature",
                        "id": school.ID,
                        "properties": {
                            "Id": school.ID,
                            "Name": school.SHORTNAME,
                            "CenterX": school.Long,
                            "CenterY": school.Lat
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [school.Long, school.Lat]
                        }
                    };

                    var features = [];
                    features.push(feature);

                    var geoJsonData = {
                        "type": "FeatureCollection",
                        "features": features
                    };

                    var thisView = this;
                    this.geoJsonLayer = L.geoJson(geoJsonData, {

                        pointToLayer: this.geoJsonPointToLayer,
                        onEachFeature: this.geoJsonLayerEachFeature,
                        style: this.geoJsonLayerStyle
                    });

                    this.projectMapLayer.addLayer(this.geoJsonLayer);

                }

                // Ensure state is restored i.e. a selected pin is redrawn in ZoomEnd
                this.restoredState = false;

                // Ensure the bounds are valid before we update the map extent - we also
                // only want to update the extent if we have new data i.e. a symbology
                // change should not update the extent
                if (e) {
                    var projectBounds = this.projectMapLayer.getBounds();
                    if (projectBounds.isValid()) {
                        //this.map.fitBounds(projectBounds);

                        this.hideNoDataOverlay();
                    }
                    else
                        this.displayNoDataOverlay();
                }

                // Resubscribe to WBSId changed event - this was unsubscribed to when data was loaded
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onSelectedSchoolIdChanged, this);

            },
            geoJsonLayerStyle: function () {

                return {
                    // Magenta
                    //fillColor: "#D6B0BA",
                    //color: "#7D194F",

                    // Gray
                    fillColor: "#636260",
                    color: "#25282a",

                    // Green/yellow
                    //fillColor: "#e4fbb7",
                    //color: "#679808",

                    weight: 2,
                    opacity: 1,
                    fillOpacity: 0.5
                };
            },
            displayNoDataOverlay: function () {

                if (this.$el.find(".no-data-overlay-full").length === 0)
                    $('<div class="no-data-overlay-full" style="z-index: 3; position: absolute !important; background-color: #ffffff !important; opacity: 0.8 !important; padding: 0 !important;"><div class="no-data-overlay-message">No data available</div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                this.$el.find(".no-data-overlay-full").remove();
            },
            remove: function () {
                console.log('---------- SchoolMapView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // Clear tooltips
                if (this.$('#mapSettings').data('ui-tooltip'))
                    this.$('#mapSettings').tooltip('destroy');

                // Kill the leaflet map
                this.map.remove();

                // App event listeners are cleaned up by Backbone via listenTo
                // App.vent.off(...


                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });