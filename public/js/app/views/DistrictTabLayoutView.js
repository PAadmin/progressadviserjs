define(['App', 'backbone', 'marionette', 'jquery', 'options/ViewOptions', 'hbs!templates/districtTabLayout',
    'views/SchoolLayoutView','views/DeficiencyTableView'],
    function (App, Backbone, Marionette, $, ViewOptions, template, SchoolLayoutView,DeficiencyTableView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {

                districtTabContentRegion: '#district-tab-content-region'

            },
            initialize: function (options) {

                this.tab = null;

                console.log('DistrictTabLayoutView:initialize');

                _.bindAll(this);

                this.options = options;
            },
            onRender: function () {
                console.log('DistrictTabLayoutView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- DistrictTabLayoutView:onShow ----------');

                this.listenTo(App.vent, App.Events.ModelEvents.DistrictTabChanged, this.onDistrictTabChanged);

                var detailTabBar = this.$("#districtTabBar");
                detailTabBar.bind('click', this.onDetailsTabClicked);

                this.activeRegion = '#district-tab-content-region';
                $(this.activeRegion).css('display', 'block');
                //this.startView = new LandownerAcknowledgementView();
                this.startView = new DeficiencyTableView(); //SchoolLayoutView();
                this.districtTabContentRegion.show(this.startView);

                // Turn banner off to begin survey?
                //App.model.set('userSetAppBanner', true);
                App.model.set('appBannerVisible', false);
                console.log('DistrictTabLayoutView:appBannerVisible:false?');
            },

            resetTabs: function () {
                this.districtTabContentRegion.reset();

            },
            onDistrictTabChanged: function (text) {
                this.tab = App.model.get('districtTab');

                console.log('DistrictTabLayoutView:onDistrictTabChanged:DistrictTab:' + this.tab);

                this.$('#districtTabBar li').removeClass('active');

                //if (this.tab === "Summary") {
                //    this.$('#districtTab6').addClass('active');
                //}
                if (this.tab === "School Locations") {
                    this.$('#districtTab1').addClass('active');
                }
                else if (this.tab == "Deficiency View") {
                    this.$('#districtTab2').addClass('active');
                }
                else {
                    return;
                }

                this.onDetailsTabClicked();

                this.tab = null;

            },
            onDetailsTabClicked: function (e) {

                var tab;

                if (this.tab !== null) {
                    tab = this.tab;

                    console.log('DistrictTabLayoutView:onDetailsTabClicked:fromOnDistrictTabChanged:'  + tab);

                    this.resetTabs();

                    // GA - track the tabs
                    this._pageView(tab);


                    if (tab === "School Locations") {
                        console.log('  School Locations');

                        if (this.activeRegion != '#district-tab-content-region')
                            $(this.activeRegion).css('display', 'none');
                        this.activeRegion = '#district-tab-content-region';
                        $(this.activeRegion).css('display', 'block');

                        this.districtTabContentRegion.show(new SchoolLayoutView());

                        //App.model.set('districtTab', 'Field Evaluations (Acreage)');

                    }
                    else if (tab === "Deficiency View") {
                        console.log('  Deficiency View');

                        if (this.activeRegion != '#district-tab-content-region')
                            $(this.activeRegion).css('display', 'none');
                        this.activeRegion = '#district-tab-content-region';
                        $(this.activeRegion).css('display', 'block');

                        this.districtTabContentRegion.show(new DeficiencyTableView());

                    }
                }
                else {

                    console.log('DistrictTabLayoutView:onDetailsTabClicked:calledFromPhysicalTabClick');

                    var tabText = e.target.innerText;
                    if (tabText === undefined) {
                        tabText = e.target.textContent;
                    }

                    if (tabText === "") {
                        return;
                    }
                    else {
                        tab = tabText;

                        // Store the selected tab
                        App.model.set('districtTab', tab);
                    }
                }

            },
            // GA - To track our application routes
            _pageView: function (tab) {
                var path = Backbone.history.getFragment();
                var prefix = "";
                if (App.mobile) {
                    if (App.phone)
                        prefix = 'phone-';
                    else
                        prefix = 'tablet-';
                }
                ga('send', 'pageview', {page: prefix + "/" + path + "#" + tab});
            },

            resize: function () {
                // Force the child views to resize (as necessary)
                var self = this;

            },
            remove: function () {
                console.log('---------- DistrictTabLayoutView:remove ----------');

                // TODO: Kill referenced views

                // Remove all the event handlers
                this.undelegateEvents();

                // Remove jQuery bindings
                this.$("#districtTabBar").unbind('click');

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });