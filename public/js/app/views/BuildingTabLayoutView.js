define( ['App', 'backbone', 'marionette', 'jquery', 'options/ViewOptions', 'hbs!templates/buildingTabLayout','views/FunctionalityNotAvailableView'],
    function(App, Backbone, Marionette, $, ViewOptions, template, FunctionalityNotAvailableView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            regions: {

                facilitiesTabContentRegion: '#facilities-tab-content-region'

            },
            initialize: function(options){
                this.tab = null;

                //App.model.set('facilitiesTab', 'Rice Acreage');

                console.log('BuildingTabLayoutView:initialize');

                _.bindAll(this);

                this.options = options;
            },
            onRender: function(){
                console.log('BuildingTabLayoutView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- FacilitiesTabLayoutView:onShow ----------');

                // Display the WBS hint for the user
                var viewOptions = new ViewOptions();
                viewOptions.live = false;

                this.listenTo(App.vent, App.Events.ModelEvents.FacilitiesTabChanged, this.onFacilitiesdTabChanged);

                var detailTabBar = this.$("#facilitiesTabBar");
                detailTabBar.bind('click', this.onDetailsTabClicked);

                this.activeRegion = '#facilities-tab-content-region';
                $(this.activeRegion).css('display', 'block');
                //this.startView = new  FacilitiesEvaluationSurvey2View();
                //this.facilitiesTabContentRegion.show(this.startView);

            },

            resetTabs: function(){
                this.facilitiesTabContentRegion.reset();

            },
            onFacilitiesTabChanged: function (text) {
                console.log('FacilitiesTabLayoutView:onFacilitiesTabChanged:tab:' + text);
                this.tab = App.model.get('facilitiesTab');

                this.$('#facilitiesTabBar li').removeClass('active');

                if (this.tab === "Buildings") {
                    this.$('#facilitiesTab2').addClass('active');
                }

                this.onDetailsTabClicked();

                this.tab = null;

            },
            onDetailsTabClicked: function(e){

                var tab;

                if (this.tab !== null) {

                    console.log('BuildingTabLayoutView:onDetailsTabClicked:fromOnFacilitiesTabChanged:' + this.tab);

                    tab = this.tab;

                    this.resetTabs();

                    // GA - track the tabs
                    this._pageView(tab);

                    console.log('  Buildings Tab Clicked');

                    if (this.activeRegion != '#facilities-tab-content-region')
                        $(this.activeRegion).css('display', 'none');
                    this.activeRegion = '#facilities-tab-content-region';
                    $(this.activeRegion).css('display', 'block');

                    //this.facilitiesTabContentRegion.show(new FacilitiesEvaluationSurvey2View());

                }
                else {

                    console.log('BuildingTabLayoutView:onDetailsTabClicked:fromPhysicalTabClick');

                    var tabText = e.target.innerText;

                    if (tabText === undefined) {
                        tabText = e.target.textContent;
                    }
                    tab = tabText;

                    // Store the selected tab
                    App.model.set('facilitiesTab', tab);
                }
            },
            // GA - To track our application routes
            _pageView: function(tab) {
                var path = Backbone.history.getFragment();
                var prefix = "";
                if (App.mobile){
                    if (App.phone)
                        prefix = 'phone-';
                    else
                        prefix = 'tablet-';
                }
                ga('send', 'pageview', {page: prefix + "/" + path + "#" + tab});
            },
            resize: function(){
                // Force the child views to resize (as necessary)
                var self = this;

            },
            remove: function(){
                console.log('---------- BuildingTabLayoutView:remove ----------');

                // TODO: Kill referenced views

                // Remove all the event handlers
                this.undelegateEvents();

                // Remove jQuery bindings
                this.$("#facilitiesTabBar").unbind('click');

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });