define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/facilitiesFilteredLayout', 'views/SchoolCollectionView','views/BuildingCollectionView','views/RoomCollectionView','kendo/kendo.data','kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, template, SchoolCollectionView,BuildingCollectionView,RoomCollectionView) {

        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                schoolFilteredList: '#school-list',
                buildingFilteredList: '#building-list',
                roomFilteredList: '#room-list'
            },

            initialize: function () {
                console.log('FacilitiesFilteredLayoutView:initialize');

                var self = this;
                var app = App;

                this.viewModel = kendo.observable({
                    isChecked: false
                });

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                //this.getData();
            },
            onRender: function () {
                console.log('FacilitiesFilteredLayoutView:initialize');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('FacilitiesFilteredLayoutView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                console.log('FacilitiesFilteredLayoutView:onShow:districtNum:' + this.districtNum);

                this.schoolFilteredList.show(new SchoolCollectionView());
                //this.buildingFilteredList.show(new BuildingCollectionView());
                //this.roomFilteredList.show(new RoomCollectionView());

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedSchoolIdChanged, this.onSelectedSchoolIdChanged, this);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedBuildingIdChanged, this.onSelectedBuildingIdChanged, this);
            },
            onSelectedSchoolIdChanged: function (args) {
                console.log('FacilitiesFilteredLayoutView:onSelectedSchoolIdChanged:' + args);

                var id = parseInt(App.model.get('selectedSchoolId'), 0);

                if (this.roomFilteredList.currentView)
                    this.roomFilteredList.currentView.remove();

                if (id === 9459 || id === 9356) { // Morse or Adams

                    // activate buildings list
                    this.buildingFilteredList.show(new BuildingCollectionView());
                    $('#bottom-right-panel-body').show();

                }
                else {
                    if (this.buildingFilteredList.currentView)
                        this.buildingFilteredList.currentView.remove();
                    $('#bottom-right-panel-body').hide();
                }
            },
            onSelectedBuildingIdChanged: function (args) {
                console.log('FacilitiesFilteredLayoutView:onSelectedBuildingIdChanged:' + args);

                //var id = parseInt(App.model.get('selectedBuildingId'), 0);

                // activate buildings list
                this.roomFilteredList.show(new RoomCollectionView());

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('FacilitiesFilteredLayoutView:resize');
            },
            remove: function () {
                console.log('-------- FacilitiesFilteredLayoutView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });