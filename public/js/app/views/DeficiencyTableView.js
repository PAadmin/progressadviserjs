define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/deficiencyTable', 'views/DeficiencyGridView', 'kendo/kendo.combobox'],
    function (App, Backbone, Marionette, $, template, DeficiencyGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                defRegion: "#def-region"
            },
            initialize: function () {

                console.log('DeficiencyTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('DeficiencyTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log("-------- DeficiencyTableView:onShow --------");

                var self = this;

                // create combo boxes
                var items = [
                    {text: "30 days with deficiencies", value: "30 days"},
                    {text: "60 days with deficiencies", value: "60 days"},
                    {text: "90 days with deficiencies", value: "90 days"},
                    {text: "180 days with deficiencies", value: "180 days"},
                    {text: "All with deficiencies", value: "All"},
                    {text: "All with comments", value: "All with comments"}
                ];
                self.$("#dateRange").kendoDropDownList({ //kendoComboBox({
                    dataTextField: "text",
                    dataValueField: "value",
                    dataSource: items,
                    value: "30 days",
                    change: this.onDropDownChange
                });

                this.defRegion.show(new DeficiencyGridView());

            },
            onDropDownChange: function (e) {
                //console.log('ProgressClientContactTableView:onDropDownChange');
                var self = this;

                var option = this.$("#dateRange").val();

                this.defRegion.reset();
                this.defRegion.show(new DeficiencyGridView({date: option}));

            },
            resize: function () {
                console.log('defView:resize');

                if (this.defRegion.currentView)
                    this.defRegion.currentView.resize();

            },
            remove: function () {
                console.log("--------DeficiencyView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });