define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/introduction', 'views/PageAcknowledgementView',
        'kendo/kendo.all.min', 'jquery.cookie'],
    function (App, Backbone, Marionette, $, template, PageAcknowledgementView) {

        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {

                summaryFarmGridRegion: '#summary-farm-grid-region',
                summaryFieldGridRegion: '#summary-field-grid-region'

            },
            events: {
                'click #LAButton': 'laButtonClicked',
                'click #schoolButton': 'schoolButtonClicked',
                'click #buildingButton': 'buildingButtonClicked'
                //'click #roomButton': 'roomButtonClicked',
                //'click #WITButton': 'witButtonClicked',
                //'click #LWLButton': 'lwlButtonClicked',
                //'click #FMButton': 'fmButtonClicked',
                //'click #SMButton': 'smButtonClicked',
                //'click #NextButton': 'nextButtonClicked',
                //'click #PostNMPButton': 'postNMPButtonClicked',
                //'click #PreNMPButton': 'preNMPButtonClicked'
            },
            initialize: function () {
                _.bindAll(this);

                var self = this;
                var app = App;

                this.districtNum = App.model.get('districtNum');
                this.permittee = App.model.get('permittee');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.getTempData();
                this.getFinalFarmData();

                self.farmTempDataSource.fetch(function () {

                    var farmTempData = this.data();

                    var count = farmTempData.length;
                    console.log('IntroductionView:dataSource:fetch');

                    if (count === 0) {
                        console.log('IntroductionView:dataSource:districtNumNotFound');

                        // Create record for this farm if none exists
                        self.farmTempDataSource.add({
                            districtNumber: self.districtNum,
                            LandownerAwareWDR: "No",
                            LandownerCopyWDR: "No",
                            LocationsWaterLeave: "Yes",
                            LocationsWaterLeaveMarked: "No",
                            LocationsWaterDrains: " ",
                            FarmMapFilledOut: "No",
                            WellInformationFilledOut: "No",
                            MissingFields: "No",
                            MissingFieldsDesc: null,
                            CopyForRecords: "No",
                            IntroductionSeen: "No"
                        });

                        // save the created data item
                        self.farmTempDataSource.sync();

                    }

                    $.each(farmTempData, function (index, value) {

                        self.districtNumber = this.districtNumber;
                        self.LandownerAwareWDR = this.LandownerAwareWDR;
                        self.LandownerCopyWDR = this.LandownerCopyWDR;
                        self.LocationsWaterLeave = this.LocationsWaterLeave;
                        self.LocationsWaterLeaveMarked = this.LocationsWaterLeaveMarked;
                        self.LocationsWaterDrains = this.LocationsWaterDrains;
                        self.FarmMapFilledOut = this.FarmMapFilledOut;
                        self.WellInformationFilledOut = this.WellInformationFilledOut;
                        self.MissingFields = this.MissingFields;
                        self.MissingFieldsDesc = this.MissingFieldsDesc;
                        self.CopyForRecords = this.CopyForRecords;
                        self.introductionSeen = this.IntroductionSeen;

                        var items = [{text: "Yes", value: "Yes"}, {text: "No", value: "No"}];
                        self.$("#copyForRecords").kendoComboBox({
                            dataTextField: "text",
                            dataValueField: "value",
                            dataSource: items,
                            value: self.CopyForRecords
                        });


                        var buttons = self.$(".intro-review");
                        $.each(buttons, function (index, value) {

                            if (this.id === "LAButton") {
                                if (self.LandownerAwareWDR === "Yes" && self.LandownerCopyWDR === "Yes") {
                                    self.$(this).removeClass("btn-danger");
                                    self.$(this).addClass("btn-success");
                                    self.$(this).html("<i class='fa fa-check'></i>&nbsp;Ready (Click here to start)");
                                }
                                else {
                                    self.$(this).html("<i class='fa fa-warning'></i>&nbsp; Fix (Click here to start)");
                                }
                            }
                            else if (this.id === "schoolButton") {

                                var schoolButton = this;

                                // Check to see if any "red" fields exist in the list
                                self.fieldTempDataSource.fetch(function () {

                                    var fieldData = this.data();
                                    var fieldTempCount = this.data().length;

                                    // Make sure for each field at least 1 farm management practice is checked
                                    var allNo = true;
                                    $.each(fieldData, function (index) {
                                        allNo = true;
                                        var fieldRecord = fieldData[index]; //.at(index);
                                        if (fieldRecord.FollowLabelRestrictions === "Yes" ||
                                            fieldRecord.FollowCountyPermitCds === "Yes" ||
                                            fieldRecord.WaterholdingRequirements === "Yes" ||
                                            fieldRecord.MonitorWindConditions === "Yes" ||
                                            fieldRecord.AppropriateBufferZones === "Yes" ||
                                            fieldRecord.AttendTrainings === "Yes" ||
                                            fieldRecord.UseLowDriftNozzles === "Yes" ||
                                            fieldRecord.UsePCARecommendations === "Yes" ||
                                            fieldRecord.EndOfRowShutoff === "Yes" ||
                                            fieldRecord.AvoidSurfaceWater === "Yes" ||
                                            fieldRecord.ComplianceWithPPE === "Yes" ||
                                            fieldRecord.UseDriftControlAgents === "Yes" ||
                                            fieldRecord.ProvideEmployeeSafetyTraining === "Yes" ||
                                            fieldRecord.LandLevPrecTech === "Yes" ||
                                            fieldRecord.UtilizeTailwaterReturn === "Yes" ||
                                            fieldRecord.UtilizePeripheralDrains === "Yes" ||
                                            fieldRecord.MonitorRainForecasts === "Yes" ||
                                            fieldRecord.CropRotationOrg === "Yes" ||
                                            fieldRecord.CoverCroppingOrg === "Yes" ||
                                            fieldRecord.StripCroppingOrg === "Yes" ||
                                            fieldRecord.CompactingLevees === "Yes" ||
                                            fieldRecord.WaterReleasePreFieldPrep === "Yes" ||
                                            fieldRecord.WaterReleasePreHarvest === "Yes" ||
                                            fieldRecord.SlowRelease === "Yes" ||
                                            fieldRecord.CropRotationNonorg === "Yes" ||
                                            fieldRecord.VoluntaryTrainings === "Yes") {
                                            allNo = false;
                                        }
                                        else {
                                            return false;
                                        }

                                    });

                                    if (!allNo) {
                                        self.$(schoolButton).removeClass("btn-danger");
                                        self.$(schoolButton).addClass("btn-success");
                                        self.$(schoolButton).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                    }
                                    else {
                                        self.$(schoolButton).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                        return;
                                    }


                                    // Get field record count from source data and compare
                                    var purDataSource = new kendo.data.DataSource({
                                        type: "odata",
                                        transport: {
                                            read: {
                                                url: App.config.DataServiceURL + "/odata/RiceField",
                                                dataType: "json"
                                            }
                                        },
                                        schema: {
                                            data: "value",
                                            total: function (data) {
                                                return data['odata.count'];
                                            },
                                            model: {
                                                id: "OBJECTID",
                                                fields: {
                                                    OBJECTID: {type: "number"},
                                                    site_id: {type: "string", validation: {required: false}},
                                                    permit_num: {type: "string", validation: {required: false}},
                                                    permittee: {type: "string", validation: {required: false}},
                                                    mtrs: {type: "string", validation: {required: true}},
                                                    crop_list: {type: "string", validation: {required: false}},
                                                    site_name: {type: "string", validation: {required: false}},
                                                    county_num: {type: "string", validation: {required: false}},
                                                    county_name: {type: "string", validation: {required: false}}
                                                }
                                            }
                                        },
                                        batch: false,
                                        //pageSize: 10,
                                        serverPaging: true,
                                        serverSorting: true,
                                        serverFiltering: true,
                                        sort: {field: "OBJECTID", dir: "asc"},
                                        filter: {field: "permit_num", operator: "eq", value: self.districtNum.toString()}
                                        //change: this.onPurDataSourceChange
                                    });

                                    purDataSource.fetch(function () {
                                        var purFieldCount = this.data().length;

                                        if (fieldTempCount >= purFieldCount) {
                                            self.$(schoolButton).removeClass("btn-danger");
                                            self.$(schoolButton).addClass("btn-success");
                                            self.$(schoolButton).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                        }
                                        else {
                                            self.$(schoolButton).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                        }
                                    });
                                });
                            }
                            else if (this.id === "buildingButton") {

                                var feFMPButton = this;

                                // Check to see if any "red" fields exist in the list
                                self.fieldTempDataSource.fetch(function () {

                                    var fieldData = this.data();
                                    var fieldTempCount = this.data().length;

                                    // Make sure for each field at least 1 farm management practice is checked
                                    var allNo = true;
                                    $.each(fieldData, function (index) {
                                        allNo = true;
                                        var fieldRecord = fieldData[index]; //.at(index);
                                        if (fieldRecord.FollowLabelRestrictions === "Yes" ||
                                            fieldRecord.FollowCountyPermitCds === "Yes" ||
                                            fieldRecord.WaterholdingRequirements === "Yes" ||
                                            fieldRecord.MonitorWindConditions === "Yes" ||
                                            fieldRecord.AppropriateBufferZones === "Yes" ||
                                            fieldRecord.AttendTrainings === "Yes" ||
                                            fieldRecord.UseLowDriftNozzles === "Yes" ||
                                            fieldRecord.UsePCARecommendations === "Yes" ||
                                            fieldRecord.EndOfRowShutoff === "Yes" ||
                                            fieldRecord.AvoidSurfaceWater === "Yes" ||
                                            fieldRecord.ComplianceWithPPE === "Yes" ||
                                            fieldRecord.UseDriftControlAgents === "Yes" ||
                                            fieldRecord.ProvideEmployeeSafetyTraining === "Yes" ||
                                            fieldRecord.LandLevPrecTech === "Yes" ||
                                            fieldRecord.UtilizeTailwaterReturn === "Yes" ||
                                            fieldRecord.UtilizePeripheralDrains === "Yes" ||
                                            fieldRecord.MonitorRainForecasts === "Yes" ||
                                            fieldRecord.CropRotationOrg === "Yes" ||
                                            fieldRecord.CoverCroppingOrg === "Yes" ||
                                            fieldRecord.StripCroppingOrg === "Yes" ||
                                            fieldRecord.CompactingLevees === "Yes" ||
                                            fieldRecord.WaterReleasePreFieldPrep === "Yes" ||
                                            fieldRecord.WaterReleasePreHarvest === "Yes" ||
                                            fieldRecord.SlowRelease === "Yes" ||
                                            fieldRecord.CropRotationNonorg === "Yes" ||
                                            fieldRecord.VoluntaryTrainings === "Yes") {
                                            allNo = false;
                                        }
                                        else {
                                            return false;
                                        }

                                    });

                                    if (!allNo) {
                                        self.$(feFMPButton).removeClass("btn-danger");
                                        self.$(feFMPButton).addClass("btn-success");
                                        self.$(feFMPButton).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                    }
                                    else {
                                        self.$(feFMPButton).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                        return;
                                    }


                                    // Get field record count from source data and compare
                                    var purDataSource = new kendo.data.DataSource({
                                        type: "odata",
                                        transport: {
                                            read: {
                                                url: App.config.DataServiceURL + "/odata/RiceField",
                                                dataType: "json"
                                            }
                                        },
                                        schema: {
                                            data: "value",
                                            total: function (data) {
                                                return data['odata.count'];
                                            },
                                            model: {
                                                id: "OBJECTID",
                                                fields: {
                                                    OBJECTID: {type: "number"},
                                                    site_id: {type: "string", validation: {required: false}},
                                                    permit_num: {type: "string", validation: {required: false}},
                                                    permittee: {type: "string", validation: {required: false}},
                                                    mtrs: {type: "string", validation: {required: true}},
                                                    crop_list: {type: "string", validation: {required: false}},
                                                    site_name: {type: "string", validation: {required: false}},
                                                    county_num: {type: "string", validation: {required: false}},
                                                    county_name: {type: "string", validation: {required: false}}
                                                }
                                            }
                                        },
                                        batch: false,
                                        //pageSize: 10,
                                        serverPaging: true,
                                        serverSorting: true,
                                        serverFiltering: true,
                                        sort: {field: "OBJECTID", dir: "asc"},
                                        filter: {field: "permit_num", operator: "eq", value: self.districtNum.toString()}
                                        //change: this.onPurDataSourceChange
                                    });

                                    purDataSource.fetch(function () {
                                        var purFieldCount = this.data().length;

                                        if (fieldTempCount >= purFieldCount) {
                                            self.$(feFMPButton).removeClass("btn-danger");
                                            self.$(feFMPButton).addClass("btn-success");
                                            self.$(feFMPButton).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                        }
                                        else {
                                            self.$(feFMPButton).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                        }
                                    });
                                });
                            }
                            else if (this.id === "WITButton") {
                                if (self.WellInformationFilledOut === "Yes") {
                                    self.$(this).removeClass("btn-danger");
                                    self.$(this).addClass("btn-success");
                                    self.$(this).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                }
                                else {
                                    self.$(this).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                }
                            }
                            else if (this.id === "LWLButton") {
                                if (self.LocationsWaterLeave === "Yes" && self.LocationsWaterLeaveMarked === "No") {
                                    // Leave flagged as red
                                    self.$(this).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                }
                                else {
                                    self.$(this).removeClass("btn-danger");
                                    self.$(this).addClass("btn-success");
                                    self.$(this).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                }
                            }
                            else if (this.id === "FMButton") {
                                if (self.FarmMapFilledOut === "Yes") {
                                    self.$(this).removeClass("btn-danger");
                                    self.$(this).addClass("btn-success");
                                    self.$(this).html("<i class='fa fa-check'></i>&nbsp;Ready");
                                }
                                else {
                                    self.$(this).html("<i class='fa fa-warning'></i>&nbsp; Fix");
                                }
                            }
                            else if (this.id === "SMButton") {

                                var smButton = this;
                                self.farmFinalDataSource.fetch(function () {
                                    var data = this.data();
                                    var count = data.length;
                                    console.log('IntroductionView:farmFinalDataSource:fetch');
                                    // Get values from target data
                                    if (count === 0) {

                                        self.$(smButton).html("<i class='fa fa-warning'></i>&nbsp;Not Yet Submitted");
                                    }
                                    else {
                                        self.$(smButton).removeClass("btn-danger");
                                        self.$(smButton).addClass("btn-success");
                                        self.$(smButton).html("<i class='fa fa-check'></i>&nbsp;Submitted");
                                    }
                                });
                            }
                        });

                        // Open modal acknowledgement dialog the first time
                        if (self.introductionSeen !== "Yes") {
                            var insText = "<p>Provide the requested information in the web-based template for compliance with the Rice WDR, based on the previous year’s growing season information. For example, you will use" +
                                "your farming information from the 2015 crop year for the Farm Evaluation due by 1 March 2016. You only need to provide the Farm Evaluation to the CRC once a year on 1 March.</p>" +
                                "You must complete all parts of the Farm Evaluation. Failure to complete portions of the Farm Evaluation will not allow you to submit the data/information to the CRC.";
                            var options = {
                                instructions: insText,
                                field: "IntroductionSeen"
                            };

                            App.modal.show(new PageAcknowledgementView(options));

                        }

                        return false;  // only look at first record (should only be 1)

                    });

                });


            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            hasClass: function (el, selector) {
                var className = " " + selector + " ";
                if ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1) {
                    return true;
                }
                return false;
            },
            onShow: function () {

                console.log("-------- SummaryView:onShow --------");

                var self = this;

                console.log('SummaryView:onShow:districtNum:' + this.districtNum);
                if (this.districtNum === null) {
                    // Go back to home page
                    window.location.href = "";
                }

                // Panel title
                this.$("#suTitle").html(this.permittee + " (" + this.districtNum + ")");

                var feText = "<strong>Note:</strong> This form is to be filled out for the past crop harvested.";

                //if (!App.mobile) {
                    this.$('#qFE').attr('title', feText);
                    this.$('#qFE').tooltip();
                //}
            },
            createAutoClosingAlert: function (selector, html) {
                console.log('SummaryView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },
            nextButtonClicked: function (e) {
                console.log('IntroductionView:nextButtonClicked');

                App.model.set('farmTab', 'Landowner Acknowledgement');

            },


            getTempData: function () {

                var self = this;

                this.farmTempDataSource = new kendo.data.DataSource({
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/WholeFarmTemp",
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {

                                console.log('WellInformationGridView:update: url');
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp" + "(" + data.Id + ")";

                            }
                        },
                        create: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp";
                            },
                            dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/WholeFarmTemp" + "(" + data.Id + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                PermitNumber: {type: "string", validation: {required: true}},
                                LandownerAwareWDR: {type: "string", validation: {required: true}},
                                LandownerCopyWDR: {type: "string", validation: {required: true}},
                                LocationsWaterLeave: {type: "string", validation: {required: true}},
                                LocationsWaterLeaveMarked: {type: "string", validation: {required: true}},
                                LocationsWaterDrains: {type: "string", validation: {required: true}},
                                FarmMapFilledOut: {type: "string", validation: {required: true}},
                                WellInformationFilledOut: {type: "string", validation: {required: true}},
                                MissingFields: {type: "string", validation: {required: true}},
                                MissingFieldsDesc: {type: "string", validation: {required: true}},
                                CopyForRecords: {type: "string", validation: {required: true}},
                                IntroductionSeen: {type: "string", validation: {required: true}}

                            }
                        }
                    },
                    requestStart: function () {
                        //console.log('LandownerAcknowledgementView:dataSource:requestStart');

                    },
                    requestEnd: function () {
                        //console.log('LandownerAcknowledgementView:dataSource:requestEnd');

                    },
                    error: function () {
                        //console.log('LandownerAcknowledgementView:dataSource:error');

                    },
                    //change: this.onFarmTempDataSourceChange,
                    batch: false,
                    pageSize: 50,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    filter: {field: "PermitNumber", operator: "eq", value: self.districtNum.toString()}
                });

                this.fieldTempDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/FieldsTemp",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        id: "Id",
                        fields: {
                            Id: {editable: false, defaultValue: 0, type: "number"},
                            PermitNumber: {type: "string", validation: {required: true}},
                            SiteId: {type: "string", validation: {required: true}},
                            RiceAcreageGrown: {type: "number", validation: {required: true}},
                            NoRiceGrown: {type: "string", validation: {required: true}},
                            RotateRice: {type: "string", validation: {required: true}},
                            OtherCrops: {type: "string", validation: {required: true}},
                            PercentageOtherCrops: {type: "number", validation: {required: true}},
                            RotationFreq: {type: "string", validation: {required: true}},
                            LandTakenOut: {type: "string", validation: {required: true}},
                            FollowLabelRestrictions: {type: "string", validation: {required: true}},
                            FollowCountyPermitCds: {type: "string", validation: {required: true}},
                            WaterholdingRequirements: {type: "string", validation: {required: true}},
                            MonitorWindConditions: {type: "string", validation: {required: true}},
                            AppropriateBufferZones: {type: "string", validation: {required: true}},
                            AttendTrainings: {type: "string", validation: {required: true}},
                            UseLowDriftNozzles: {type: "string", validation: {required: true}},
                            UsePCARecommendations: {type: "string", validation: {required: true}},
                            EndOfRowShutoff: {type: "string", validation: {required: true}},
                            AvoidSurfaceWater: {type: "string", validation: {required: true}},
                            ComplianceWithPPE: {type: "string", validation: {required: true}},
                            UseDriftControlAgents: {type: "string", validation: {required: true}},
                            ProvideEmployeeSafetyTraining: {type: "string", validation: {required: true}},
                            LandLevPrecTech: {type: "string", validation: {required: true}},
                            UtilizeTailwaterReturn: {type: "string", validation: {required: true}},
                            UtilizePeripheralDrains: {type: "string", validation: {required: true}},
                            MonitorRainForecasts: {type: "string", validation: {required: true}},
                            CropRotationOrg: {type: "string", validation: {required: true}},
                            CoverCroppingOrg: {type: "string", validation: {required: true}},
                            StripCroppingOrg: {type: "string", validation: {required: true}},
                            CompactingLevees: {type: "string", validation: {required: true}},
                            WaterReleasePreFieldPrep: {type: "string", validation: {required: true}},
                            WaterReleasePreHarvest: {type: "string", validation: {required: true}},
                            SlowRelease: {type: "string", validation: {required: true}},
                            CropRotationNonorg: {type: "string", validation: {required: true}},
                            VoluntaryTrainings: {type: "string", validation: {required: true}}
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 100,
                    filter: {field: "PermitNumber", operator: "eq", value: self.districtNum}
                });

                this.witTempDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/WITTemp",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        id: "Id",
                        fields: {
                            Id: {editable: false, defaultValue: 0, type: "number"},
                            districtNumber: {type: "string", validation: {required: true}},
                            WellId: {
                                editable: true,
                                type: "string",
                                validation: {required: true}
                            },
                            GroundSlopedAway: {
                                type: "string",
                                validation: {required: true}
                            },
                            StandingWaterAvoided: {type: "string", validation: {required: true}},
                            GoodHousekeepingPractices: {type: "string", validation: {required: true}},
                            AirGap: {type: "string", validation: {required: true}},
                            BackflowPrevCheckValve: {type: "string", validation: {required: true}},
                            YearAbandoned: {type: "string", validation: {required: true}},
                            DestroyedDescription: {type: "string", validation: {required: true}}
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 100,
                    filter: {field: "PermitNumber", operator: "eq", value: self.districtNum}
                });
            },
            getFinalFarmData: function () {

                var self = this;

                this.farmFinalDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/WholeFarmFinal",
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {

                                console.log('SummaryView:WholeFarmFinal:update:' + data.Id);
                                return App.config.DataServiceURL + "/odata/WholeFarmFinal" + "(" + data.Id + ")";

                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('SummaryView:WholeFarmFinal:create:');
                                return App.config.DataServiceURL + "/odata/WholeFarmFinal";
                            },
                            dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/WholeFarmFinal" + "(" + data.Id + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                PermitNumber: {type: "string", validation: {required: true}},
                                LandownerAwareWDR: {type: "string", validation: {required: true}},
                                LandownerCopyWDR: {type: "string", validation: {required: true}},
                                LocationsWaterLeave: {type: "string", validation: {required: true}},
                                LocationsWaterLeaveMarked: {type: "string", validation: {required: true}},
                                LocationsWaterDrains: {type: "string", validation: {required: true}},
                                FarmMapFilledOut: {type: "string", validation: {required: true}},
                                WellInformationFilledOut: {type: "string", validation: {required: true}},
                                MissingFields: {type: "string", validation: {required: true}},
                                MissingFieldsDesc: {type: "string", validation: {required: true}},
                                CopyForRecords: {type: "string", validation: {required: true}},
                                IntroductionSeen: {type: "string", validation: {required: true}}
                            }
                        }
                    },
                    requestStart: function () {
                        //console.log('LandownerAcknowledgementView:dataSource:requestStart');

                    },
                    requestEnd: function () {
                        //console.log('LandownerAcknowledgementView:dataSource:requestEnd');

                    },
                    error: function () {
                        //console.log('SummaryView:dataSource:error');

                    },
                    //change: this.onFarmTempDataSourceChange,
                    batch: false,
                    pageSize: 50,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    //change: this.onFarmFinalDataSourceChange
                    filter: {field: "PermitNumber", operator: "eq", value: self.districtNum.toString()}
                });
            },

            schoolButtonClicked: function (e) {
                console.log('IntroductionView:schoolButtonClicked');

                App.model.set('districtTab', 'School Locations');

            },
            buildingButtonClicked: function (e) {
                console.log('IntroductionView:schoolButtonClicked');

                App.model.set('districtTab', 'Buildings');

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('SummaryView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

            },
            remove: function () {
                console.log("-------- SummaryView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });