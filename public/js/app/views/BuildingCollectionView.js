define(['App', 'backbone', 'marionette', 'jquery', 'models/BuildingModel',
        'hbs!templates/buildingCollection', 'views/BuildingView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, BuildingModel, template, BuildingView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: BuildingView,
            events: {

            },
            initialize: function (options) {
                console.log('BuildingCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();

                var self = this;

                //this.districtNum = App.model.get('districtNum');
                //this.permittee = App.model.get('permittee');

                this.selectedBuildingId = App.model.get('selectedBuildingId');

                console.log('BuildingCollectionView:initialize:selectedBuildingId: ' + this.selectedBuildingId);

                this.getData();

            },
            onShow: function () {
                console.log("-------- BuildingCollectionView:onShow --------");

            },
            getData: function () {

                var buildings = [
                    {"Id": 1, "name": "Building 100"},
                    {"Id": 2, "name": "Building 200"},
                    {"Id": 3, "name": "Building 300"},
                    {"Id": 4, "name": "Building 400"},
                    {"Id": 5, "name": "Building 500"},
                    {"Id": 6, "name": "Building 600"},
                    {"Id": 8, "name": "Building 800"},
                    {"Id": 9, "name": "Building 900"}
                ];

                this.buildingDataSource = new kendo.data.DataSource({
                    data: buildings,
                    change: this.onBuildingDataSourceChange
                });

                this.buildingDataSource.read();

            },

            onBuildingDataSourceChange: function (e) {
                console.log('BuildingCollectionView:onBuildingDataSourceChange');

                var self = this;

                self.collection.reset();
                self.collection.comparator = function (model) {
                    return model.get("id");
                };

                var sourceData = this.buildingDataSource.data();

                var fieldCount = sourceData.length;

                console.log('BuildingCollectionView:dataSource:fetch');

                $.each(sourceData, function (index, value) {

                    // Need to get the data record and populate it
                    var model = new BuildingModel();

                    model.set("id", value.Id);
                    model.set("name", value.name);

                    if (self.options)
                        model.set("isEditable", self.options.isEditable);
                    else
                        model.set("isEditable", false);

                    if((value.Id % 3) === 0)
                        model.set ("status","Complete");
                    else if((value.Id % 2) === 0)
                        model.set ("status","InProgress");

                    self.collection.push(model);

                });


            },
            onResize: function () {
//                console.log('BuildingCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('BuildingCollectionView:resize');

                //TODO: Add your code
            },
            remove: function () {
                console.log('---------- BuildinglCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
