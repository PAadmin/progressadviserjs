define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/buildingLayout', 'views/FacilitiesFilteredLayoutView',
         'views/BuildingTabLayoutView'],
    function(App, Backbone, Marionette, $, Model, template, FacilitiesFilteredLayoutView, BuildingTabLayoutView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            template: template,
            regions: {
                facilitiesFilteredLayoutRegion: '#facilities-filtered-layout',
                facilitiesFilteredListLayoutRegion: "#facilities-filtered-list-layout"
            },
            initialize: function() {
                console.log('BuildingLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.listenTo(App.vent, App.Events.ModelEvents.AppBannerVisibilityChanged, this.onAppBannerVisibilityChanged, this);
            },
            onRender: function (){
                console.log('BuildingLayoutView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('BuildingLayoutView:onShow');

                // Setup the container
                this.resize();

                this.facilitiesFilteredLayoutRegion.show(new FacilitiesFilteredLayoutView());
                this.facilitiesFilteredListLayoutRegion.show(new BuildingTabLayoutView());
            },
            onAppBannerVisibilityChanged: function(){
                console.log('BuildingLayoutView:onAppBannerVisibilityChanged');

                this.resize();
            },
            onResize: function(){
                //console.log('ProjectDetailsLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                console.log('BuildingLayoutView:resize');

                var height = $(window).height();
                var calculatedHeight = height;
                if (App.model.get('appBannerVisible') === true){
                    calculatedHeight = height - 80 - 150;
                }
                else{
                    calculatedHeight = height - 152; //72
                }
                if (calculatedHeight < 662)
                    calculatedHeight = 662;
                //this.$(".project-details-layout-container").height(calculatedHeight);

                // Now resize our views as the window size has not changed
                if (this.facilitiesFilteredLayoutRegion.currentView)
                    this.facilitiesFilteredLayoutRegion.currentView.resize();
                if (this.facilitiesFilteredListLayoutRegion.currentView)
                    this.facilitiesFilteredListLayoutRegion.currentView.resize();

            },
            remove: function(){
                console.log('BuildingLayoutView:remove');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // App event listeners are cleaned up by Backbone via listenTo
                // App.vent.off(...

                //call the superclass remove method
                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });