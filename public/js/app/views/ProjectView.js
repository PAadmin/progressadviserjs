define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/project'],
    function(App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            template: template,
            initialize: function(){
                _.bindAll(this);
            },
            onRender:function(){
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow:function(){

                this.$('.projectName').text(this.model.get("ProjectFullName"));
            }

        });
    });