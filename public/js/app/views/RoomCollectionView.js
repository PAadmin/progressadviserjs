define(['App', 'backbone', 'marionette', 'jquery', 'models/RoomModel',
        'hbs!templates/roomCollection', 'views/RoomView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, RoomModel, template, RoomView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: RoomView,
            events: {},
            initialize: function (options) {
                console.log('RoomCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();

                var self = this;

                //this.districtNum = App.model.get('districtNum');
                //this.permittee = App.model.get('permittee');

                this.selectedBuildingId = App.model.get('selectedBuildingId');

                console.log('RoomCollectionView:initialize:selectedRoomId: ' + this.selectedBuildingId);

                this.getData();

            },
            onShow: function () {
                console.log("-------- RoomCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;
                var rooms = [];

                if (self.selectedBuildingId === 1) {

                    rooms = [
                        {"Id": 1, "name": "Job Center"},
                        {"Id": 1, "name": "Room 101"},
                        {"Id": 1, "name": "Room 103"},
                        {"Id": 1, "name": "Room 104"},
                        {"Id": 1, "name": "Room 105"},
                        {"Id": 1, "name": "Room 106"},
                        {"Id": 1, "name": "Room 107"},
                        {"Id": 1, "name": "Room 108"},
                        {"Id": 1, "name": "Room 109"},
                        {"Id": 1, "name": "Room 110"},
                        {"Id": 1, "name": "Room 111"},
                        {"Id": 1, "name": "Room 112"},
                        {"Id": 1, "name": "Room 113"},
                        {"Id": 1, "name": "Room 115"}

                    ];
                }
                else if (self.selectedBuildingId === 2) {
                    rooms = [
                        {"Id": 2, "name": "Library"},
                        {"Id": 2, "name": "Media Room"},
                        {"Id": 2, "name": "Room 201"},
                        {"Id": 2, "name": "Room 203"},
                        {"Id": 2, "name": "Room 204"},
                        {"Id": 2, "name": "Room 205"},
                        {"Id": 2, "name": "Room 206"},
                        {"Id": 2, "name": "Room 207"},
                        {"Id": 2, "name": "Room 208"},
                        {"Id": 2, "name": "Room 209"},
                        {"Id": 2, "name": "Room 210"},
                        {"Id": 2, "name": "Room 211"},
                        {"Id": 2, "name": "Room 212"},
                        {"Id": 2, "name": "Room 214"},
                        {"Id": 2, "name": "Room 216"}

                    ];
                } else if (self.selectedBuildingId === 3) {
                    rooms = [
                        {"Id": 3, "name": "Room 301"},
                        {"Id": 3, "name": "Room 302"},
                        {"Id": 3, "name": "Room 303"},
                        {"Id": 3, "name": "Room 304"},
                        {"Id": 3, "name": "Room 305"},
                        {"Id": 3, "name": "Room 306"},
                        {"Id": 3, "name": "Room 307"},
                        {"Id": 3, "name": "Room 308"},
                        {"Id": 3, "name": "Room 309"},
                        {"Id": 3, "name": "Room 310"},
                        {"Id": 3, "name": "Room 311"},
                        {"Id": 3, "name": "Room 312"},
                        {"Id": 3, "name": "Room 313"},
                        {"Id": 3, "name": "Room 314"},
                        {"Id": 3, "name": "Room 315"},
                        {"Id": 3, "name": "Room 316"}
                    ];
                }

                this.roomDataSource = new kendo.data.DataSource({
                    data: rooms
                    //change: self.onRoomDataSourceChange,
                    //filter: {field: "Id", operator: "eq", value: self.selectedBuildingId}
                });

                this.roomDataSource.read();

                // Load up source room data
                //var sourceState = {};
                //var sourceFilters = [];
                //
                //sourceFilters.push({field: "Id", operator: "eq", value: this.selectedBuildingId});
                //sourceState.filter = sourceFilters;
                //sourceState.sort = {field: "name", dir: "asc"};
                //this.roomDataSource.query(sourceState);

                this.roomDataSource.fetch(function () {
                    console.log('RoomCollectionView:dataSource:fetch'); //:filter:' + JSON.stringify(this.filter().filters[0]));

                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("id");
                    };

                    var sourceData = self.roomDataSource.data();

                    var fieldCount = sourceData.length;

                    $.each(sourceData, function (index, value) {

                        // Need to get the data record and populate it
                        var model = new RoomModel();

                        model.set("id", value.id);
                        model.set("name", value.name);

                        if (self.options)
                            model.set("isEditable", self.options.isEditable);
                        else
                            model.set("isEditable", false);

                        if((index % 3) === 0)
                            model.set ("status","Complete");
                        else if((index % 2) === 0)
                            model.set ("status","InProgress");
                        self.collection.push(model);

                    });
                });
            },
            onRoomDataSourceChange: function (e) {
                console.log('RoomCollectionView:onRoomDataSourceChange');

                var self = this;

                self.collection.reset();
                self.collection.comparator = function (model) {
                    return model.get("id");
                };

                //this.roomDataSource.read();
                var sourceData = this.roomDataSource.data();

                var fieldCount = sourceData.length;


                $.each(sourceData, function (index, value) {

                    // Need to get the data record and populate it
                    var model = new RoomModel();

                    model.set("id", value.id);
                    model.set("name", value.name);

                    if (self.options)
                        model.set("isEditable", self.options.isEditable);
                    else
                        model.set("isEditable", false);

                    if((index % 3) === 0)
                        model.set ("status","Complete");

                    self.collection.push(model);
                });


            },
            onResize: function () {
//                console.log('RoomCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('RoomCollectionView:resize');

                //TODO: Add your code
            },
            remove: function () {
                console.log('---------- RoomCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
