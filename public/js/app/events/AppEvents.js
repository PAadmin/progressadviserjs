define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(options) {

            },
            // Model Events - events that are triggered by a state change in the model
            // In this scenario we never want these events firing without values persisted
            // on the model...for more details see AppModel.js
            ModelEvents: {

                AppBannerVisibilityChanged: "AppBannerVisibilityChanged",
                BreadcrumbChanged: "BreadcrumbChanged",
                DistrictTabChanged: "DistrictTabChanged",
                //FieldTabChanged: "FieldTabChanged",
                DistrictNumChanged: "DistrictNumChanged",
                PermitteeChanged: "PermitteeChanged",
                SelectedSchoolIdChanged: "SelectedSchoolIdChanged",
                SelectedBuildingIdChanged: "SelectedBuildingIdChanged",
                SelectedRoomIdChanged: "SelectedRoomIdChanged",
                CentroidChanged: "CentroidChanged",
                BasemapLayerChanged: 'BasemapLayerChanged'
            }
        });
    });